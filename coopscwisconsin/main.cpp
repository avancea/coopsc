//generates the Wisconsin benchmark data set
#include <string>
#include <iostream>
#include <cstdlib>
#include <ctime>
#include <stdlib.h>
#include <libpq-fe.h>
#include <boost/format.hpp>
#include <boost/program_options.hpp>

using namespace std;
using namespace boost;
using namespace boost::program_options;

const char* CREATE_TABLE_QUERY = "create table %1% "
	"(  unique1           integer NOT NULL, "
	"   unique2           integer NOT NULL PRIMARY KEY, "
	"   two               integer NOT NULL, "
	"   four           	  integer NOT NULL, "
	"   ten            	  integer NOT NULL, "
	"   twenty        	  integer NOT NULL, "
	"   onePercent        integer NOT NULL, "
	"   tenPercent        integer NOT NULL, "
	"   twentyPercent     integer NOT NULL, "
	"   fiftyPercent      integer NOT NULL, "
	"   unique3           integer NOT NULL, "
	"   evenOnePercent    integer NOT NULL, "
	"   oddOnePercent     integer NOT NULL, "
	"   stringu1          char(52) NOT NULL, "
	"   stringu2          char(52) NOT NULL, "
	"   string4           char(52) NOT NULL  "
	")";

const char* CREATE_UNIQUE1_INDEX = "CREATE UNIQUE INDEX %1%_unique1_idx ON %1% (unique1)";

struct WiscRow
{
	int unique1;
	int unique2;
	int two;
	int four;
	int ten;
	int twenty;
	int onePercent;
	int tenPercent;
	int twentyPercent;
	int fiftyPercent;
	int unique3;
	int evenOnePercent;
	int oddOnePercent;
	std::string stringu1;
	std::string stringu2;
	std::string string4;
};

PGconn *conn = NULL;

std::string host;
int port;
std::string database;
std::string user;
std::string password;
std::string table;
unsigned long long generator;
unsigned long long prime;
unsigned long long no_tuples;

/* generate a unique random number between 1 and limit*/
unsigned long long rand(unsigned long long seed, unsigned long long limit)
{
	do
	{
		seed = (generator * seed) % prime;
	} while (seed > limit);
	return seed;
}

std::string convert(unsigned long long unique)
{
	char tmp[8], prefix[8];
	int i, j, rem, cnt;

	/* first set result string to "AAAAAAA" */
	for (i = 0; i < 7; i++)
		prefix[i] = 'A';
	i = 6;
	cnt = 0;

	/* convert unique value from right to left into an alphabetic string in tmp*/
	/* tmp digits are right justified in tmp */
	while (unique > 0)
	{
		rem = unique % 26;
		tmp[i] = 'A' + rem;
		unique = unique / 26;
		i--;
		cnt++;
	}
	/* finally move tmp into result, left justifying it */
	for (j = 0; j < cnt; j++, i++)
		prefix[j] = tmp[i + 1];
	prefix[7] = 0;

	//adding 45 'x' at the end
	return prefix + std::string(45, 'x');
}

const char* string4prefix[] =
{ "AAAA", "HHHH", "OOOO", "VVVV" };

void generate_row(WiscRow& row, unsigned long long unique1, unsigned long long unique2)
{
	row.unique1 = unique1;
	row.unique2 = unique2;
	row.two = row.unique1 % 2;
	row.four = row.unique1 % 4;
	row.ten = row.unique1 % 10;
	row.twenty = row.unique1 % 20;
	row.onePercent = row.unique1 % 100;
	row.tenPercent = row.unique1 % 10;
	row.twentyPercent = row.unique1 % 5;
	row.fiftyPercent = row.unique1 % 2;
	row.unique3 = row.unique1;
	row.evenOnePercent = row.onePercent * 2;
	row.oddOnePercent = row.onePercent * 2 + 1;
	row.stringu1 = convert(row.unique1);
	row.stringu2 = convert(row.unique2);
	row.string4 = string4prefix[row.unique2 % 4] + std::string(48, 'x');
}

void execute(const std::string& sql)
{
	PGresult* res;
	res = PQexec(conn, sql.c_str());
	if (PQresultStatus(res) != PGRES_COMMAND_OK)
	{
		cerr << PQerrorMessage(conn);
		PQclear(res);
		PQfinish(conn);
		exit(1);
	}
	PQclear(res);
}

void parseArgs(int argc, char **argv)
{

	cout << "Wisconsin Benchmark dataset generator" << endl << endl;
	options_description desc("Allowed options");
	desc.add_options()("help", "produce help message")("host", value<string> ()->default_value("127.0.0.1"), "database host")("port",
			value<int> ()->default_value(5432), "database port")("database", value<string> (), "database name")("user", value<string> (),
			"database user")("password", value<string> (), "database password")("table", value<string> ()->default_value("wisconsin"),
			"table name")("no_tuples", value<int> (0)->default_value(10000000), "number of tuples");

	variables_map vm;
	store(parse_command_line(argc, argv, desc), vm);
	notify(vm);

	if (vm.count("help"))
	{
		cout << desc << "\n";
		exit(1);
	}
	if (vm.count("host"))
	{
		host = vm["host"].as<string> ();
	}
	else
	{
		cout << "Database host not set!\n";
		exit(1);
	}
	cout << "Database host : " << host << endl;

	if (vm.count("port"))
	{
		port = vm["port"].as<int> ();
	}
	else
	{
		cerr << "Database port not set!\n";
		exit(1);
	}

	if (vm.count("database"))
	{
		database = vm["database"].as<string> ();
	}
	else
	{
		cerr << "Database name not set!\n";
		exit(1);
	}
	cout << "Database name : " << database << endl;

	if (vm.count("user"))
	{
		user = vm["user"].as<string> ();
	}
	else
	{
		cerr << "User name not set!\n";
		exit(1);
	}
	cout << "User name : " << user << endl;

	if (vm.count("password"))
	{
		password = vm["password"].as<string> ();
	}
	else
	{
		cerr << "Password not set!\n";
		exit(1);
	}
	cout << "Password : " << password << endl << endl;

	if (vm.count("table"))
	{
		table = vm["table"].as<string> ();
	}
	else
	{
		cerr << "Table name  not set!\n";
		exit(1);
	}
	cout << "Table name : " << table << endl << endl;

	if (vm.count("no_tuples"))
	{
		no_tuples = vm["no_tuples"].as<int> ();
	}
	else
	{
		cerr << "Number of tuples not set!\n";
		exit(1);
	}
	cout << "Number of tuples : " << no_tuples << endl << endl;

}

void createTable()
{
	execute((boost::format(CREATE_TABLE_QUERY) % table).str());
}

void createIndex()
{
	execute((boost::format(CREATE_UNIQUE1_INDEX) % table).str());
}

void initGeneratorPrime()
{
	if (no_tuples <= 1000)
	{
		generator = 279;
		prime = 1009;
	}
	else if (no_tuples <= 10000)
	{
		generator = 2969;
		prime = 10007;
	}
	else if (no_tuples <= 100000)
	{
		generator = 21395;
		prime = 100003;
	}
	else if (no_tuples <= 1000000)
	{
		generator = 2107;
		prime = 1000003;
	}
	else if (no_tuples <= 10000000)
	{
		generator = 211;
		prime = 10000019;
	}
	else if (no_tuples <= 100000000)
	{
		generator = 21;
		prime = 100000007;
	}
	else
	{
		cerr << "too many rows requested" << endl;
		exit(0);
	}
}

std::string ntuple(WiscRow& row)
{
	return (boost::format("(%1%, %2%, %3%, %4%, %5%, %6%, %7%, %8%, %9%, "
		"%10%, %11%, %12%, %13%, \'%14%\', \'%15%\', \'%16%\')") % row.unique1 % row.unique2 % row.two % row.four % row.ten % row.twenty
			% row.onePercent % row.tenPercent % row.twentyPercent % row.fiftyPercent % row.unique3 % row.evenOnePercent % row.oddOnePercent
			% row.stringu1 % row.stringu2 % row.string4).str();
}

void generateRelation()
{
	unsigned long long seed;
	seed = generator;
	WiscRow row;

	std::string insert = "";
	for (unsigned int i = 0; i < no_tuples; i++)
	{
		if (i % 10000 == 0)
			cout << i << "/" << no_tuples << endl;

		seed = rand(seed, no_tuples);
		generate_row(row, seed - 1, i);

		if (i % 1000 == 0)
		{
			if (insert != "")
				execute(insert);
			insert = "insert into " + table + " values " + ntuple(row);
		}
		else
			insert += "\n, " + ntuple(row);
	}
	if (insert != "")
		execute(insert);
	cout << no_tuples << "/" << no_tuples << endl;
}

int main(int argc, char **argv)
{

	parseArgs(argc, argv);
	std::string conninfo;
	conninfo = (boost::format("hostaddr = \'%1%\' port = %2% user = \'%3%\' password = \'%4%\' dbname = \'%5%\' connect_timeout = \'10\'")
			% host % port % user % password % database).str();
	conn = PQconnectdb(conninfo.c_str());
	if (PQstatus(conn) != CONNECTION_OK)
	{
		cerr << "Connection to database failed: " << PQerrorMessage(conn);
		exit(1);
	}
	initGeneratorPrime();
	createTable();
	generateRelation();
	cout << "Creating index..." << std::endl;
	createIndex();
	cout << "Done" << std::endl;
	PQfinish(conn);
}
;
