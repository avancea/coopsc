#include <libpq-fe.h>
#include <iostream>
#include <boost/program_options.hpp>
#include <boost/format.hpp>
#include <boost/lexical_cast.hpp>
using namespace std;
using namespace boost;
using namespace boost::program_options;

std::string host;
int port;
std::string database;
std::string user;
std::string password;

PGconn *conn = NULL;

enum Command
{
	None, Init, Drop, List, Add, Remove
};
Command com;
std::string comarg; //command argument

//dimension size
std::string table;

std::set<std::string> fields;

int minlevel;
int maxlevel;
int tslevel;

std::string fieldsstring;

//parse command argument
void parseComArg()
{
	//replacing ':' with ' '
	for (unsigned int i = 0; i < comarg.size(); i++)
		if (comarg[i] == ':')
			comarg[i] = ' ';
	std::stringstream s1(comarg);

	s1 >> table;
	std::string sfields, slevels;
	s1 >> sfields;
	s1 >> slevels;
	for (unsigned int i = 0; i < sfields.size(); i++)
		if (sfields[i] == ',')
			sfields[i] = ' ';

	for (unsigned int i = 0; i < slevels.size(); i++)
		if (slevels[i] == ',')
			slevels[i] = ' ';

	std::vector<string> f;

	std::stringstream s2(sfields + " ");
	std::string tmp;

	s2 >> tmp;
	while (!s2.eof())
	{
		f.push_back(tmp);
		s2 >> tmp;
	}

	std::stringstream s3(slevels + " ");
	minlevel = maxlevel = tslevel = -1;
	s3 >> minlevel >> maxlevel >> tslevel;
	if ((table == "") || (com == Add && (tslevel < 0 || minlevel < 0 || maxlevel < 0)) || (com == Remove && ((slevels != "") || (f.size()
			== 0))))
	{
		cerr << "Invalid command arguments\n";
		exit(1);
	}

	for (unsigned int i = 0; i < f.size(); i++)
		fields.insert(f[i]);

	std::set<std::string>::iterator it;
	for (it = fields.begin(); it != fields.end(); it++)
	{
		if (it != fields.begin())
			fieldsstring += ",";
		fieldsstring += (*it);
	}
}

void parseArgs(int argc, char **argv)
{
	options_description desc("CoopSC Database Tool\nAllowed options");
	desc.add_options()("help", "produce help message")("host", value<string> ()->default_value("127.0.0.1"), "database host")("port",
			value<int> ()->default_value(5432), "database port")("database", value<string> (), "database name")("user", value<string> (),
			"database user")("password", value<string> (), "database password")("initcoopsc", "initializes the CoopSC database component")(
			"dropcoopsc", "drops the CoopSC database component")("list", "lists the entries")("add", value<string> (),
			"adds an entry\nFormat: table:fields:minlevel,maxlevel,timestamplevel\nExample: -add wisconsin:unique1,unique2:10,15,13")(
			"remove", value<string> (), "removes an entry\nFormat: table:fields\nExample: -remove wisconsin:unique1,unique2");

	variables_map vm;
	store(parse_command_line(argc, argv, desc), vm);
	notify(vm);

	if (vm.count("help"))
	{
		cout << desc << "\n";
		exit(1);
	}
	if (vm.count("host"))
	{
		host = vm["host"].as<string> ();
	}
	else
	{
		cout << "Database host not set!\n";
		exit(1);
	}

	if (vm.count("port"))
	{
		port = vm["port"].as<int> ();
	}
	else
	{
		cerr << "Database port not set!\n";
		exit(1);
	}

	if (vm.count("database"))
	{
		database = vm["database"].as<string> ();
	}
	else
	{
		cerr << "Database name not set!\n";
		exit(1);
	}

	if (vm.count("user"))
	{
		user = vm["user"].as<string> ();
	}
	else
	{
		cerr << "User name not set!\n";
		exit(1);
	}

	if (vm.count("password"))
	{
		password = vm["password"].as<string> ();
	}
	else
	{
		cerr << "Password not set!\n";
		exit(1);
	}

	com = None;
	if (vm.count("initcoopsc"))
		com = Init;
	if (vm.count("dropcoopsc"))
		com = Drop;
	if (vm.count("list"))
	{
		if (com != None)
		{
			cerr << "Only one command must be specified\n";
			exit(1);
		}
		com = List;
	}
	if (vm.count("add"))
	{
		if (com != None)
		{
			cerr << "Only one command must be specified\n";
			exit(1);
		}
		com = Add;
		comarg = vm["add"].as<string> ();
	}
	if (vm.count("remove"))
	{
		if (com != None)
		{
			cerr << "Only one command must be specified";
			exit(1);
		}
		com = Remove;
		comarg = vm["remove"].as<string> ();
	}
	if (com == None)
	{
		cerr << "No command specified!\n";
		exit(1);
	}
	if (com == Add || com == Remove)
		parseComArg();
}

void execute(const std::string& sql)
{
	PGresult* res;
	res = PQexec(conn, sql.c_str());
	if (PQresultStatus(res) != PGRES_COMMAND_OK)
	{
		cerr << PQerrorMessage(conn);
		PQclear(res);
		PQfinish(conn);
		exit(1);
	}
	PQclear(res);
}

void init()
{
	execute("begin");
	execute("create schema coopsc");
	execute("CREATE TABLE coopsc.metadata"
		"("
		"id serial NOT NULL,"
		"table_name varchar(50),"
		"fields varchar(100),"
		"minlevel int,"
		"maxlevel int,"
		"tslevel int,"
		"CONSTRAINT coopsc_metadata_pkey PRIMARY KEY (id)"
		")");
	execute("commit");
}

void drop()
{
	execute("drop schema if exists coopsc cascade");
}

void list()
{
	PGresult* res;
	res = PQexec(conn, "select table_name, fields, minlevel, maxlevel, tslevel from coopsc.metadata order by table_name");

	if (PQresultStatus(res) != PGRES_TUPLES_OK)
	{
		cerr << PQerrorMessage(conn);
		PQclear(res);
		PQfinish(conn);
		exit(1);
	}
	for (int k = 0; k < PQntuples(res); k++)
	{
		const char* tablename = PQgetvalue(res, k, 0);
		const char* fields = PQgetvalue(res, k, 1);
		const char* minlevel = PQgetvalue(res, k, 2);
		const char* maxlevel = PQgetvalue(res, k, 3);
		const char* tslevel = PQgetvalue(res, k, 4);
		cout << tablename << ":" << fields << ":" << minlevel << "," << maxlevel << "," << tslevel << std::endl;
	}
	PQclear(res);
}

bool existsEntry()
{
	PGresult* res;
	std::string query = std::string("select * from coopsc.metadata where table_name='") + table + "' and fields='" + fieldsstring + "'";
	res = PQexec(conn, query.c_str());

	if (PQresultStatus(res) != PGRES_TUPLES_OK)
	{
		cerr << PQerrorMessage(conn);
		PQclear(res);
		PQfinish(conn);
		exit(1);
	}
	return PQntuples(res) != 0;
}

std::string getTimestampTableName()
{
	std::string result = table;
	std::set<std::string>::iterator it;
	for (it = fields.begin(); it != fields.end(); it++)
		result += "_" + (*it);
	result += "_timestamps";
	return result;
}

std::string getTriggerFunctionName()
{
	std::string result = "tf_" + table;
	std::set<std::string>::iterator it;
	for (it = fields.begin(); it != fields.end(); it++)
		result += "_" + (*it);
	return result;
}

std::string getTriggerName()
{
	std::string result = "coopsc_" + table;
	std::set<std::string>::iterator it;
	for (it = fields.begin(); it != fields.end(); it++)
		result += "_" + (*it);
	return result;
}

std::string constructTimestampTable()
{
	std::string result;
	std::string timestamptable = getTimestampTableName();

	result = "CREATE TABLE  coopsc." + timestamptable + "(";
	std::set<std::string>::iterator it;
	for (it = fields.begin(); it != fields.end(); it++)
	{
		result += "nquad_" + (*it) + " integer NOT NULL,";
	}
	result += "time_stamp bigint NOT NULL,";
	result += "CONSTRAINT " + timestamptable + "_pkey PRIMARY KEY (";
	for (it = fields.begin(); it != fields.end(); it++)
	{
		if (it != fields.begin())
			result += ",";
		result += "nquad_" + (*it);
	}
	result += "))";
	return result;
}

std::string constructTriggerFunction()
{
	std::string result;

	std::string condnew, condold;
	std::string valnew, valold;
	std::string eq;

	std::stringstream sn, so, vn, vo, seq;
	std::set<std::string>::iterator it;

	for (it = fields.begin(); it != fields.end(); it++)
	{
		if (it != fields.begin())
		{
			sn << " and ";
			so << " and ";
			vn << ",";
			vo << ",";
			seq << " and ";
		}
		sn << "new." << (*it) << "/" << (1 << tslevel) << "=" << "nquad_" << (*it);
		so << "old." << (*it) << "/" << (1 << tslevel) << "=" << "nquad_" << (*it);
		vn << "new." << (*it) << "/" << (1 << tslevel);
		vo << "old." << (*it) << "/" << (1 << tslevel);
		seq << "(new." << (*it) << "/" << (1 << tslevel) << " = " << "old." << (*it) << "/" << (1 << tslevel) << ")";
	}
	vn << ",1";
	vo << ",1";
	condnew = sn.str();
	condold = so.str();
	valnew = vn.str();
	valold = vo.str();
	eq = seq.str();

	result += "CREATE FUNCTION coopsc." + getTriggerFunctionName() + "()\n";
	result += "RETURNS trigger AS \n";
	result += "$BODY$declare\n";
	result += "begin\n";
	result += "if (tg_op <> 'DELETE')\n";
	result += "then\n";
	result += "if not exists (select * from coopsc." + getTimestampTableName() + " where " + condnew + ")\n";
	result += "then\n";
	result += "insert into coopsc." + getTimestampTableName() + " values (" + valnew + ");\n";
	result += "end if;\n";
	result += "update coopsc." + getTimestampTableName() + " set time_stamp=time_stamp+1 where " + condnew + ";\n";
	result += "end if;\n";
	result += "if (tg_op <> 'INSERT')\n";
	result += "then\n";
	result += "if (tg_op = 'UPDATE') and " + eq + "\n";
	result += "then\n";
	result += "return null;\n";
	result += "end if;\n";
	result += "if not exists (select * from coopsc." + getTimestampTableName() + " where " + condold + ")\n";
	result += "then\n";
	result += "insert into coopsc." + getTimestampTableName() + " values (" + valold + ");\n";
	result += "end if;\n";
	result += "update coopsc." + getTimestampTableName() + " set time_stamp=time_stamp+1 where " + condold + ";\n";
	result += "end if;\n";
	result += "return null;\n";
	result += "end;$BODY$\n";
	result += "LANGUAGE 'plpgsql' VOLATILE;\n";
	return result;
}

std::string constructTrigger()
{

	std::string result;
	result += "CREATE TRIGGER " + getTriggerName() + "\n";
	result += "AFTER INSERT OR UPDATE OR DELETE\n";
	result += "ON " + table + "\n";
	result += "FOR EACH ROW\n";
	result += "EXECUTE PROCEDURE coopsc." + getTriggerFunctionName() + "();";
	return result;
}

void add()
{
	if (existsEntry())
	{
		cerr << "Entry already exists.\n";
		return;
	}
	execute("begin");
	execute(constructTimestampTable());
	execute(constructTriggerFunction());
	execute(constructTrigger());
	execute(std::string("insert into coopsc.metadata(table_name, fields, minlevel, maxlevel, tslevel) values('") + table + "', '"
			+ fieldsstring + "', " + boost::lexical_cast<std::string>(minlevel) + "," + boost::lexical_cast<std::string>(maxlevel) + ","
			+ boost::lexical_cast<std::string>(tslevel) + ")");
	execute("commit");
}

void remove()
{
	if (!existsEntry())
	{
		cerr << "Entry does not exist.\n";
		return;
	}
	execute("begin");
	execute(string("drop trigger ") + getTriggerName() + " on " + table);
	execute(string("drop function coopsc.") + getTriggerFunctionName() + "()");
	execute(string("drop table coopsc.") + getTimestampTableName());
	execute(string("delete from coopsc.metadata where table_name='") + table + "' and fields='" + fieldsstring + "'");
	execute("commit");
}

int main(int argc, char **argv)
{
	parseArgs(argc, argv);
	std::string conninfo;
	conninfo = (boost::format("hostaddr = \'%1%\' port = %2% user = \'%3%\' password = \'%4%\' dbname = \'%5%\' connect_timeout = \'10\'")
			% host % port % user % password % database).str();
	conn = PQconnectdb(conninfo.c_str());
	if (PQstatus(conn) != CONNECTION_OK)
	{
		cerr << "Connection to database failed: " << PQerrorMessage(conn);
		exit(1);
	}
	switch (com)
	{
	case None:
		break;
	case Init:
		init();
		break;
	case Drop:
		drop();
		break;
	case List:
		list();
		break;
	case Add:
		add();
		break;
	case Remove:
		remove();
		break;
	}
	PQfinish(conn);
	return 0;
}
