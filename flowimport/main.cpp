//generates the Wisconsin benchmark data set
#include <string>
#include <iostream>
#include <cstdlib>
#include <ctime>
#include <stdlib.h>
#include <stdint.h>
#include <libpq-fe.h>
#include <boost/format.hpp>
#include <boost/program_options.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>
#include <boost/date_time/gregorian/gregorian.hpp>

using namespace std;
using namespace boost;
using namespace boost::program_options;
using namespace boost::posix_time;
using namespace boost::gregorian;

struct Flow
{
	int32_t start;
	int32_t duration;

	int32_t proto;

	int32_t srcip;
	int32_t srcport;

	int32_t dstip;
	int32_t dstport;

	int32_t packets;
	int32_t bytes;
};


PGconn *conn = NULL;

std::string host;
int port;
std::string database;
std::string user;
std::string password;
std::string table;

void execute(const std::string& sql)
{
	PGresult* res;
	res = PQexec(conn, sql.c_str());
	if (PQresultStatus(res) != PGRES_COMMAND_OK)
	{
		cerr << PQerrorMessage(conn);
		PQclear(res);
		PQfinish(conn);
		exit(1);
	}
	PQclear(res);
}

void parseArgs(int argc, char **argv)
{

	cout << "Flow importer" << endl << endl;
	options_description desc("Allowed options");
	desc.add_options()("help", "produce help message")
			          ("host", value<string> ()->default_value("127.0.0.1"), "database host")
			          ("port", value<int> ()->default_value(5432), "database port")
			          ("database", value<string> (), "database name")
			          ("user", value<string> (), "database user")
			          ("password", value<string> (), "database password")
			          ("table", value<string> ()->default_value("flows"), "table name");

	variables_map vm;
	store(parse_command_line(argc, argv, desc), vm);
	notify(vm);

	if (vm.count("help"))
	{
		cout << desc << "\n";
		exit(1);
	}
	if (vm.count("host"))
	{
		host = vm["host"].as<string> ();
	}
	else
	{
		cout << "Database host not set!\n";
		exit(1);
	}
	cout << "Database host : " << host << endl;

	if (vm.count("port"))
	{
		port = vm["port"].as<int> ();
	}
	else
	{
		cerr << "Database port not set!\n";
		exit(1);
	}

	if (vm.count("database"))
	{
		database = vm["database"].as<string> ();
	}
	else
	{
		cerr << "Database name not set!\n";
		exit(1);
	}
	cout << "Database name : " << database << endl;

	if (vm.count("user"))
	{
		user = vm["user"].as<string> ();
	}
	else
	{
		cerr << "User name not set!\n";
		exit(1);
	}
	cout << "User name : " << user << endl;

	if (vm.count("password"))
	{
		password = vm["password"].as<string> ();
	}
	else
	{
		cerr << "Password not set!\n";
		exit(1);
	}
	cout << "Password : " << password << endl << endl;

	if (vm.count("table"))
	{
		table = vm["table"].as<string> ();
	}
	else
	{
		cerr << "Table name  not set!\n";
		exit(1);
	}
	cout << "Table name : " << table << endl << endl;
}

int32_t iptoint(const std::string& sip)
{
	//No checking of the input
	unsigned int c1,c2,c3,c4;
	sscanf(sip.c_str(), "%d.%d.%d.%d",&c1,&c2,&c3,&c4);
	int32_t ip = (unsigned long)c4+c3*256+c2*256*256+c1*256*256*256;
	return ip;
}


std::map<std::string, int> protomap;
int nextproto;


int16_t prototoint(const std::string& proto)
{
	if (protomap.find(proto) != protomap.end())
		return protomap[proto];
	protomap[proto] = nextproto;
	nextproto++;
	return nextproto - 1;
}




void readFlows(vector<Flow>& flows, int max)
{
	flows.clear();
	ptime epoch(date(1970, 1, 1));

	char buf[200];
	string tmp;

	for (int i = 0; i < max; i++)
	{
		cin.getline(buf, sizeof(buf));
		if (cin.eof()) break;

		for (int i = 0, k = 0; buf[i]; i++)
			if (buf[i] == ':') { k++; if (k > 2) buf[i] = ' ';}
		stringstream ss(buf);
		Flow flow;


		string time1, time2; ss >> time1 >> time2;
		ptime pt = time_from_string(time1 + " " + time2);

		flow.start = (pt - epoch).total_seconds();

		double d; ss >> d;
		flow.duration = d * 1000;

		std::string proto;
		ss >> proto; flow.proto = prototoint(proto);

		double port;

		std::string srcip, dstip;

		ss >> srcip >> port;  flow.srcport = port;
		ss >> tmp;
		ss >> dstip >> port; flow.dstport = port;
		ss >> flow.packets >> flow.bytes;

		flow.srcip = iptoint(srcip);
		flow.dstip = iptoint(dstip);

		flows.push_back(flow);
	}
}



std::string ntuple(const Flow& flow)
{
	return (boost::format("(%1%, %2%, %3%, %4%, %5%, %6%, %7%, %8%, %9%)")
	         % flow.start % flow.duration % flow.proto % flow.srcip % flow.srcport
	         % flow.dstip % flow.dstport % flow.packets % flow.bytes).str();
}


const int n = 20000;



void insertFlows(const vector<Flow>& flows)
{
	string insertsql = "insert into " + table + " (starttime, duration, proto, srcip, srcport, dstip, dstport, packets, bytes) values ";
	for (auto it = flows.begin(); it != flows.end(); it++)
	{
		if (it != flows.begin()) insertsql += ",";
		insertsql += ntuple(*it);
	}
	execute(insertsql);
}




int main(int argc, char **argv)
{

	protomap["TCP"] = 1;
	protomap["UDP"] = 2;
	protomap["ICMP"] = 3;
	nextproto = 4;


	parseArgs(argc, argv);
	std::string conninfo;
	conninfo = (boost::format("hostaddr = \'%1%\' port = %2% user = \'%3%\' password = \'%4%\' dbname = \'%5%\' connect_timeout = \'10\'")
			% host % port % user % password % database).str();
	conn = PQconnectdb(conninfo.c_str());
	if (PQstatus(conn) != CONNECTION_OK)
	{
		cerr << "Connection to database failed: " << PQerrorMessage(conn);
		exit(1);
	}

	int total = 0;
	vector<Flow> flows;
	while (true)
	{
		readFlows(flows, n);

		insertFlows(flows);
		total += flows.size();
		cout << "No. flow  entries imported : " << total << endl;

		if (flows.size() != n) break;

	}

	PQfinish(conn);
}

