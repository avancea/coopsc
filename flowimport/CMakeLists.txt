cmake_minimum_required(VERSION 2.8)

file (GLOB flowimport_SRCS *.cpp) 

find_package (Boost 1.40 COMPONENTS program_options date_time REQUIRED)
link_directories (${Boost_LIBRARY_DIRS})
include_directories (${Boost_INCLUDE_DIRS})

find_package (PG REQUIRED)
include_directories (${PG_INCLUDE_DIRS})
link_directories (${PG_LIBRARY_DIRS})


add_executable (flowimport ${flowimport_SRCS})
target_link_libraries (flowimport ${PG_LIBRARIES})  
target_link_libraries (flowimport ${Boost_LIBRARIES})



if ("${CMAKE_SYSTEM}" MATCHES "Linux")
add_executable (flowimports ${flowimport_SRCS})
set_target_properties (flowimports PROPERTIES LINK_FLAGS "-static")
target_link_libraries (flowimports ${CoopSC_SOURCE_DIR}/libs/libpq.a)  
target_link_libraries (flowimports ${Boost_LIBRARIES})
target_link_libraries (flowimports chimera)  
target_link_libraries (flowimports pthread)
target_link_libraries (flowimports ssl)
target_link_libraries (flowimports crypto)
target_link_libraries (flowimports dl)
target_link_libraries (flowimports z)
endif ()

