#include "manager.h"



void Manager::setIdle(const std::string& ip, int port)
{
	boost::mutex::scoped_lock lock(mutex);
	busynodes.erase(std::make_pair(ip, port));
	idlenodes.insert(std::make_pair(ip, port));
	nodes.insert(std::make_pair(ip, port));
}

void Manager::setBusy(const std::string& ip, int port)
{
	boost::mutex::scoped_lock lock(mutex);
	busynodes.insert(std::make_pair(ip, port));
	idlenodes.erase(std::make_pair(ip, port));
	nodes.insert(std::make_pair(ip, port));
}

std::pair<std::string, int> Manager::selectNode() const
{
	boost::mutex::scoped_lock lock(mutex);

	auto it = nodes.begin();
	int k = rand() % nodes.size();
	while (k) { it++; k--; }
	return *it;
}


