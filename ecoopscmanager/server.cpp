
#include <boost/archive/binary_oarchive.hpp>
#include <boost/archive/binary_iarchive.hpp>
#include <boost/serialization/shared_ptr.hpp>
#include <boost/serialization/vector.hpp>
#include <boost/serialization/tracking.hpp>

#include "server.h"
#include "manager.h"

typedef boost::archive::binary_oarchive pp;

typedef boost::shared_ptr<boost::archive::binary_oarchive> OArchivePtr;
typedef boost::shared_ptr<boost::archive::binary_iarchive> IArchivePtr;

using boost::asio::ip::tcp;

void Server::operator()()
{
	OArchivePtr oa;
	IArchivePtr ia;
	int status = REPLY_OK;
	try
	{
		for (;;)
		{
			ia = IArchivePtr(new boost::archive::binary_iarchive(*stream));
			int command;
			(*ia) >> command;

			std::cout << "Here!" << std::endl;

			oa = OArchivePtr(new boost::archive::binary_oarchive(*stream));

			if (command == CMD_SELECT_NODE)
			{

				std::pair<std::string, int> node;
				try {
					node = manager.selectNode();
				} catch (std::exception& e)
				{
					status = REPLY_ERROR; std::string error = e.what();
					(*oa) << status; stream->flush();
					(*oa) << error;	stream->flush();
					continue;
				} catch (...)
				{
					std::cout << "A" << std::endl;
				}
				int status = REPLY_OK;
				(*oa) << status << node.first << node.second;
				stream->flush();
				continue;
			}
			if (command == CMD_SET_IDLE)
			{
				std::pair<std::string, int> node;
				(*ia) >> node.first >> node.second;
				manager.setIdle(node.first, node.second);
				int status = REPLY_OK;
				(*oa) << status;
				stream->flush();
				continue;
			}
			if (command == CMD_SET_BUSY)
			{
				std::pair<std::string, int> node;
				(*ia) >> node.first >> node.second;
				manager.setBusy(node.first, node.second);
				int status = REPLY_OK;
				(*oa) << status;
				stream->flush();
				continue;
			}
		}

	} catch (...)
	{
		//LOG_ERROR("Communication error! Thread stopped!");
	}
}

