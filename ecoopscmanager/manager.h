/*
 * manager.h
 *
 *  Created on: Jul 22, 2011
 *      Author: andrei
 */

#ifndef MANAGER_H_
#define MANAGER_H_
#include <set>
#include <utility>
#include <string>
#include <boost/thread.hpp>
#include "listener.h"

class Manager
{
private:
	int port;
	std::set<std::pair<std::string, int> > idlenodes;
	std::set<std::pair<std::string, int> > busynodes;
	std::set<std::pair<std::string, int> > nodes;
	Listener listener;
	mutable boost::mutex mutex;
public:
	Manager(int _port): port(_port), listener(*this)
	{

	}

	void setIdle(const std::string& ip, int port);

	void setBusy(const std::string& ip, int port);

	std::pair<std::string, int> selectNode() const;

	int getPort() const
	{
		return port;
	}

	void run()
	{
		listener.start();
	}

};



#endif /* MANAGER_H_ */
