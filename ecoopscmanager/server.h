#ifndef _SERVER_H
#define _SERVER_H

#include <boost/smart_ptr.hpp>
#include <boost/thread.hpp>
#include <boost/asio.hpp>

typedef boost::shared_ptr<boost::asio::ip::tcp::iostream> TcpIostreamPtr;


const int CMD_SELECT_NODE	  	= 1;
const int CMD_SET_IDLE   		= 2;
const int CMD_SET_BUSY	        = 3;
const int CMD_QUIT   	        = 4;

const int REPLY_OK		  = 1;
const int REPLY_ERROR	  = 2;

class Manager;

class Server
{
private:
	Manager& manager;
	TcpIostreamPtr stream;

public:
	Server(const Server& that) :
		manager(that.manager), stream(that.stream)
	{

	}

	Server(Manager& _manager, TcpIostreamPtr _stream) :
		manager(_manager), stream(_stream)
	{
	}

	void operator()();
};

#endif
