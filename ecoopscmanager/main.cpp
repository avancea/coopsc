#include <iostream>
#include <iostream>
#include <boost/program_options.hpp>
#include <boost/asio.hpp>
#include "manager.h"

using namespace std;
using namespace boost;
using namespace boost::program_options;
using namespace asio;


int port;

int main(int argc, char **argv)
{
	options_description desc("Arguments");
	desc.add_options()
	("port", value<int> ()->default_value(9999), "database port");

	variables_map vm;
	store(parse_command_line(argc, argv, desc), vm);
	notify(vm);

	if (vm.count("port"))
	{
		port = vm["port"].as<int> ();
	}
	else
	{
		cerr << "Database port not set!\n";
		exit(1);
	}
	cout << "Database port : " << port << endl;

	Manager manager(port);
	manager.run();

	while (true)
	{
		sleep(2000);
	}

}
