#include <iostream>
#include <string>
#include <sstream>
#include <cstdlib>
#include <fstream>
#include <time.h>
#include <assert.h>
#include <time.h>
#include <unistd.h>
#include <logger.h>
#include <boost/progress.hpp>
#include <boost/asio.hpp>
#include <boost/program_options.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/archive/binary_oarchive.hpp>
#include <boost/archive/binary_iarchive.hpp>
#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/text_iarchive.hpp>
#include <boost/archive/xml_oarchive.hpp>
#include <boost/archive/xml_iarchive.hpp>
#include <coopsc.h>
#include "generator.h"

using namespace coopsc;
using namespace std;
using namespace boost;
using namespace boost::program_options;

string database; // the database
string user; // the user
string password; // the password
string host; // database host name
unsigned int port; // database port
string coopschost; // coopsc host on which the coopsc
unsigned int coopscport; // cache manager port

unsigned int samplesize;
unsigned int updatesamplesize;
unsigned int nosamples;

ofstream os;
ofstream checksumsos;

int cachelevel;
int cachesize;
bool testing;


//chimera port
int chport;

//chimera bootstrap host
string chhost;

//chimera bootstrap host
string chboothost;

//chimera bootstrap port
int chbootport;

using namespace boost;
using namespace asio;

unsigned int d; //no of dimensions
string table;
vector<string> fields;
vector<int> center;
vector<int> dev;
vector<int> sz;

coopsc::Generator* generator;

// Return the local ip of the computer
string getLocalIp()
{
	static std::string localip;
	if (localip != "")
		return localip;
	io_service io_service;
	ip::tcp::resolver resolver(io_service);
	std::cout << "host name : " << ip::host_name() << std::endl;
	ip::tcp::resolver::query query(ip::host_name(), "");

	ip::tcp::resolver::iterator it = resolver.resolve(query);
	std::cout << "after resolve" << std::endl;
	string result = "127.0.0.1";
	while (it != ip::tcp::resolver::iterator())
	{
		ip::address addr = (it++)->endpoint().address();
		if (addr.is_v4())
		{
			string tmp = addr.to_string();
			if (tmp.size() < 4)
				continue;
			if (tmp[0] == '1' && tmp[1] == '2' && tmp[2] == '7')
				continue;
			std::cout << "ip :" << tmp << std::endl;
			result = tmp;
			//			break;
		}
	}
	localip = result;
	return result;
}

string getHostName()
{
	char hostname[200];
	gethostname(hostname, sizeof(hostname));
	return hostname;
}

//parses the arguments;
//fills the global variables
void parseArgs(int argc, char **argv)
{
	vector<string> vunique1;
	vunique1.push_back("unique1");

	options_description desc("Arguments");
	desc.add_options()
			("help", "produce help message")
			("host", value<string> (), "database host")
			("port", value<int> ()->default_value(5432), "database port")
			("database", value<string> (), "database name")
			("user", value<string> (), "database user")
			("password", value<string> (), "database password")
			("coopschost", value<string> ()->default_value(getLocalIp()), "coopsc host")
			("coopscport", value<unsigned int> ()->default_value(1999),	"coopsc port")
			("cachelevel", value<unsigned int> ()->default_value(2), "0 - no cache; 1 - local cache; 2 - cooperative cache")
			("cachesize", value<unsigned int> ()->default_value(128), "cache size (in megabytes)")
			("chhost", value<string> ()->default_value(getLocalIp()), "chimera host")
			("chport", value<unsigned int> ()->default_value(9999), "chimera port")
			("chboothost", value<string> ()->default_value(""), "chimera bootstrap host")
			("chbootport", value<unsigned int> ()->default_value(9999), "chimera bootstrap port")
			("samplesize", value<unsigned int> ()->default_value(50), "number of queries per sample")
			("updatesamplesize", value<unsigned int> ()->default_value(0), "number of update queries per sample")
			("nosamples",value<unsigned int> ()->default_value(0), "number of samples")
			("test", "verifies the correctness of the results")("out", value<string> (), "the output file")
			("checksumsout", value<string> (), "the checksums output file")
			("dimensions", value<unsigned int> ()->default_value(1),	"number of dimensions")
			("table", value<string> ()->default_value("wisconsin"), "the name of the table")
			("fields", value<vector<string>>()->default_value(vunique1, "unique1"),"the names of the fields")
	        ("center", value<vector<int>>(), "center point")
	        ("dev", value<vector<int>>(), "standard deviation, per dimension")
	        ("size",value<vector<int>>(), "selection size, per dimension");


	variables_map vm;
	store(parse_command_line(argc, argv, desc), vm);
	notify(vm);



	if (vm.count("help"))
	{
		cout << desc << "\n";
		exit(1);
	}

	if (vm.count("host"))
	{
		host = vm["host"].as<string> ();
	}
	else
	{
		cout << "Database host not set!\n";
		exit(1);
	}
	cout << "Database host : " << host << endl;

	if (vm.count("port"))
	{
		port = vm["port"].as<int> ();
	}
	else
	{
		cerr << "Database port not set!\n";
		exit(1);
	}
	cout << "Database port : " << port << endl;

	if (vm.count("database"))
	{
		database = vm["database"].as<string> ();
	}
	else
	{
		cerr << "Database name not set!\n";
		exit(1);
	}
	cout << "Database name : " << database << endl;

	if (vm.count("user"))
	{
		user = vm["user"].as<string> ();
	}
	else
	{
		cerr << "User name not set!\n";
		exit(1);
	}
	cout << "User name : " << user << endl;

	if (vm.count("password"))
	{
		password = vm["password"].as<string> ();
	}
	else
	{
		cerr << "Password not set!\n";
		exit(1);
	}
	cout << "Password : " << password << endl;

	if (vm.count("coopschost"))
	{
		coopschost = vm["coopschost"].as<string> ();
	}
	else
	{
		cerr << "CoopSC host not set!\n";
		exit(1);
	}
	cout << "CoopSC host : " << coopschost << endl;

	if (vm.count("coopscport"))
	{
		coopscport = vm["coopscport"].as<unsigned int> ();
	}
	else
	{
		cerr << "CoopSC port not set!\n";
		exit(1);
	}
	cout << "CoopSC port : " << coopscport << endl;

	if (vm.count("cachelevel"))
	{
		cachelevel = vm["cachelevel"].as<unsigned int> ();
	}
	else
	{
		cerr << "Cache level not set!\n";
		exit(1);
	}
	cout << "Cache level : " << cachelevel << endl;

	if (vm.count("cachesize"))
	{
		cachesize = vm["cachesize"].as<unsigned int> ();
	}
	else
	{
		cerr << "Cache size not set!\n";
		exit(1);
	}
	cout << "Cache size : " << cachesize << " Mb" << endl;

	if (vm.count("chhost"))
	{
		chhost = vm["chhost"].as<string> ();
	}
	else
	{
		cerr << "Chimera host not set!\n";
		exit(1);
	}
	cout << "Chimera host : " << chhost << endl;

	if (vm.count("chport"))
	{
		chport = vm["chport"].as<unsigned int> ();
	}
	else
	{
		cerr << "Chimera port not set!\n";
		exit(1);
	}
	cout << "Chimera port : " << chport << endl;

	if (vm.count("chboothost"))
	{
		chboothost = vm["chboothost"].as<string> ();
	}

	if (vm.count("chbootport"))
	{
		chbootport = vm["chbootport"].as<unsigned int> ();
	}
	else
	{
		if (chboothost != "")
		{
			cerr << "Chimera bootstrap port not set!\n";
			exit(1);
		}
	}

	cout << "Chimera bootstrap host : " << chboothost << ":" << chbootport << endl;
	if (vm.count("samplesize"))
	{
		samplesize = vm["samplesize"].as<unsigned int> ();
	}
	else
	{
		cerr << "Sample size not specified!\n";
		exit(1);
	}
	cout << "Sample size : " << samplesize << endl;

	if (vm.count("updatesamplesize"))
	{
		updatesamplesize = vm["updatesamplesize"].as<unsigned int> ();
	}
	else
	{
		cerr << "Update sample size not specifiedt!\n";
		exit(1);
	}
	cout << "Update sample size : " << updatesamplesize << endl;

	if (vm.count("nosamples"))
	{
		nosamples = vm["nosamples"].as<unsigned int> ();
	}
	else
	{
		cerr << "Number of samples not set!\n";
		exit(1);
	}
	cout << "Number of samples : " << nosamples << endl;

	testing = (vm.count("test"));

	if (testing)
	{
		cout << "Testing session" << endl;
	}
	if (vm.count("out"))
	{
		os.open(vm["out"].as<string> ().c_str());
		cout << "Writing result to " << vm["out"].as<string> () << endl;
	}
	else
	{
		cout << "Output files not specified!" << endl;
	}

	if (vm.count("checksumsout"))
	{
		checksumsos.open(vm["checksumsout"].as<string> ().c_str());
		cout << "Writing checksums to " << vm["out"].as<string> () << endl;
	}
	else
	{
		cout << "Output files for checksums not specified!" << endl;
	}

	if (vm.count("dimensions"))
	{
		d = vm["dimensions"].as<unsigned int> ();
		cout << "Number of dimensions: " << d << "\n";
	}
	else
	{
		cerr << "Number of dimensions not specified!\n";
		exit(1);
	}

	if (vm.count("table"))
	{
		table = vm["table"].as<string> ();
	}
	else
	{
		cerr << "Table name not specified!\n";
		exit(1);
	}

	if (vm.count("fields"))
	{
		fields = vm["fields"].as<vector<string> > ();
		if (fields.size() != d)
		{
			cerr << "Invalid fields!\n";
			exit(1);
		}
		cout << "Fields: "; for (int i = 0; i < d; i++) cout << fields[i]  << " ";
		cout << endl;
	}
	else
	{
		cerr << "Fields not specified!\n";
		exit(1);
	}

	if (vm.count("center"))
	{
		center = vm["center"].as<vector<int> > ();
		if (center.size() != d)
		{
			cerr << "Invalid center point!\n";
			exit(1);
		}
		cout << "Center: "; for (int i = 0; i < d; i++) cout << center[i]  << " ";
		cout << endl;
	}
	else
	{
		cerr << "Center point not specified!\n";
		exit(1);
	}

	if (vm.count("dev"))
	{
		dev = vm["dev"].as<vector<int> > ();
		if (dev.size() != d)
		{
			cerr << "Invalid deviation!\n " << d << " " << dev.size() << endl;
			exit(1);
		}
		cout << "Dev: "; for (int i = 0; i < d; i++) cout << dev[i]  << " ";
		cout << endl;
	}
	else
	{
		cerr << "Standard deviation not specified!\n";
		exit(1);
	}

	if (vm.count("size"))
	{
		sz = vm["size"].as<vector<int>> ();
		if (sz.size() != d)
		{
			cerr << "Invalid size!\n";
			exit(1);
		}
		cout << "Size: "; for (int i = 0; i < d; i++) cout << sz[i]  << " ";
		cout << endl;
	}
	else
	{
		cerr << "Query size not specified!\n";
		exit(1);
	}

}

void registerTestClient(Connection& con)
{
	string insertsql = string("insert into coopsc.testclients values ('") + coopschost + "', " + lexical_cast<string> (coopscport) + ")";
	con.execute(insertsql);
}

void unregisterTestClient(Connection& con)
{
	string deletesql = string("delete from coopsc.testclients where ip = '") + coopschost + "' and port=" + lexical_cast<string> (coopscport);
	con.execute(deletesql);
}

void clearTestClients(Connection& con)
{
	con.execute("drop table if exists coopsc.testclients");
	con.execute("create table coopsc.testclients (ip char(50), port int)");
}

int getNoTestClients(Connection& con)
{
	string countsql = "select count(*) from coopsc.testclients";
	ResultSetPtr result = con.query(countsql);
	return lexical_cast<int> (result->getValue(0, 0));
}

string serialize(const map<PeerId, unsigned int>& peertuples)
{
	stringstream ss;
	ss << "{";
	for (auto it = peertuples.begin(); it != peertuples.end(); it++)
	{
		if (it != peertuples.begin())
			ss << ",";
		ss << "'" << it->first << "':" << it->second;
	}
	ss << "}";
	return ss.str();
}

string serialize(const StatisticsRegistry::QueryStat& stat)
{
	stringstream ss;
	ss << "{";
	ss << "'tuplesServer':" << stat.tuplesServer << ",";
	ss << "'tuplesLocal':" << stat.tuplesLocal << ",";
	ss << "'tuplesPeers':" << serialize(stat.tuplesPeers) << ",";
	ss << "'timestamps':" << stat.timestamps << ",";
	ss << "'duration':" << stat.duration << ",";
	ss << "'durationLocalRewriting':" << stat.durationLocalRewriting << ",";
	ss << "'durationDistributedRewriting':" << stat.durationDistributedRewriting;
	ss << "}";
	return ss.str();
}

void executeUpdateSample(Connection& conn, coopsc::Generator& generator, unsigned int &responsetime)
{
	responsetime = 0;
	if (updatesamplesize == 0)
		return;
	system_time starttime = get_system_time();
	for (unsigned int j = 0; j < updatesamplesize; j++)
	{
		string update = generator.update();
		conn.execute(update);
	}
	system_time endtime = get_system_time();
	posix_time::time_duration duration = endtime - starttime;
	responsetime = duration.total_milliseconds() / updatesamplesize;
}

//return the adv response time
void executeSelectSample(Connection& conn, coopsc::Generator& generator, string& output, unsigned int &responsetime)
{
	conn.getStatisticsRegistry().clear();

	system_time starttime = get_system_time();
	for (unsigned int j = 0; j < samplesize; j++)
	{
		string query = generator.select();
		ResultSetPtr result = conn.query(query);
		if (testing)
		{
			ResultSetPtr r = conn.queryServer(query);
			if (result->checksum() != r->checksum())
				LOG_ERROR("Wrong response");
		}
		checksumsos << result->getNoTuples() << "-" << result->checksum() << endl;
	}
	system_time endtime = get_system_time();
	posix_time::time_duration duration = endtime - starttime;
	std::stringstream ss;
	const StatisticsRegistry::QueryStats stats = conn.getStatisticsRegistry().getQueryStats();
	ss << "{";
	ss << "'n':" << samplesize << "," << endl;
	ss << "'duration':" << duration.total_milliseconds() << "," << endl;
	ss << "'queries':[";
	for (auto it = stats.begin(); it != stats.end(); it++)
	{
		if (it != stats.begin())
			ss << "," << endl;
		else
			ss << endl;
		ss << "           " << serialize(*it);
	}
	ss << endl << "          ]" << endl;
	ss << "}";
	output = ss.str();
	responsetime = duration.total_milliseconds() / samplesize;
}

int main(int argc, char **argv)
{
	INIT_LOGGER(cout);
	parseArgs(argc, argv);
	Connection conn(database, host, port, user, password, coopschost, coopscport, (coopsc::CacheType) cachelevel, cachesize * 1024 * 1024, chhost, chport,
			chboothost, chbootport);
	coopsc::Generator generator(table, fields, center, dev, sz);
	conn.connect();

	if (chboothost == "")
	{
		clearTestClients(conn);
	}
	if (cachelevel == 2)
		sleep(5);

	registerTestClient(conn);
	cout << getHostName() << " : starting the test" << endl;
	os << "HOST=\'" << getHostName() << "\'" << endl;
	os << "PORT=" << coopscport << endl;
	os << "CACHE_LEVEL=" << cachelevel << endl;

	conn.getStatisticsRegistry().start();


	if (cachelevel != 0)
	{
		cout << getHostName() << " : filling the cache" << endl;
		bool first = true;
		os << "PRE_QUERIES=[";
		//Filling the cache
		while (conn.getStorageManager().getSize() < 1024 * 1024 * (cachesize - 2) && cachesize != 0)
		{
			if (!first)
			{
				os << ",";
			}
			os << endl;
			first = false;

			string output;
			unsigned int responsetime;
			executeSelectSample(conn, generator, output, responsetime);
			os << output;
		}
		unsigned int responsetime;
		executeUpdateSample(conn, generator, responsetime);
		os << endl << "]" << endl;
		cout << getHostName() << " : cache filled" << endl;
	}

	vector<unsigned int> updatestimes;
	os << "QUERIES=[";
	for (unsigned int i = 0; i < nosamples; i++)
	{
		if (i != 0)
		{
			os << ",";
		}
		os << endl;

		string output;
		unsigned int responsetime;
		executeSelectSample(conn, generator, output, responsetime);

		os << output;
		cout << getHostName() << " : " << (i + 1) << " " << responsetime << endl;

		executeUpdateSample(conn, generator, responsetime);
		updatestimes.push_back(responsetime);
	}
	os << endl << "]" << endl;
	cout << getHostName() << " : finished" << endl;

	if (updatestimes.size() != 0)
	{
		os << "UPDATES=[";
		for (auto it = updatestimes.begin(); it != updatestimes.end(); it++)
		{
			if (it != updatestimes.begin())
				os << ",";
			os << *it;
		}
		os << "]" << endl;
	}

	unregisterTestClient(conn);
	while (getNoTestClients(conn) != 0)
	{
		string output;
		unsigned int responsetime;
		executeSelectSample(conn, generator, output, responsetime);
		executeUpdateSample(conn, generator, responsetime);
	}
}
