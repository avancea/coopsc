/*
 * managerclient.h
 *
 *  Created on: Aug 5, 2011
 *      Author: andrei
 */

#ifndef MANAGERCLIENT_H_
#define MANAGERCLIENT_H_
#include <string>
#include <utility>
#include <boost/asio.hpp>
#include <boost/smart_ptr.hpp>
#include <boost/function.hpp>
#include <boost/archive/binary_oarchive.hpp>
#include <boost/archive/binary_iarchive.hpp>


namespace ecoopsc
{

class Connection;

class ManagerClient
{
private:
	typedef boost::shared_ptr<boost::asio::ip::tcp::iostream> TcpIostreamPtr;
	typedef boost::shared_ptr<boost::archive::binary_oarchive> OArchivePtr;
	typedef boost::shared_ptr<boost::archive::binary_iarchive> IArchivePtr;
	TcpIostreamPtr stream;

	Connection& con;
public:
	ManagerClient(Connection& _con): con(_con)
	{

	}

	void connect();

	std::pair<std::string, int> getENode();


};


}

#endif /* MANAGERCLIENT_H_ */
