#include <boost/lexical_cast.hpp>
#include <stdexcept>
#include "managerclient.h"
#include "ecoopsc.h"
#include "logger.h"

namespace ecoopsc
{

const int CMD_SELECT_NODE	  	= 1;
const int CMD_SET_IDLE   		= 2;
const int CMD_SET_BUSY	        = 3;
const int CMD_QUIT   	        = 4;

const int REPLY_OK		  = 1;
const int REPLY_ERROR	  = 2;


void ManagerClient::connect()
{
	if (stream.get() != NULL)
		throw std::runtime_error("Already connected!");
	stream = TcpIostreamPtr(new boost::asio::ip::tcp::iostream(con.getManagerHost(), boost::lexical_cast<std::string>(con.getManagerPort())));
}

std::pair<std::string, int> ManagerClient::getENode()
{
	OArchivePtr oa = OArchivePtr(new boost::archive::binary_oarchive(*stream)); //out archive
	(*oa) << CMD_SELECT_NODE;
	stream->flush();

	int status;
	IArchivePtr ia = IArchivePtr(new boost::archive::binary_iarchive(*stream)); //in archive
	(*ia) >> status;

	if (status == REPLY_OK)
	{
		std::string host; int port;
		(*ia) >> host >> port;
		return std::make_pair(host, port);
	}
	else
	{
		std::string error;
		(*ia) >> error;
		throw std::runtime_error(error);
	}
}

}
