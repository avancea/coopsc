#ifndef RESULTSET_H_
#define RESULTSET_H_
#include <boost/smart_ptr.hpp>
#include <string>
#include <map>

namespace ecoopsc
{

//The generic result set class
class ResultSet
{
private:

	int nofields;
	std::map<std::string, int> field2p;
	std::map<int, std::string> p2field;
	std::vector<std::vector<std::string>> values;

public:

	ResultSet(const std::vector<std::string>& fields);

	~ResultSet()
	{
	}


	void addRow(const std::vector<std::string>& row);

	unsigned int getNoTuples() const;
	unsigned int getNoFields() const;
	std::string getFieldName(int fieldno) const;
	std::string getValue(unsigned int tupleno, unsigned int fieldno) const;
	std::string getValue(unsigned int tupleno, const std::string& field) const;


};

typedef boost::shared_ptr<ResultSet> ResultSetPtr;

}

#endif /* RESULTSET_H_ */
