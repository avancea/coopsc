#ifndef _ENODE_CLIENT
#define _ENODE_CLIENT
#include <string>
#include <boost/asio.hpp>
#include <boost/smart_ptr.hpp>
#include <boost/function.hpp>
#include <boost/archive/binary_oarchive.hpp>
#include <boost/archive/binary_iarchive.hpp>
#include "resultset.h"

namespace ecoopsc
{

class ENodeClient
{
private:
	typedef boost::shared_ptr<boost::asio::ip::tcp::iostream> TcpIostreamPtr;
	typedef boost::shared_ptr<boost::archive::binary_oarchive> OArchivePtr;
	typedef boost::shared_ptr<boost::archive::binary_iarchive> IArchivePtr;

	std::string host;
	int port;
	TcpIostreamPtr stream;
public:
	ENodeClient(const std::string _host, int _port): host(_host), port(_port)
	{

	}


	void connect();
	void disconnect();

	ResultSetPtr query(const std::string& q);

};


typedef boost::shared_ptr<ENodeClient> ENodeClientPtr;



}
#endif
