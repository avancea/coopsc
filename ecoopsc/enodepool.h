#ifndef _ENODE_POOL
#define _ENODE_POOL

#include <map>
#include <utility>
#include "enodeclient.h"

namespace ecoopsc
{

class ENodePool
{
private:
	std::map<std::pair<std::string, int>, ENodeClientPtr> pool;

public:
	ENodePool()
	{

	}

	ENodeClientPtr getClient(const std::string& host, int port);



};

}

#endif
