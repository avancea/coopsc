#include "enodepool.h"
namespace ecoopsc
{

ENodeClientPtr ENodePool::getClient(const std::string& host, int port)
{
	if (pool.find(std::make_pair(host, port)) == pool.end())
	{
		ENodeClient* client = new ENodeClient(host, port);
		client->connect();
		return pool[std::make_pair(host, port)] = ENodeClientPtr(client);
	}
	return pool[std::make_pair(host, port)];
}

}
