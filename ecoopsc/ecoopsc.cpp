#include "ecoopsc.h"

namespace ecoopsc
{

ResultSetPtr Connection::query(const std::string& query)
{
	std::pair<std::string, int> enode;
	try {
		enode = manager.getENode();
	} catch (std::exception& e)
	{
		std::cerr << "c" << e.what() << std::endl;
	}

	ResultSetPtr result;
	try {
		result = pool.getClient(enode.first, enode.second)->query(query);
	} catch (std::exception& e)
	{
		std::cerr << "d" << e.what() << std::endl;
	}
	return result;
}

}
