#include <boost/lexical_cast.hpp>
#include <stdexcept>
#include "enodeclient.h"

namespace ecoopsc
{

const int CMD_QUERY	  	= 1;
const int CMD_EXECUTE   = 2;
const int CMD_QUIT	    = 3;

const int REPLY_OK		  = 1;
const int REPLY_ERROR	  = 2;



void ENodeClient::connect()
{
	if (stream.get() != NULL)
		throw std::runtime_error("Already connected!");
	stream = TcpIostreamPtr(new boost::asio::ip::tcp::iostream(host, boost::lexical_cast<std::string>(port)));
}

void ENodeClient::disconnect()
{
	if (stream.get() == NULL)
		throw std::runtime_error("Not connected!");
	stream->close();
}

ResultSetPtr ENodeClient::query(const std::string& q)
{
	OArchivePtr oa = OArchivePtr(new boost::archive::binary_oarchive(*stream)); //out archive
	(*oa) << CMD_QUERY << q;
	stream->flush();

	int status;
	IArchivePtr ia = IArchivePtr(new boost::archive::binary_iarchive(*stream)); //in archive
	(*ia) >> status;

	if (status == REPLY_OK)
	{

		int nofields; (*ia) >> nofields;

		std::vector<std::string> fields;
		for (int i = 0; i < nofields; i++)
		{
			std::string field;
			int fieldsize;
			std::string fieldtype;

			(*ia) >> field >> fieldsize >> fieldtype;
			fields.push_back(field);
		}

		ResultSetPtr result = ResultSetPtr(new ResultSet(fields));

		int notuples; (*ia) >> notuples;
		for (int i = 0; i < notuples; i++)
		{
			std::vector<std::string> row;
			for (int j = 0; j < nofields; j++)
			{
				std::string value; (*ia) >> value;
				row.push_back(value);
			}
			result->addRow(row);
		}
		return result;
	} else
	{
		std::string error; (*ia) >> error;
		throw std::runtime_error(error);
	}

}

}
