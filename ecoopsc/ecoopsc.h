/*
 * ecoopscclient.h
 *
 *  Created on: Jul 22, 2011
 *      Author: andrei
 */
#include <string>
#include "resultset.h"
#include "enodepool.h"
#include "managerclient.h"

#ifndef ECOOPSCCLIENT_H_
#define ECOOPSCCLIENT_H_

namespace ecoopsc
{

class Connection
{
private:
	std::string managerhost;
	int managerport;
	ManagerClient manager;
	ENodePool pool;

public:
	Connection(const std::string& _managerhost, int _managerport): managerhost(_managerhost), managerport(_managerport), manager(*this)
	{

	}

	ResultSetPtr query(const std::string& query);

	const std::string& getManagerHost() const
	{
		return managerhost;
	}

	int getManagerPort() const
	{
		return managerport;
	}

	void connect()
	{
		manager.connect();
	}

};


}



#endif /* ECOOPSCCLIENT_H_ */
