cmake_minimum_required(VERSION 2.8)


file (GLOB ecoopsc_SRCS *.cpp) 

find_package (Boost 1.40 COMPONENTS program_options thread system signals iostreams serialization REQUIRED)
link_directories (${Boost_LIBRARY_DIRS})
include_directories (${Boost_INCLUDE_DIRS})

find_package (OpenSSL REQUIRED)
link_directories (${OpenSSL_LIBRARY_DIRS})
include_directories (${OpenSSL_INCLUDE_DIRS})


add_library (ecoopsc ${ecoopsc_SRCS})
