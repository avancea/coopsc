
#include "resultset.h"
#include <stdexcept>

namespace ecoopsc
{

unsigned int ResultSet::getNoTuples() const
{
	return values.size();
}

unsigned int ResultSet::getNoFields() const
{
	return nofields;
}


std::string ResultSet::getFieldName(int fieldno) const
{
	auto it = p2field.find(fieldno);
	if (it == p2field.end()) throw std::runtime_error("Invalid field number");
	return it->second;
}

std::string ResultSet::getValue(unsigned int tupleno, unsigned int fieldno) const
{
	return values.at(tupleno).at(fieldno);
}

std::string ResultSet::getValue(unsigned int tupleno, const std::string& field) const
{
	auto it = field2p.find(field);
	if (it == field2p.end()) throw std::runtime_error("Invalid field name");
	return getValue(tupleno, it->second);

}

ResultSet::ResultSet(const std::vector<std::string>& fields)
{
	nofields = fields.size();
	for (int i = 0; i < fields.size();i++)
	{
		field2p[fields[i]] = i;
		p2field[i] = fields[i];
	}

}


void ResultSet::addRow(const std::vector<std::string>& row)
{
	if (row.size() != nofields)
	{
		throw std::runtime_error("Invalid number of fields ");
	}
	values.push_back(row);
}




}
