#ifndef _SERVER_H
#define _SERVER_H

#include <boost/smart_ptr.hpp>
#include <boost/thread.hpp>
#include <boost/asio.hpp>

typedef boost::shared_ptr<boost::asio::ip::tcp::iostream> TcpIostreamPtr;


const int CMD_QUERY	  	= 1;
const int CMD_EXECUTE   = 2;
const int CMD_QUIT	    = 3;

const int REPLY_OK		  = 1;
const int REPLY_ERROR	  = 2;


namespace ecoopsc
{

class ECoopSC;

class Server
{
private:
	ECoopSC& ecoopsc;
	TcpIostreamPtr stream;

public:
	Server(const Server& that) :
		ecoopsc(that.ecoopsc), stream(that.stream)
	{

	}

	Server(ECoopSC& _ecoopsc, TcpIostreamPtr _stream) :
		ecoopsc(_ecoopsc), stream(_stream)
	{
	}

	void operator()();
};

}

#endif
