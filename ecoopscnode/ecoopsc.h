#ifndef _ECOOPSC_H
#define _ECOOPSC_H
#include <string>
#include "connection.h"
#include "managerclient.h"
#include "listener.h"

namespace ecoopsc
{

class ECoopSC
{
private:
	coopsc::Connection connection;
	int ecoopscport; //ecoopsc port
	std::string ecoopschost;

	std::string mhost;
	int mport;
	ManagerClient mclient;
	Listener listener;

public:
	ECoopSC(const std::string& _dbname, const std::string& _dbhost, int _dbport, //database name, host, port
			const std::string& _dbusr, const std::string& _dbpwd, //user, password
			const std::string& _coopschost, int _coopscport, //coopsc host, port
			unsigned int _cachesize,  //cache size
			const std::string& _chimerahost, int _chimeraport, //chimera host, port
			const std::string& _chimerabshost, int _chimerabsport, //chimera bootstrap host, port
			const std::string& _mhost, int _mport, //management host, port
			const std::string& _ecoopschost, int _ecoopscport); //eCoopSC port
	//disable copy constructor
	ECoopSC(const ECoopSC&)= delete;
	//disable assignment operator
	ECoopSC& operator =(const ECoopSC&)= delete;


	coopsc::Connection& getConnection()
	{
		return connection;
	}


	ManagerClient& getManagerClient()
	{
		return mclient;
	}


	std::string getManagerHost()
	{
		return mhost;
	}

	int getManagerPort()
	{
		return mport;
	}

	std::string getHost()
	{
		return ecoopschost;
	}

	int getPort()
	{
		return ecoopscport;
	}

	void connect();
};

}
#endif
