#include <boost/asio.hpp>
#include <boost/thread.hpp>
#include <boost/archive/binary_oarchive.hpp>
#include <boost/archive/binary_iarchive.hpp>
#include <boost/bind.hpp>
#include "ecoopsc.h"
#include "listener.h"


namespace ecoopsc
{

using namespace boost;
using namespace boost::asio;

void Listener::operator()()
{
	io_service.run();
}

void Listener::handle_accept(Server server, const boost::system::error_code& error)
{
	if (!error)
	{
		boost::thread thread(server);
		TcpIostreamPtr newstream(new ip::tcp::iostream());
		Server newserver(ecoopsc, newstream);
		acceptor->async_accept(*newstream->rdbuf(),
				boost::bind(&Listener::handle_accept, this, newserver, boost::asio::placeholders::error));
	}
}

void Listener::start()
{
	acceptor = AcceptorPtr(new boost::asio::ip::tcp::acceptor(io_service, ip::tcp::endpoint(ip::tcp::v4(), ecoopsc.getPort())));

	TcpIostreamPtr newstream(new ip::tcp::iostream());
	Server newserver(ecoopsc, newstream);

	acceptor->async_accept(*newstream->rdbuf(), boost::bind(&Listener::handle_accept, this, newserver, boost::asio::placeholders::error));
	thread = ThreadPtr(new boost::thread(boost::ref(*this)));
}

void Listener::stop()
{
	acceptor.reset();
	io_service.stop();
	if (thread.get() != NULL)
		thread->join();
}
}
