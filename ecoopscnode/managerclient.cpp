#include <boost/lexical_cast.hpp>
#include <stdexcept>
#include "ecoopsc.h"
#include "managerclient.h"

namespace ecoopsc
{

const int CMD_SELECT_NODE	  	= 1;
const int CMD_SET_IDLE   		= 2;
const int CMD_SET_BUSY	        = 3;
const int CMD_QUIT   	        = 4;

const int REPLY_OK		  = 1;
const int REPLY_ERROR	  = 2;


void ManagerClient::connect()
{
	if (stream.get() != NULL)
		throw std::runtime_error("Already connected!");
	stream = TcpIostreamPtr(new boost::asio::ip::tcp::iostream(ecoopsc.getManagerHost(), boost::lexical_cast<std::string>(ecoopsc.getManagerPort())));
}




void ManagerClient::busy()
{
	boost::mutex::scoped_lock lock(mutex);
	OArchivePtr oa = OArchivePtr(new boost::archive::binary_oarchive(*stream)); //out archive

	std::string host = ecoopsc.getHost();
	int port = ecoopsc.getPort();

	(*oa) << CMD_SET_BUSY << host << port;
	stream->flush();

	int status;
	IArchivePtr ia = IArchivePtr(new boost::archive::binary_iarchive(*stream)); //in archive
	(*ia) >> status;

	if (status != REPLY_OK)
	{
		std::string error;
		(*ia) >> error;
		throw std::runtime_error(error);
	}

}

void ManagerClient::idle()
{
	boost::mutex::scoped_lock lock(mutex);
	OArchivePtr oa = OArchivePtr(new boost::archive::binary_oarchive(*stream)); //out archive
	std::string host = ecoopsc.getHost();
	int port = ecoopsc.getPort();
	(*oa) << CMD_SET_IDLE << host <<  port;
	stream->flush();

	int status;
	IArchivePtr ia = IArchivePtr(new boost::archive::binary_iarchive(*stream)); //in archive
	(*ia) >> status;

	if (status != REPLY_OK)
	{
		std::string error;
		(*ia) >> error;
		throw std::runtime_error(error);
	}
}

}
