#include "ecoopsc.h"

namespace ecoopsc
{

ECoopSC::ECoopSC(const std::string& _dbname, const std::string& _dbhost, int _dbport, //database name, host, port
			const std::string& _dbusr, const std::string& _dbpwd, //user, password
			const std::string& _coopschost, int _coopscport, //coopsc host, port
			unsigned int _cachesize,  //cache size
			const std::string& _chimerahost, int _chimeraport, //chimera host, port
			const std::string& _chimerabshost, int _chimerabsport, //chimera bootstrap host, port
			const std::string& _mhost, int _mport,
			const std::string& _ecoopschost, int _ecoopscport) : //eCoopSC port
				connection(_dbname, _dbhost, _dbport, _dbusr, _dbpwd, _coopschost,_coopscport,
							coopsc::CooperativeCache, _cachesize, _chimerahost, _chimeraport,
							_chimerabshost, _chimerabsport),
				ecoopscport(_ecoopscport), ecoopschost(_ecoopschost), mhost(_mhost), mport(_mport),
				mclient(*this), listener(*this)

{
	connection.setCachePeerData(false);
}

void ECoopSC::connect()
{
	connection.connect();
	listener.start();
	mclient.connect();
	mclient.idle();
}

}
