#include <iostream>
#include <coopsc.h>
#include <boost/program_options.hpp>
#include <boost/asio.hpp>
#include "ecoopsc.h"

using namespace coopsc;
using namespace std;
using namespace boost;
using namespace boost::program_options;
using namespace asio;



string database; // the database
string user; // the user
string password; // the password
string host; // database host name
unsigned int port; // database port

string coopschost; // coopsc host on which the coopsc
unsigned int coopscport; // cache manager port



string manhost; // coopsc host on which the coopsc
unsigned int manport; // cache manager port




int chport; //chimera port
string chhost; //chimera bootstrap host
string chboothost; //chimera bootstrap host
int chbootport; //chimera bootstrap port


std::string ecoopschost; //ecoopsc host
int ecoopscport; //ecoopsc port


int cachesize;



// Return the local ip of the computer
string getLocalIp()
{
	static std::string localip;
	if (localip != "")
		return localip;
	io_service io_service;
	ip::tcp::resolver resolver(io_service);
	std::cout << "host name : " << ip::host_name() << std::endl;
	ip::tcp::resolver::query query(ip::host_name(), "");

	ip::tcp::resolver::iterator it = resolver.resolve(query);
	std::cout << "after resolve" << std::endl;
	string result = "127.0.0.1";
	while (it != ip::tcp::resolver::iterator())
	{
		ip::address addr = (it++)->endpoint().address();
		if (addr.is_v4())
		{
			string tmp = addr.to_string();
			if (tmp.size() < 4)
				continue;
			if (tmp[0] == '1' && tmp[1] == '2' && tmp[2] == '7')
				continue;
			std::cout << "ip :" << tmp << std::endl;
			result = tmp;
			//			break;
		}
	}
	localip = result;
	return result;
}




//parses the arguments;
//fills the global variables
void parseArgs(int argc, char **argv)
{
	options_description desc("Arguments");
	desc.add_options()
		("help", "produce help message")
 		("host", value<string> (), "database host")
		("port", value<int> ()->default_value(5432), "database port")
		("database", value<string> (), "database name")
		("user", value<string> (), "database user")
		("password", value<string> (), "database password")
		("coopschost", value<string> ()->default_value("127.0.0.1"), "coopsc host")
		("coopscport", value<unsigned int> ()->default_value(1999),	"coopsc port")
		("cachesize", value<unsigned int> ()->default_value(128), "cache size (in megabytes)")
		("chhost", value<string> ()->default_value("127.0.0.1"), "chimera host")
		("chport", value<unsigned int> ()->default_value(9999), "chimera port")
		("chboothost", value<string> ()->default_value(""), "chimera bootstrap host")
		("chbootport", value<unsigned int> ()->default_value(9999), "chimera bootstrap port")
		("manhost", value<string> ()->default_value(""), "management host")
		("manport", value<unsigned int> ()->default_value(9999), "management port")
		("ecoopschost", value<string> ()->default_value("127.0.0.1"), "ecoopsc host")
		("ecoopscport", value<unsigned int> ()->default_value(6543), "ecoopsc port");

	variables_map vm;
	store(parse_command_line(argc, argv, desc), vm);
	notify(vm);

	if (vm.count("help"))
	{
		cout << desc << "\n";
		exit(1);
	}

	if (vm.count("host"))
	{
		host = vm["host"].as<string> ();
	}
	else
	{
		cout << "Database host not set!\n";
		exit(1);
	}
	cout << "Database host : " << host << endl;

	if (vm.count("port"))
	{
		port = vm["port"].as<int> ();
	}
	else
	{
		cerr << "Database port not set!\n";
		exit(1);
	}
	cout << "Database port : " << port << endl;

	if (vm.count("database"))
	{
		database = vm["database"].as<string> ();
	}
	else
	{
		cerr << "Database name not set!\n";
		exit(1);
	}
	cout << "Database name : " << database << endl;

	if (vm.count("user"))
	{
		user = vm["user"].as<string> ();
	}
	else
	{
		cerr << "User name not set!\n";
		exit(1);
	}
	cout << "User name : " << user << endl;

	if (vm.count("password"))
	{
		password = vm["password"].as<string> ();
	}
	else
	{
		cerr << "Password not set!\n";
		exit(1);
	}
	cout << "Password : " << password << endl;

	if (vm.count("coopschost"))
	{
		coopschost = vm["coopschost"].as<string> ();
	}
	else
	{
		cerr << "CoopSC host not set!\n";
		exit(1);
	}
	cout << "CoopSC host : " << coopschost << endl;

	if (vm.count("coopscport"))
	{
		coopscport = vm["coopscport"].as<unsigned int> ();
	}
	else
	{
		cerr << "CoopSC port not set!\n";
		exit(1);
	}
	cout << "CoopSC port : " << coopscport << endl;


	if (vm.count("cachesize"))
	{
		cachesize = vm["cachesize"].as<unsigned int> ();
	}
	else
	{
		cerr << "Cache size not set!\n";
		exit(1);
	}
	cout << "Cache size : " << cachesize << " Mb" << endl;

	if (vm.count("chhost"))
	{
		chhost = vm["chhost"].as<string> ();
	}
	else
	{
		cerr << "Chimera host not set!\n";
		exit(1);
	}
	cout << "Chimera host : " << chhost << endl;

	if (vm.count("chport"))
	{
		chport = vm["chport"].as<unsigned int> ();
	}
	else
	{
		cerr << "Chimera port not set!\n";
		exit(1);
	}
	cout << "Chimera port : " << chport << endl;

	if (vm.count("chboothost"))
	{
		chboothost = vm["chboothost"].as<string> ();
	}

	if (vm.count("chbootport"))
	{
		chbootport = vm["chbootport"].as<unsigned int> ();
	}
	else
	{
		if (chboothost != "")
		{
			cerr << "Chimera bootstrap port not set!\n";
			exit(1);
		}
	}

	cout << "Chimera bootstrap host : " << chboothost << ":" << chbootport << endl;


	if (vm.count("manhost"))
	{
		cout << "before" << endl;
		manhost = vm["manhost"].as<string> ();
		cout << "after" << endl;
	}
	else
	{
		cerr << "Management host not set!\n";
		exit(1);
	}


	if (vm.count("manport"))
	{
		cout << "before" << endl;
		manport = vm["manport"].as<unsigned int> ();
		cout << "after" << endl;
	}
	else
	{
		cerr << "Management port not set!\n";
		exit(1);
	}

	cout << "Manager host : " << manhost << ":" << manport << endl;


	if (vm.count("ecoopschost"))
	{
		ecoopschost = vm["ecoopschost"].as<string> ();
	}
	else
	{
		cerr << "eCoopSC host not set!\n";
		exit(1);
	}

	if (vm.count("ecoopscport"))
	{
		ecoopscport = vm["ecoopscport"].as<unsigned int> ();
	}
	else
	{
		cerr << "eCoopSC port not set!\n";
		exit(1);
	}
	cout << "eCoopSC host : " << ecoopschost << ":" << ecoopscport << endl;

}


int main(int argc, char **argv)
{
	INIT_LOGGER(std::cout);
	parseArgs(argc, argv);
	ecoopsc::ECoopSC ecoopsc(database, host, port,
							user, password, //user, password
							coopschost, coopscport, //coopsc host, port
							cachesize * 1024 * 1024,  //cache size
							chhost, chport, //chimera host, port
							chboothost, chbootport, //chimera bootstrap host, port,
							manhost, manport,
							ecoopschost, ecoopscport); //eCoopSC port
	ecoopsc.connect();
	while (true)
	{
		sleep(2000);
	}
}

