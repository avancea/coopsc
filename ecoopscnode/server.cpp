#include "server.h"
#include "ecoopsc.h"

typedef boost::shared_ptr<boost::archive::binary_oarchive> OArchivePtr;
typedef boost::shared_ptr<boost::archive::binary_iarchive> IArchivePtr;

using boost::asio::ip::tcp;

namespace ecoopsc
{

void Server::operator()()
{
	OArchivePtr oa;
	IArchivePtr ia;
	int status = REPLY_OK;
	try
	{
		for (;;)
		{
			ia = IArchivePtr(new boost::archive::binary_iarchive(*stream));
			int command;
			(*ia) >> command;


			oa = OArchivePtr(new boost::archive::binary_oarchive(*stream));

			if (command == CMD_QUIT) break;
			if (command == CMD_QUERY)
			{
				std::string query; (*ia) >> query;
				try {

					ecoopsc.getManagerClient().busy();
					coopsc::ResultSetPtr result;
					result = ecoopsc.getConnection().query(query);

					status = REPLY_OK; (*oa) << status; stream->flush();
					ecoopsc.getManagerClient().idle();

					//sending result set
					int nofields = result->getNoFields();
					(*oa) << nofields;
					for (int i = 0; i < nofields; i++)
					{
						std::string fieldname = result->getFieldName(i);
						int fieldsize = result->getFieldSize(i);
						std::string fieldtype = result->getFieldType(i);
						(*oa) << fieldname << fieldsize << fieldtype;
					}

					int notuples = result->getNoTuples();
					(*oa) << notuples;
					for (int i = 0; i < notuples; i++)
						for (int j = 0; j < nofields; j++)
						{
							std::string value = result->getValue(i, j);
							(*oa) << value;
						}

					stream->flush();
				} catch (std::exception& e)
				{
					LOG_ERROR("exception :" << e.what());
					ecoopsc.getManagerClient().idle();
					status = REPLY_ERROR; std::string error = e.what();
					(*oa) << status; stream->flush();
					(*oa) << error;	stream->flush();
					continue;
				}
				continue;
			}
			if (command == CMD_EXECUTE)
			{
				std::string query; (*ia) >> query;
				try {
					ecoopsc.getManagerClient().busy();
					ecoopsc.getConnection().execute(query);
					ecoopsc.getManagerClient().idle();
					status = REPLY_OK; (*oa) << status; stream->flush();
				} catch (std::exception& e)
				{
					ecoopsc.getManagerClient().idle();
					status = REPLY_ERROR; std::string error = e.what();
					(*oa) << status; stream->flush();
					(*oa) << error;	stream->flush();
					continue;
				}
				continue;
			}
		}

	}
	catch (std::exception& e)
	{
		LOG_ERROR("Exception: " << e.what());
	}
	catch (...)
	{
		LOG_ERROR("Communication error! Thread stopped!");
	}
}

}
