#ifndef LISTENER_H_
#define LISTENER_H_
#include <string>
#include <boost/smart_ptr.hpp>
#include <boost/thread.hpp>
#include <boost/asio.hpp>
#include "server.h"

namespace ecoopsc
{

class ECoopSC;

//listen for connections from clients
class Listener
{
private:
	typedef boost::shared_ptr<boost::thread> ThreadPtr;

	typedef boost::shared_ptr<boost::asio::ip::tcp::acceptor> AcceptorPtr;

	ECoopSC& ecoopsc;

	AcceptorPtr acceptor;

	boost::asio::io_service io_service;

	//acceptor thread
	ThreadPtr thread;

	void handle_accept(Server server, const boost::system::error_code& error);

public:
	Listener(ECoopSC& _ecoopsc): ecoopsc(_ecoopsc)
	{
	}

	//no copy constructor
	Listener(const Listener&)= delete;

	//no assignment operator
	Listener& operator =(const Listener&)= delete;

	~Listener()
	{
		stop();
	}

	void operator()();
	void start();
	void stop();

};

}

#endif /* LISTENER_H_ */
