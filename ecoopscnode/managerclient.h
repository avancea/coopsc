/*
 * managerclient.h
 *
 *  Created on: Aug 5, 2011
 *      Author: andrei
 */

#ifndef MANAGERCLIENT_H_
#define MANAGERCLIENT_H_
#include <string>
#include <utility>
#include <boost/asio.hpp>
#include <boost/smart_ptr.hpp>
#include <boost/function.hpp>
#include <boost/archive/binary_oarchive.hpp>
#include <boost/archive/binary_iarchive.hpp>


namespace ecoopsc
{


class ECoopSC;

class ManagerClient
{
private:
	typedef boost::shared_ptr<boost::asio::ip::tcp::iostream> TcpIostreamPtr;
	typedef boost::shared_ptr<boost::archive::binary_oarchive> OArchivePtr;
	typedef boost::shared_ptr<boost::archive::binary_iarchive> IArchivePtr;
	TcpIostreamPtr stream;

	ECoopSC &ecoopsc;

	std::string host;
	int port;


	boost::mutex mutex;

public:
	ManagerClient(ECoopSC &_ecoopsc): ecoopsc(_ecoopsc)
	{

	}

	void connect();

	void busy();
	void idle();

};


}

#endif /* MANAGERCLIENT_H_ */
