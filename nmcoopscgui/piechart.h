#ifndef _PIECHART_H
#define _PIECHART_H

#include <QtGui>
#include <QWidget>
#include "coopscmodel.h"

class PieChart : public QWidget
{
    Q_OBJECT

private:
 	CoopSCModel& model;
public:
    PieChart(CoopSCModel& _model, QWidget *parent = 0);
    void paintEvent(QPaintEvent *e);
};

 #endif
