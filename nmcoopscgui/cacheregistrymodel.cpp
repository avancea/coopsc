#include <sstream>
#include "cacheregistrymodel.h"

int CacheRegistryModel::rowCount(const QModelIndex &parent) const
{
	if (storagemanager == NULL)
		return 0;
	return storagemanager->getRegions().size();
}

int CacheRegistryModel::columnCount(const QModelIndex &parent) const
{
	return 5;
}

QVariant CacheRegistryModel::data(const QModelIndex &index, int role) const
{
	if (role != Qt::DisplayRole)
		return QVariant();
	if (!index.isValid())
        return QVariant();
	if (storagemanager == NULL)
        return QVariant();
	if (index.row() >= ids.size())
        return QVariant();
	auto it = storagemanager->getRegions().begin();

	int id = ids[index.row()];
	coopsc::RegionPtr reg;
	try {
		 reg = storagemanager->getRegion(id);
	} catch (...)
	{
        return QVariant();
	}

	QString tmp;
	switch (index.column())
	{
	case 0 :
		tmp.setNum(id);
		return tmp;

	case 1 :
		return QString(reg->getQuery().getTableName().c_str());

	case 2 :
		return QString(getFieldsDescription(reg).c_str());

	case 3 :
		{
			std::stringstream ss;
			ss << reg->getQuery().getPredicate();
			return QString(ss.str().c_str());
		}

	case 4 :
		tmp.setNum(reg->getNoTuples());
		return tmp;
	}
}

QVariant CacheRegistryModel::headerData(int section, Qt::Orientation orientation, int role) const
{
	if (storagemanager == NULL) return QVariant();
    if (role != Qt::DisplayRole)
        return QVariant();
    if (orientation == Qt::Horizontal)
    {
    	switch (section)
    	{
    	case 0 : return QString("Id");
    	case 1 : return QString("Table");
    	case 2 : return QString("Fields");
    	case 3 : return QString("Predicate");
    	case 4 : return QString("Size");
    	default : return QVariant();
    	}
    }
    return QVariant();
}

Qt::ItemFlags CacheRegistryModel::flags(const QModelIndex & index ) const
{
	return Qt::ItemIsSelectable | Qt::ItemIsEnabled;;
}


void CacheRegistryModel::refresh()
{
	ids.clear();
	if (storagemanager == NULL)
		return;
	for (auto it = storagemanager->getRegions().begin(); it != storagemanager->getRegions().end(); it++)
		ids.push_back(it->first);
	reset();
}

std::string CacheRegistryModel::getFieldsDescription(coopsc::RegionPtr reg) const
{
	coopsc::FieldsMask fieldsmask = reg->getQuery().getFields();
	std::string tablename = reg->getQuery().getTableName();
	const coopsc::Table& table = metadataregistry->getTable(tablename);

	std::string fields = "";

	//all fields
	if (fieldsmask == table.getFieldsMask())
	{
		fields = "*";
	}
	else
	{
		for (unsigned int i = 0; fieldsmask; i++)
			if (fieldsmask & (1 << i))
			{
				if (fields != "")
					fields += ",";
				fields += table.getField(i).getName();
				fieldsmask &= ~(1 << i);
			}
	}
	return fields;
}


void CacheRegistryModel::clear()
{
	storagemanager->clear();
	refresh();
}
