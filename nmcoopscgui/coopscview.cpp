#include "coopscview.h"
#include <QToolBar>
#include <QStyle>
#include <QLabel>
#include <QErrorMessage>
#include <QStatusBar>
#include <QSplitter>
#include <boost/format.hpp>

const char* WINDOW_TITLE = "MMCoopSC GUI";
const char* FLOWS_SELECT = "select * from flows where %1% < starttime and starttime <  %2%";


CoopSCView::CoopSCView(CoopSCModel& _model) :
	QMainWindow(0), model(_model), controller(_model, *this), connectDialog(_model, *this), cacheRegistryDialog(
			model.getCacheRegistryModel(), *this)
{

	connect(&model, SIGNAL(modelUpdate()), this, SLOT(refreshView()));
	setWindowTitle(WINDOW_TITLE);
	resize(1024, 768);

	QToolBar *toolbar = addToolBar("Toolbar");

	executeAction = toolbar->addAction(QIcon(":/icons/execute.png"), "Execute");
	connectAction = toolbar->addAction(QIcon(":/icons/connect.png"), "Connect");
	disconnectAction = toolbar->addAction(QIcon(":/icons/disconnect.png"), "Disconnect");
	cacheregistryAction = toolbar->addAction(QIcon(":/icons/cacheregistry.png"), "Cache Registry");
	quitAction = toolbar->addAction(QIcon(":/icons/quit.png"), "Quit");

	connect(executeAction, SIGNAL(triggered()), &controller, SLOT(execute()));
	connect(connectAction, SIGNAL(triggered()), &controller, SLOT(connect()));
	connect(disconnectAction, SIGNAL(triggered()), &controller, SLOT(disconnect()));
	connect(cacheregistryAction, SIGNAL(triggered()), &controller, SLOT(showCacheRegistry()));
	connect(quitAction, SIGNAL(triggered()), &controller, SLOT(quit()));


	QGridLayout *startend = new QGridLayout();
	startTime = new QDateTimeEdit(); startTime->setCalendarPopup(true);
	startTime->setTimeSpec(Qt::UTC);

	duration = new QSpinBox();
	duration->setRange(1, 20);
	duration->setSizeIncrement(1, 1);
	duration->setValue(5);

	startend->addWidget(new QLabel("Start Time"), 0, 0);
	startend->addWidget(new QLabel("Duration (in minutes)"), 0, 1);
	startend->addWidget(startTime, 1, 0);
	startend->addWidget(duration, 1, 1);



	queryResult = new QTableView();
	queryResult->setModel(&model.getQueryResultModel());


	queryTree = new QTreeView();
	queryTree->setModel(&model.getQueryTreeModel());

	pieChart = new PieChart(model);

	connect(&model.getQueryResultModel(), SIGNAL(modelReset()), this, SLOT(refreshStatusBar()));
	connect(&model.getQueryResultModel(), SIGNAL(modelReset()), this, SLOT(refreshPieChart()));
	connect(&model.getQueryResultModel(), SIGNAL(modelReset()), queryResult, SLOT(resizeColumnsToContents()));

	connect(&model.getQueryTreeModel(), SIGNAL(modelReset()), queryTree, SLOT(expandAll()));
	connect(&model.getQueryTreeModel(), SIGNAL(modelReset()), this, SLOT(resizeQueryTreeColums()));


	QSplitter* vsplitter = new QSplitter(Qt::Vertical);

	QWidget *w = new QWidget();
	w->setLayout(startend);
	vsplitter->addWidget(w);
	vsplitter->addWidget(queryResult);
	QList<int> sizes; sizes << 150 << 850;
	vsplitter->setSizes(sizes);


	QSplitter* vsplitter2 = new QSplitter(Qt::Vertical);
	vsplitter2->addWidget(queryTree);
	vsplitter2->addWidget(pieChart);
	sizes.clear(); sizes << 600 << 400;
	vsplitter2->setSizes(sizes);


	QSplitter* hsplitter = new QSplitter(Qt::Horizontal);
	hsplitter->addWidget(vsplitter);
	hsplitter->addWidget(vsplitter2);

	sizes.clear(); sizes << 350 << 250;
	hsplitter->setSizes(sizes);

	setCentralWidget(hsplitter);
	refreshView();
}

CoopSCView::~CoopSCView()
{
}

std::string CoopSCView::getSql()
{


	QDateTime dt = startTime->dateTime().toTimeSpec(Qt::UTC);
	long long start = dt.toTime_t();
	long long end = start + 60 * duration->value();


	std::string sql =  (boost::format(FLOWS_SELECT) % (start - 1) %  (end + 1)).str();

	std::cout << "sql : " << sql << std::endl;

	return sql;

}

void CoopSCView::errorMessage(const std::string& msg)
{
	QErrorMessage errorMessage;
	errorMessage.showMessage(msg.c_str());
	errorMessage.exec();
}

void CoopSCView::setStatus(const std::string& status)
{
	statusBar()->showMessage(status.c_str());
}

void CoopSCView::refreshStatusBar()
{
	std::stringstream ss;
	if (model.getQueryResultModel().getResult().get() != NULL)
	{
		ss << model.getQueryResultModel().getResult()->getNoTuples() << " rows";
		setStatus(ss.str());
	}
	else
		setStatus("");
}


void CoopSCView::refreshPieChart()
{
	pieChart->repaint();
}


void CoopSCView::refreshView()
{
	if (model.connected())
	{
		executeAction-> setEnabled(true);
		connectAction->setEnabled(false);
		disconnectAction->setEnabled(true);
		cacheregistryAction->setEnabled(true);
		quitAction->setEnabled(true);
		centralWidget()->setEnabled(true);
		pieChart->setEnabled(true);
		std::stringstream ss;
		ss << WINDOW_TITLE << " - " << model.getCoopSCHost() << ":" << model.getCoopSCPort();
		setWindowTitle(ss.str().c_str());
		setStatus("Connected");

		QDateTime mindt; mindt.setTimeSpec(Qt::UTC);
		mindt.setTime_t(model.getMinTime());

		std::cout << "min time : " << mindt.toString(Qt::ISODate).toStdString() << " " << model.getMinTime() << " " << std::endl;


		startTime->setMinimumDateTime(mindt);
		startTime->setMaximumDateTime(QDateTime::fromTime_t(model.getMaxTime()));

		startTime->setDateTime(mindt);

	}
	else
	{
		executeAction->setEnabled(false);
		connectAction->setEnabled(true);
		disconnectAction->setEnabled(false);
		cacheregistryAction->setEnabled(false);
		quitAction->setEnabled(true);
		centralWidget()->setEnabled(false);
		setWindowTitle(WINDOW_TITLE);
		pieChart->setEnabled(false);
		setStatus("Disconnected");
	}
}

void CoopSCView::resizeQueryTreeColums()
{
	queryTree->resizeColumnToContents(0);
	queryTree->resizeColumnToContents(1);
}
