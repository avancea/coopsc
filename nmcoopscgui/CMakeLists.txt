cmake_minimum_required(VERSION 2.8)

include_directories (${CoopSC_SOURCE_DIR}/chimera)
include_directories (${CoopSC_SOURCE_DIR}/coopsc)

file (GLOB nmcoopscgui_SRCS *.cpp) 

file (GLOB nmcoopscgui_MOC_HDRS *.h) 


set (nmcoopscgui_UIS

)

set (nmcoopscgui_RCS
	coopsc.qrc
)



find_package (Boost 1.40 COMPONENTS program_options thread system signals iostreams serialization REQUIRED)
link_directories (${Boost_LIBRARY_DIRS})
include_directories (${Boost_INCLUDE_DIRS})

find_package (OpenSSL REQUIRED)
link_directories (${OpenSSL_LIBRARY_DIRS})
include_directories (${OpenSSL_INCLUDE_DIRS})


find_package (PG REQUIRED)
include_directories (${PG_INCLUDE_DIRS})
link_directories (${PG_LIBRARY_DIRS})


find_package (Qt4 REQUIRED QtCore QtGui)
include ( ${QT_USE_FILE} )

QT4_ADD_RESOURCES (nmcoopscgui_RC_SRCS ${nmcoopscgui_RCS})
  
# this will run uic on .ui files:
QT4_WRAP_UI (nmcoopscgui_UI_HDRS ${nmcoopscgui_UIS})
  
# and finally this will run moc:
QT4_WRAP_CPP (nmcoopscgui_MOC_SRCS ${nmcoopscgui_MOC_HDRS})

INCLUDE_DIRECTORIES( ${CMAKE_BINARY_DIR} )


add_executable (nmcoopscgui ${nmcoopscgui_SRCS}  ${nmcoopscgui_MOC_SRCS} ${nmcoopscgui_RC_SRCS} ${nmcoopscgui_UI_HDRS})
target_link_libraries (nmcoopscgui coopsc)  
target_link_libraries (nmcoopscgui ${PG_LIBRARIES}) 
target_link_libraries (nmcoopscgui ${QT_LIBRARIES}) 
target_link_libraries (nmcoopscgui ${Boost_LIBRARIES})
target_link_libraries (nmcoopscgui ${OPENSSL_LIBRARIES})
target_link_libraries (nmcoopscgui chimera)  
target_link_libraries (nmcoopscgui crypto)
