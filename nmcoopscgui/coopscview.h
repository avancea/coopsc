#ifndef COOPSWINDOW_H_
#define COOPSWINDOW_H_


#include <QApplication>
#include <QWidget>
#include <QAction>
#include <QMainWindow>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QTextEdit>
#include <QDateTimeEdit>
#include <QTableView>
#include <QTreeView>
#include "coopscmodel.h"
#include "connectdialog.h"
#include "cacheregistrydialog.h"
#include "coopsccontroller.h"
#include "piechart.h"


class CoopSCView : public QMainWindow
{
	Q_OBJECT
private :
	CoopSCModel& model;
	CoopSCController controller;

	ConnectDialog connectDialog;
	CacheRegistryDialog cacheRegistryDialog;

	QAction* executeAction;
	QAction* connectAction;
	QAction* disconnectAction;
	QAction* cacheregistryAction;
	QAction* quitAction;


	QTableView *queryResult;

	QDateTimeEdit* startTime;
	QSpinBox* duration;

	PieChart* pieChart;


	QTreeView* queryTree;
public :
	CoopSCView (CoopSCModel& _model);
	~CoopSCView();

	//returns the sql query typed in the text box
	std::string getSql();

	//displays an error message
	void errorMessage(const std::string& msg);

	//called when the model in changed
	void update();

	//sets the status message;
	void setStatus(const std::string& status);

	ConnectDialog& getConnectDialog()
	{
		return connectDialog;
	}

	CacheRegistryDialog& getCacheRegistryDialog()
	{
		return cacheRegistryDialog;
	}
public slots :
	void refreshStatusBar();
	void refreshPieChart();
	void refreshView();
	void resizeQueryTreeColums();
};

#endif /* COOPSWINDOW_H_ */
