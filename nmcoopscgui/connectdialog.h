#ifndef CONNECTDIALOG_H_
#define CONNECTDIALOG_H_
#include <QDialog>
#include <QLabel>
#include <QPushButton>
#include <QDialogButtonBox>
#include <QLineEdit>
#include <QSpinBox>
#include <QComboBox>
#include "coopscmodel.h"
class CoopSCView;

class ConnectDialog: public QDialog
{
private :
	CoopSCModel& model;
	CoopSCView& view;

	QLineEdit* entrydbname;
	QLineEdit* entrydbhost;
	QSpinBox* entrydbport;
	QLineEdit* entrydbusr;
	QLineEdit* entrydbpwd;
	QLineEdit* entrycoopschost;
	QSpinBox* entrycoopscport;
	QComboBox* combocachelevel;
	QSpinBox* entrycachesize;
	QSpinBox* entrychimeraport;
	QLineEdit* entrychimerabshost;
	QSpinBox* entrychimerabsport;
public :
	ConnectDialog(CoopSCModel& _model, CoopSCView& _view);
	virtual ~ConnectDialog()
	{

	}

	//returns the database name
	const std::string getDbName() const
	{
		return entrydbname->text().toStdString();
	}

	//returns the database host
	const std::string getDbHost() const
	{
		return entrydbhost->text().toStdString();
	}

	//returns the database port
	int getDbPort() const
	{
		return entrydbport->value();
	}

	//returns the database user name
	const std::string getDbUsr() const
	{
		return entrydbusr->text().toStdString();
	}

	//returns the database password
	const std::string getDbPwd() const
	{
		return entrydbpwd->text().toStdString();
	}

	//returns the cache manager host
	const std::string getCoopSCHost() const
	{
		return entrycoopschost->text().toStdString();
	}

	//returns the cache manager port
	int getCoopSCPort() const
	{
		return entrycoopscport->value();
	}

	//returns the size of the cache
	int getCacheSize()
	{
		return entrycachesize->value();
	}

	//returns the cache level
	int getCacheLevel()
	{
		return combocachelevel->currentIndex();
	}

	int getChimeraPort()
	{
		return entrychimeraport->value();
	}

	std::string getChimeraBootstrapHost()
	{
		return entrychimerabshost->text().toStdString();
	}

	int getChimeraBootstrapPort()
	{
		return entrychimerabsport->value();
	}

};

#endif /* CONNECTDIALOG_H_ */
