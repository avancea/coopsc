#include "piechart.h"
#include "nightcharts.h"

PieChart::PieChart(CoopSCModel& _model, QWidget *parent)
    : QWidget(parent), model(_model)
{
}


void PieChart::paintEvent(QPaintEvent *e)
{
	QWidget::paintEvent(e);
	QPainter painter;
	QFont font;
	font.setPixelSize(8);
	painter.begin(this);
	Nightcharts chart;
	chart.setFont(font);
	chart.setType(Nightcharts::Pie);//{Histogramm,Pie,DPie};
	chart.setLegendType(Nightcharts::Round);//{Round,Vertical}
	chart.setCords(this->width() / 3, this->height() / 3,this->width() / 3,this->height() / 3);

	std::map<std::string, int> stats = model.getStats();

	Qt::GlobalColor colors[] = {Qt::red, Qt::green, Qt::blue, Qt::cyan, Qt::magenta, Qt::yellow, Qt::gray};

	int k = 0;
	for (auto it = stats.begin(); it != stats.end(); it++)
	{
		if (k >= sizeof(colors)/sizeof(colors[0])) continue;
		chart.addPiece(QString(it->first.c_str()), colors[k], it->second);
		k++;
	}
	chart.draw(&painter);
	chart.drawLegend(&painter);
}
