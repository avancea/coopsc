#ifndef CACHEREGISTRYMODEL_H_
#define CACHEREGISTRYMODEL_H_
#include <string>
#include <vector>
#include <coopsc.h>
#include <QAbstractTableModel>


class CacheRegistryModel: public QAbstractTableModel
{
	Q_OBJECT
private:
	coopsc::StorageManager* storagemanager;
	coopsc::MetadataRegistry* metadataregistry;
	std::vector<int> ids;
	std::string getFieldsDescription(coopsc::RegionPtr reg) const;
public:
	CacheRegistryModel()
	{
		storagemanager = NULL;
	}

    int rowCount(const QModelIndex &parent) const;
    int columnCount(const QModelIndex &parent) const;
    QVariant data(const QModelIndex &index, int role) const;
    QVariant headerData(int section, Qt::Orientation orientation, int role) const;
    Qt::ItemFlags flags(const QModelIndex & index ) const;

	void setStorageManager(coopsc::StorageManager* _storagemanager)
	{
		storagemanager = _storagemanager;
	}

	void setMetadataRegistry(coopsc::MetadataRegistry* _metadataregistry)
	{
		metadataregistry = _metadataregistry;
	}

	void refresh();

	virtual ~CacheRegistryModel()
	{

	}

public slots:
	void clear();
};

#endif /* CACHEREGISTRYMODEL_H_ */
