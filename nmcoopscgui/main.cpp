#include <iostream>
#include "logger.h"
#include "coopscmodel.h"
#include "coopscview.h"
#include <QApplication>

int main(int argc, char *argv[])
{
	INIT_LOGGER(std::cout);

	QApplication app(argc, argv);
	CoopSCModel model;
	CoopSCView view(model);
	view.show();
	return app.exec();
}
