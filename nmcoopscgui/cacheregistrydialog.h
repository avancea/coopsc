#ifndef CACHEREGISTRYDIALOG_H_
#define CACHEREGISTRYDIALOG_H_

#include <QDialog>
#include <QPushButton>
#include <QDialogButtonBox>
#include <QTableView>

#include "cacheregistrymodel.h"

class CoopSCView;

class CacheRegistryDialog: public QDialog
{
private:
	CacheRegistryModel& model;
	CoopSCView& view;

	QTableView* cacheregistryview;

public:
	CacheRegistryDialog(CacheRegistryModel& _model, CoopSCView& _view);
	void update();
	virtual ~CacheRegistryDialog();
};

#endif /* CACHEREGISTRYDIALOG_H_ */
