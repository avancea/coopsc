#include <boost/lexical_cast.hpp>
#include <QDateTime>
#include "queryresultmodel.h"

QueryResultModel::QueryResultModel()
{
}

QueryResultModel::~QueryResultModel()
{
}

void QueryResultModel::setResult(coopsc::ResultSetPtr _result)
{
	result = _result;
	reset();
}

coopsc::ResultSetPtr QueryResultModel::getResult()
{
	return result;
}

int QueryResultModel::rowCount(const QModelIndex &parent) const
{
	if (result.get() == NULL)
		return 0;
	return result->getNoTuples();
}

int QueryResultModel::columnCount(const QModelIndex &parent) const
{
	if (result.get() == NULL)
		return 0;
	return result->getNoFields() - 1;
}


std::string intToIP(int ip)
{
	unsigned int x = (unsigned int)ip;
	int b0, b1, b2, b3;
	b0 = x & 0xff; x = x >> 8;
	b1 = x & 0xff; x = x >> 8;
	b2 = x & 0xff; x = x >> 8;
	b3 = x & 0xff; x = x >> 8;

	std::stringstream ss;
	ss << b3 << "." << b2 << "." << b1 << "." << b0;
	return ss.str();

}


QVariant QueryResultModel::data(const QModelIndex &index, int role) const
{
	if (role != Qt::DisplayRole)
		return QVariant();
	if (!index.isValid())
		return QVariant();
	if (result.get() == NULL)
		return QVariant();
	if (index.row() < 0 || index.row() >= result->getNoTuples())
		return QVariant();
	if (index.column() < 0 || index.column() >= result->getNoFields())
		return QVariant();

	std::string r = result->getValue(index.row(), index.column() + 1);

	switch (index.column())
	{
		case 0: //starttime
		{

			QDateTime starttime;
			starttime.setTimeSpec(Qt::UTC);
			starttime.setTime_t(boost::lexical_cast<long long>(r));
			return starttime.toString(Qt::SystemLocaleShortDate);
		}
		case 1: //duration
		{
			int ip = boost::lexical_cast<int>(r);
			return QString::number(ip / 1000.0);

		}

		case 2: //protocol
		{
			int protocol = boost::lexical_cast<int>(r);
			if (protocol == 1) return QString("TCP");
			if (protocol == 2) return QString("UDP");
			if (protocol == 3) return QString("ICMP");
			return QString("?");
		}
		case 3: //srcip
		case 5: //dstip
		{
			int ip = boost::lexical_cast<int>(r);

			return QString(intToIP(ip).c_str());
		}
	}

	return QString(r.c_str());
}

QVariant QueryResultModel::headerData(int section, Qt::Orientation orientation, int role) const
{
	if (result.get() == NULL)
		return QVariant();
	if (role != Qt::DisplayRole)
		return QVariant();
	if (orientation == Qt::Horizontal)
	{
		switch (section)
		{
			case 0: return QString("Start Time");
			case 1: return QString("Duration (s)");
			case 2: return QString("Protocol");
			case 3: return QString("SRC IP");
			case 4: return QString("SRC Port");
			case 5: return QString("DST IP");
			case 6: return QString("DST Port");
			case 7: return QString("Packets");
			case 8: return QString("Bytes");
		}

	}
	return QVariant();
}

Qt::ItemFlags QueryResultModel::flags(const QModelIndex & index) const
{
	return Qt::ItemIsSelectable | Qt::ItemIsEnabled;
}
