#include <QTableView>
#include <QHBoxLayout>
#include "cacheregistrydialog.h"
#include "coopscview.h"

CacheRegistryDialog::CacheRegistryDialog(CacheRegistryModel& _model, CoopSCView& _view) :
	QDialog(&_view), model(_model), view(_view)
{
	setWindowTitle("Cache Registry");
	resize(600, 250);
	cacheregistryview = new QTableView();
	cacheregistryview->setModel(&model);

	QDialogButtonBox* buttonBox = new QDialogButtonBox(Qt::Horizontal);
	buttonBox->addButton(QDialogButtonBox::Ok);
	QPushButton* bclear = buttonBox->addButton("Clear", QDialogButtonBox::ActionRole);

	connect(buttonBox, SIGNAL(accepted()), this, SLOT(accept()));
	connect(bclear, SIGNAL(clicked()), &model, SLOT(clear()));

	connect(&model, SIGNAL(modelReset()), cacheregistryview, SLOT(resizeColumnsToContents()));

	QVBoxLayout *bl = new QVBoxLayout();
	bl->addWidget(cacheregistryview);
	bl->addWidget(buttonBox);

	setLayout(bl);

}

CacheRegistryDialog::~CacheRegistryDialog()
{
	// TODO Auto-generated destructor stub
}

