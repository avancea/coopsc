#include <iostream>
#include <boost/foreach.hpp>
#include <boost/asio.hpp>
#include <boost/lexical_cast.hpp>
#include <cctype>
#include "coopscmodel.h"
#include "logger.h"

using namespace boost::asio;

// Return the local ip of the computer
std::string CoopSCModel::getLocalIp()
{
	try
	{
		io_service io_service;
		ip::tcp::resolver resolver(io_service);
		ip::tcp::resolver::query query(ip::host_name(), "");
		ip::tcp::resolver::iterator it = resolver.resolve(query);

		while (it != ip::tcp::resolver::iterator())
		{
			ip::address addr = (it++)->endpoint().address();
			if (addr.is_v4())
				return addr.to_string();
		}
	} catch (...)
	{
	}
	return "127.0.0.1";
}

void CoopSCModel::execute(const std::string& sql)
{

	connection->getStatisticsRegistry().clear();
	connection->getStatisticsRegistry().start();

	coopsc::ResultSetPtr result;

	//first, determining the type of query (select, delete, update, insert)
	std::string type;
	BOOST_FOREACH(char c, sql)
	{
		if (isspace(c))
		{
			if (type.empty())
				continue;
			else
				break;
		}
		type += toupper(c);
	}
	queryResultModel.setResult(result);
	if (type == "SELECT")
	{
		result = connection->query(sql);
		queryTreeModel.setQueryPlan(connection->getLastQueryPlan());
		queryResultModel.setResult(result);
	}
	else
	{
		connection->execute(sql);
		queryTreeModel.setQueryPlan(coopsc::QueryPlanPtr());
	}
	connection->getStatisticsRegistry().stop();
}

//connects to the database
void CoopSCModel::connect()
{
	try
	{
		connection = boost::shared_ptr<coopsc::Connection>(new coopsc::Connection(dbname, dbhost, dbport, dbusr, dbpwd, coopschost,
				coopscport, (coopsc::CacheType) cachelevel, cachesize * 1024 * 1024, "192.168.0.2", chimeraport, chimerabshost, chimerabsport));
	} catch (std::exception& e)
	{
		LOG_ERROR(e.what());
		throw;
	}
	try
	{
		connection->connect();
	} catch (std::exception& e)
	{
		LOG_ERROR(e.what());
		connection.reset();
		throw;
	}
	cacheRegistryModel.setStorageManager(&connection->getStorageManager());
	cacheRegistryModel.setMetadataRegistry(&connection->getMetadataRegistry());
	queryTreeModel.setQueryPlan(connection->getLastQueryPlan());

	emit modelUpdate();
}



long long CoopSCModel::getMinTime()
{
	coopsc::ResultSetPtr result = connection->query("select min(starttime) from flows");
	std::string value = result->getValue(0, 0);

	return boost::lexical_cast<int>(value);

}


long long CoopSCModel::getMaxTime()
{
	coopsc::ResultSetPtr result = connection->query("select max(starttime) from flows");
	std::string value = result->getValue(0, 0);

	return boost::lexical_cast<int>(value);
}



//disconnects from the database
void CoopSCModel::disconnect()
{
	connection.reset();
	cacheRegistryModel.setStorageManager(NULL);
	queryTreeModel.setQueryPlan(coopsc::QueryPlanPtr());
	emit modelUpdate();
}
//returns true if the connection is established
bool CoopSCModel::connected()
{
	return (connection.get() != NULL);
}



std::map<std::string, int> CoopSCModel::getStats()
{

	std::map<std::string, int> result;

	if (connection.get() == NULL) return result;
	const coopsc::StatisticsRegistry::QueryStat& stat = connection->getStatisticsRegistry().getCurrentStat();

	if (stat.tuplesLocal) result["Local"] = stat.tuplesLocal;
	if (stat.tuplesServer) result["Server"] = stat.tuplesServer;
	for (auto it = stat.tuplesPeers.begin(); it != stat.tuplesPeers.end(); it++)
	{
		std::stringstream ss;
		ss << it->first.first << ":" << it->first.second;
		result[ss.str()] = it->second;
	}
	return result;
}




