#ifndef ECoopSCModel_H_
#define ECoopSCModel_H_

#include <ecoopsc.h>
#include <QObject>
#include <boost/smart_ptr.hpp>
#include "queryresultmodel.h"

class ECoopSCModel: public QObject
{
	Q_OBJECT
private:
	// database name, host, port, user, password

	std::string manhost;
	int manport;

	QueryResultModel queryResultModel;

	boost::shared_ptr<ecoopsc::Connection> connection;

public:
	ECoopSCModel() :
		manport(5432), manhost(ECoopSCModel::getLocalIp())
	{

	}

	//returns the query result model;
	QueryResultModel& getQueryResultModel()
	{
		return queryResultModel;
	}

	//executes a sql query
	void execute(const std::string& sql);

	//connects to the database
	void connect();

	//disconnects from the database
	void disconnect();

	//returns true if the connection is established
	bool connected();

	//returns the database name
	const std::string& getManagerHost() const
	{
		return manhost;
	}

	//sets the database name
	void setManagerHost(const std::string& _manhost)
	{
		manhost = _manhost;
	}


	//returns the database port
	int getManagerPort() const
	{
		return manport;
	}

	//sets the database port
	void setManagerPort(int _manport)
	{
		manport = _manport;
	}


	static std::string getLocalIp();

signals:
    void modelUpdate();

};

#endif /* ECoopSCModel_H_ */
