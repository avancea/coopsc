#include "connectdialog.h"
#include "ecoopscview.h"
#include <QDialogButtonBox>



ConnectDialog::ConnectDialog(ECoopSCModel& _model, ECoopSCView& _view) : QDialog(&_view), model(_model), view(_view)
{
	setWindowTitle("Connect");

	entrymanport = new QSpinBox();
	entrymanport->setRange(1, 65535);
	entrymanport->setSizeIncrement(1, 1);
	entrymanport->setValue(9999);



	entrymanhost = new QLineEdit();
	entrymanhost->setText("127.0.0.1");

	QGridLayout *l = new QGridLayout();
	l->addWidget(new QLabel("Manager Host"), 0, 0);
	l->addWidget(entrymanhost, 0, 1);


	l->addWidget(new QLabel("Manager Port"), 2, 0);
	l->addWidget(entrymanport, 2, 1);



	QDialogButtonBox* buttonBox = new QDialogButtonBox(Qt::Horizontal);

	buttonBox->addButton(QDialogButtonBox::Ok);
	buttonBox->addButton(QDialogButtonBox::Cancel);

    connect(buttonBox, SIGNAL(accepted()), this, SLOT(accept()));
    connect(buttonBox, SIGNAL(rejected()), this, SLOT(reject()));

	QWidget *w = new QWidget();
	w->setLayout(l);

	QVBoxLayout* vl = new QVBoxLayout();
	vl->addWidget(w);
	vl->addWidget(buttonBox);

	vl->setSizeConstraint(QLayout::SetFixedSize);

	setLayout(vl);
}
