#ifndef COOPSCCONTROLLER_H_
#define COOPSCCONTROLLER_H_
#include "ecoopscmodel.h"


#include <QObject>
class ECoopSCView;

class ECoopSCController : public QObject
{
	Q_OBJECT

private:
	ECoopSCModel& model;
	ECoopSCView& view;
public:
	ECoopSCController(ECoopSCModel& _model, ECoopSCView& _view) :
		model(_model), view(_view)
	{

	}
public slots :
	//executes a queries
	void execute();

	//connects to the database
	void connect();

	//disconnects from the database;
	void disconnect();



	//quits the application
	void quit();
};

#endif /* COOPSCCONTROLLER_H_ */
