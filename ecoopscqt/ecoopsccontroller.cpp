#include <stdexcept>
#include <logger.h>
#include "ecoopsccontroller.h"
#include "ecoopscview.h"

//executes a queries
void ECoopSCController::execute()
{
	try
	{
		model.execute(view.getSql());
	} catch (std::exception& e)
	{
		view.errorMessage(e.what());
	}
}

//connects to the database
void ECoopSCController::connect()
{
	int result = view.getConnectDialog().exec();

	if (result != QDialog::Accepted)
		return;


	model.setManagerHost(view.getConnectDialog().getManagerHost());
	model.setManagerPort(view.getConnectDialog().getManagerPort());
	try
	{
		model.connect();
	} catch (std::exception& e)
	{
		view.errorMessage(e.what());
	}
}


//quits the application
void ECoopSCController::quit()
{
	view.close();
}

//disconnects from the database;
void ECoopSCController::disconnect()
{
	model.disconnect();
}
