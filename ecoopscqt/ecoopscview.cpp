#include "ecoopscview.h"
#include <QToolBar>
#include <QStyle>
#include <QLabel>
#include <QErrorMessage>
#include <QStatusBar>
#include <QSplitter>

ECoopSCView::ECoopSCView(ECoopSCModel& _model) :
	QMainWindow(0), model(_model), controller(_model, *this), connectDialog(_model, *this)
{

	connect(&model, SIGNAL(modelUpdate()), this, SLOT(refreshView()));
	setWindowTitle("CoopSC GUI");
	resize(1024, 768);

	QToolBar *toolbar = addToolBar("Toolbar");

	executeAction = toolbar->addAction(QIcon(":/icons/execute.png"), "Execute");
	connectAction = toolbar->addAction(QIcon(":/icons/connect.png"), "Connect");
	disconnectAction = toolbar->addAction(QIcon(":/icons/disconnect.png"), "Disconnect");
	quitAction = toolbar->addAction(QIcon(":/icons/quit.png"), "Quit");

	connect(executeAction, SIGNAL(triggered()), &controller, SLOT(execute()));
	connect(connectAction, SIGNAL(triggered()), &controller, SLOT(connect()));
	connect(disconnectAction, SIGNAL(triggered()), &controller, SLOT(disconnect()));
	connect(quitAction, SIGNAL(triggered()), &controller, SLOT(quit()));

	sql = new QTextEdit();
	queryresult = new QTableView();
	queryresult->setModel(&model.getQueryResultModel());



	connect(&model.getQueryResultModel(), SIGNAL(modelReset()), this, SLOT(refreshStatusBar()));
	connect(&model.getQueryResultModel(), SIGNAL(modelReset()), queryresult, SLOT(resizeColumnsToContents()));



	QSplitter* vsplitter = new QSplitter(Qt::Vertical);
	vsplitter->addWidget(sql);
	vsplitter->addWidget(queryresult);
	QList<int> sizes; sizes << 150 << 850;
	vsplitter->setSizes(sizes);


	QSplitter* hsplitter = new QSplitter(Qt::Horizontal);
	hsplitter->addWidget(vsplitter);

	sizes.clear(); sizes << 350 << 250;
	hsplitter->setSizes(sizes);

	setCentralWidget(hsplitter);
	refreshView();
}

ECoopSCView::~ECoopSCView()
{
}

std::string ECoopSCView::getSql()
{
	return sql->toPlainText().toStdString();
}

void ECoopSCView::errorMessage(const std::string& msg)
{
	QErrorMessage errorMessage;
	errorMessage.showMessage(msg.c_str());
	errorMessage.exec();
}

void ECoopSCView::setStatus(const std::string& status)
{
	statusBar()->showMessage(status.c_str());
}

void ECoopSCView::refreshStatusBar()
{
	std::stringstream ss;
	if (model.getQueryResultModel().getResult().get() != NULL)
	{
		ss << model.getQueryResultModel().getResult()->getNoTuples() << " rows";
		setStatus(ss.str());
	}
	else
		setStatus("");
}

void ECoopSCView::refreshView()
{
	if (model.connected())
	{
		executeAction-> setEnabled(true);
		connectAction->setEnabled(false);
		disconnectAction->setEnabled(true);
		quitAction->setEnabled(true);
		centralWidget()->setEnabled(true);
		std::stringstream ss;
		ss << "ECoopSC GUI - " << model.getManagerHost() << ":" << model.getManagerPort();
		setWindowTitle(ss.str().c_str());
		setStatus("Connected");
	}
	else
	{
		executeAction->setEnabled(false);
		connectAction->setEnabled(true);
		disconnectAction->setEnabled(false);
		quitAction->setEnabled(true);
		centralWidget()->setEnabled(false);
		setWindowTitle("CoopSC GUI");
		setStatus("Disconnected");
	}
}

void ECoopSCView::resizeQueryTreeColums()
{

}
