#ifndef COOPSWINDOW_H_
#define COOPSWINDOW_H_


#include <QApplication>
#include <QWidget>
#include <QAction>
#include <QMainWindow>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QTextEdit>
#include <QTableView>
#include <QTreeView>
#include "ecoopscmodel.h"
#include "connectdialog.h"
#include "ecoopsccontroller.h"


class ECoopSCView : public QMainWindow
{
	Q_OBJECT
private :
	ECoopSCModel& model;
	ECoopSCController controller;

	ConnectDialog connectDialog;


	QAction* executeAction;
	QAction* connectAction;
	QAction* disconnectAction;
	QAction* quitAction;


	QTableView *queryresult;
	QTextEdit* sql;
public :
	ECoopSCView (ECoopSCModel& _model);
	~ECoopSCView();

	//returns the sql query typed in the text box
	std::string getSql();

	//displays an error message
	void errorMessage(const std::string& msg);

	//called when the model in changed
	void update();

	//sets the status message;
	void setStatus(const std::string& status);

	ConnectDialog& getConnectDialog()
	{
		return connectDialog;
	}


public slots :
	void refreshStatusBar();
	void refreshView();
	void resizeQueryTreeColums();
};

#endif /* COOPSWINDOW_H_ */
