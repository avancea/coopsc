#include <iostream>
#include "logger.h"
#include "ecoopscmodel.h"
#include "ecoopscview.h"
#include <QApplication>

int main(int argc, char *argv[])
{
	INIT_LOGGER(std::cout);

	QApplication app(argc, argv);
	ECoopSCModel model;
	ECoopSCView view(model);
	view.show();
	return app.exec();
}
