#include <boost/lexical_cast.hpp>
#include "queryresultmodel.h"

QueryResultModel::QueryResultModel()
{
}

QueryResultModel::~QueryResultModel()
{
}

void QueryResultModel::setResult(ecoopsc::ResultSetPtr _result)
{
	result = _result;
	reset();
}

ecoopsc::ResultSetPtr QueryResultModel::getResult()
{
	return result;
}

int QueryResultModel::rowCount(const QModelIndex &parent) const
{
	if (result.get() == NULL)
		return 0;
	return result->getNoTuples();
}

int QueryResultModel::columnCount(const QModelIndex &parent) const
{
	if (result.get() == NULL)
		return 0;
	return result->getNoFields();
}

QVariant QueryResultModel::data(const QModelIndex &index, int role) const
{
	if (role != Qt::DisplayRole)
		return QVariant();
	if (!index.isValid())
		return QVariant();
	if (result.get() == NULL)
		return QVariant();
	if (index.row() < 0 || index.row() >= result->getNoTuples())
		return QVariant();
	if (index.column() < 0 || index.column() >= result->getNoFields())
		return QVariant();

	return QString(result->getValue(index.row(), index.column()).c_str());
}

QVariant QueryResultModel::headerData(int section, Qt::Orientation orientation, int role) const
{
	if (result.get() == NULL)
		return QVariant();
	if (role != Qt::DisplayRole)
		return QVariant();
	if (orientation == Qt::Horizontal)
	{
		std::string header = result->getFieldName(section);
		return QString(header.c_str());
	}
	return QVariant();
}

Qt::ItemFlags QueryResultModel::flags(const QModelIndex & index) const
{
	return Qt::ItemIsSelectable | Qt::ItemIsEnabled;
}
