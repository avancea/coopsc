#ifndef CONNECTDIALOG_H_
#define CONNECTDIALOG_H_
#include <QDialog>
#include <QLabel>
#include <QPushButton>
#include <QDialogButtonBox>
#include <QLineEdit>
#include <QSpinBox>
#include <QComboBox>
#include "ecoopscmodel.h"
class ECoopSCView;

class ConnectDialog: public QDialog
{
private :
	ECoopSCModel& model;
	ECoopSCView& view;

	QLineEdit* entrymanhost;
	QSpinBox* entrymanport;

public :
	ConnectDialog(ECoopSCModel& _model, ECoopSCView& _view);
	virtual ~ConnectDialog()
	{

	}

	//returns the database name
	const std::string getManagerHost() const
	{
		return entrymanhost->text().toStdString();
	}


	//returns the database port
	int getManagerPort() const
	{
		return entrymanport->value();
	}


};

#endif /* CONNECTDIALOG_H_ */
