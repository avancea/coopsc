#include <iostream>
#include <boost/foreach.hpp>
#include <boost/asio.hpp>
#include <cctype>
#include "ecoopscmodel.h"
#include "logger.h"
using namespace boost::asio;

// Return the local ip of the computer
std::string ECoopSCModel::getLocalIp()
{
	try
	{
		io_service io_service;
		ip::tcp::resolver resolver(io_service);
		ip::tcp::resolver::query query(ip::host_name(), "");
		ip::tcp::resolver::iterator it = resolver.resolve(query);

		while (it != ip::tcp::resolver::iterator())
		{
			ip::address addr = (it++)->endpoint().address();
			if (addr.is_v4())
				return addr.to_string();
		}
	} catch (...)
	{
	}
	return "127.0.0.1";
}

void ECoopSCModel::execute(const std::string& sql)
{
	ecoopsc::ResultSetPtr result;
	result = connection->query(sql);
	queryResultModel.setResult(result);
}

//connects to the database
void ECoopSCModel::connect()
{
	try
	{
		connection = boost::shared_ptr<ecoopsc::Connection>(new ecoopsc::Connection(manhost, manport));
	} catch (std::exception& e)
	{
		LOG_ERROR(e.what());
		throw;
	}
	try
	{
		connection->connect();
	} catch (std::exception& e)
	{
		LOG_ERROR(e.what());
		connection.reset();
		throw;
	}

	emit modelUpdate();
}

//disconnects from the database
void ECoopSCModel::disconnect()
{
	connection.reset();
	emit modelUpdate();
}
//returns true if the connection is established
bool ECoopSCModel::connected()
{
	return (connection.get() != NULL);
}
