#ifndef QUERYRESULTMODEL_H_
#define QUERYRESULTMODEL_H_
#include <ecoopsc.h>
#include <QAbstractTableModel>

//encapsulates a result set
class QueryResultModel: public QAbstractTableModel
{
private:
	ecoopsc::ResultSetPtr result;
public:
	QueryResultModel();
	virtual ~QueryResultModel();
	void setResult(ecoopsc::ResultSetPtr _result);

    int rowCount(const QModelIndex &parent) const;
    int columnCount(const QModelIndex &parent) const;
    QVariant data(const QModelIndex &index, int role) const;
    QVariant headerData(int section, Qt::Orientation orientation, int role) const;
    Qt::ItemFlags flags (const QModelIndex & index ) const;
    ecoopsc::ResultSetPtr getResult();
};

#endif /* QUERYRESULTMODEL_H_ */
