#ifndef CACHEREGISTRYDIALOG_H_
#define CACHEREGISTRYDIALOG_H_
#include <gtkmm.h>
#include "cacheregistrymodel.h"

class CoopSCView;

class CacheRegistryDialog: public Gtk::Dialog
{
private:
	class ModelColumns: public Gtk::TreeModel::ColumnRecord
	{
	public:

		ModelColumns()
		{
			add(id);
			add(tablename);
			add(fields);
			add(predicate);
			add(size);
		}

		Gtk::TreeModelColumn<int> id;
		Gtk::TreeModelColumn<Glib::ustring> tablename;
		Gtk::TreeModelColumn<Glib::ustring> fields;
		Gtk::TreeModelColumn<Glib::ustring> predicate;
		Gtk::TreeModelColumn<int> size;
	};

	CacheRegistryModel& model;

	CoopSCView& view;
	Gtk::TreeView treeView;
	Glib::RefPtr<Gtk::ListStore> refTreeModel;
	ModelColumns columns;
	Gtk::ScrolledWindow scrolledWindow;

public:
	CacheRegistryDialog(CacheRegistryModel& _model, CoopSCView& _view);
	void update();
	virtual ~CacheRegistryDialog();
};

#endif /* CACHEREGISTRYDIALOG_H_ */
