#ifndef CONNECTDIALOG_H_
#define CONNECTDIALOG_H_
#include <gtkmm.h>
#include "coopscmodel.h"

class CoopSCView;

class ConnectDialog: public Gtk::Dialog
{
private:
	//model columns for combocachelevel
	class ModelColumns: public Gtk::TreeModel::ColumnRecord
	{
	public:

		ModelColumns()
		{
			add(colid);
			add(cachetype);
		}

		Gtk::TreeModelColumn<int> colid;
		Gtk::TreeModelColumn<Glib::ustring> cachetype;
	};

	CoopSCModel& model;
	CoopSCView& view;

	Gtk::Table table;

	Gtk::Label labeldbname;
	Gtk::Entry entrydbname;

	Gtk::Label labeldbhost;
	Gtk::Entry entrydbhost;

	Gtk::Label labeldbport;
	Gtk::SpinButton entrydbport;

	Gtk::Label labeldbusr;
	Gtk::Entry entrydbusr;

	Gtk::Label labeldbpwd;
	Gtk::Entry entrydbpwd;

	Gtk::Label labelcoopschost;
	Gtk::Entry entrycoopschost;

	Gtk::Label labelcoopscport;
	Gtk::SpinButton entrycoopscport;

	Gtk::Label labelcachelevel;
	Gtk::ComboBox combocachelevel;

	Gtk::Label labelcachesize;
	Gtk::SpinButton entrycachesize;

	Gtk::Label labelchimeraport;
	Gtk::SpinButton entrychimeraport;

	Gtk::Label labelchimerabshost;
	Gtk::Entry entrychimerabshost;

	Gtk::Label labelchimerabsport;
	Gtk::SpinButton entrychimerabsport;

	Gtk::Button ok;
	Gtk::Button cancel;

	Glib::RefPtr<Gtk::ListStore> refListStore;
	ModelColumns columns;

public:
	ConnectDialog(CoopSCModel& _model, CoopSCView& _view);
	virtual ~ConnectDialog()
	{

	}

	//returns the database name
	const std::string getDbName() const
	{
		return entrydbname.get_text();
	}

	//returns the database host
	const std::string getDbHost() const
	{
		return entrydbhost.get_text();;
	}

	//returns the database port
	int getDbPort() const
	{
		return entrydbport.get_value_as_int();
	}

	//returns the database user name
	const std::string getDbUsr() const
	{
		return entrydbusr.get_text();
	}

	//returns the database password
	const std::string getDbPwd() const
	{
		return entrydbpwd.get_text();
	}

	//returns the cache manager host
	const std::string getCoopSCHost() const
	{
		return entrycoopschost.get_text();
	}

	//returns the cache manager port
	int getCoopSCPort() const
	{
		return entrycoopscport.get_value_as_int();
	}

	//returns the size of the cache
	int getCacheSize()
	{
		return entrycachesize.get_value_as_int();
	}

	//returns the cache level
	int getCacheLevel()
	{
		return combocachelevel.get_active_row_number();
	}

	int getChimeraPort()
	{
		return entrychimeraport.get_value_as_int();
	}

	std::string getChimeraBootstrapHost()
	{
		return entrychimerabshost.get_text();
	}

	int getChimeraBootstrapPort()
	{
		return entrychimerabsport.get_value_as_int();
	}

};

#endif /* CONNECTDIALOG_H_ */
