#ifndef QUERYRESULTVIEW_H_
#define QUERYRESULTVIEW_H_
#include <gtkmm.h>
#include <vector>
#include <boost/bind.hpp>
#include <boost/smart_ptr.hpp>
#include "queryresultmodel.h"

class CoopSCView;

class QueryResultView: public Gtk::Frame
{
private:
	class ModelColumns: public Gtk::TreeModel::ColumnRecord
	{
	private:
		std::vector<Gtk::TreeModelColumn<Glib::ustring> > cols;
	public:
		ModelColumns(QueryResultModel& model);
		Gtk::TreeModelColumn<Glib::ustring>& col(int k)
		{
			return cols[k];
		}
	};

	QueryResultModel &model;
	CoopSCView& view;
	QueryResultModel::Connection connection;
	Gtk::TreeView treeView;
	Glib::RefPtr<Gtk::ListStore> refTreeModel;
	boost::shared_ptr<ModelColumns> columns;
	Gtk::ScrolledWindow scrolledWindow;

public:
	QueryResultView(QueryResultModel& _model, CoopSCView& _view);
	virtual ~QueryResultView()
	{
		model.disconnectObserver(connection);
	}

	void update();
};

#endif /* QUERYRESULTVIEW_H_ */
