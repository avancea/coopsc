#ifndef COOPSWINDOW_H_
#define COOPSWINDOW_H_

#include <gtkmm.h>
#include "coopscmodel.h"
#include "connectdialog.h"
#include "cacheregistrydialog.h"
#include "queryresultview.h"
#include "coopsccontroller.h"
#include "querytreeview.h"

class CoopSCView: public Gtk::Window
{
private:
	CoopSCModel& model;
	CoopSCController controller;
	CoopSCModel::Connection connection;

	QueryResultView queryresult;
	QueryTreeView querytreeview;
	ConnectDialog connectDialog;
	CacheRegistryDialog cacheRegistryDialog;

	Glib::RefPtr<Gtk::UIManager> refUIManager;
	Glib::RefPtr<Gtk::ActionGroup> refActionGroup;
	Gtk::VBox box;

	Gtk::TextView sql;

	//contains sql and queryresult
	Gtk::VPaned sql_result;

	Gtk::HPaned sql_result_plan;

	Glib::RefPtr<Gtk::Action> executeAction;
	Glib::RefPtr<Gtk::Action> connectAction;
	Glib::RefPtr<Gtk::Action> disconnectAction;
	Glib::RefPtr<Gtk::Action> cacheregistryAction;
	Glib::RefPtr<Gtk::Action> quitAction;

	Gtk::Statusbar statusBar;

public:
	CoopSCView(CoopSCModel& _model);
	~CoopSCView();

	//returns the sql query typed in the text box
	std::string getSql();

	//displays an error message
	void errorMessage(const std::string& msg);

	//called when the model in changed
	void update();

	ConnectDialog& getConnectDialog()
	{
		return connectDialog;
	}

	CacheRegistryDialog& getCacheRegistryDialog()
	{
		return cacheRegistryDialog;
	}

	//sets the status message
	void setStatus(const std::string& status);
};

#endif /* COOPSWINDOW_H_ */
