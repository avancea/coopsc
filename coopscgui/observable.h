#ifndef OBSERVABLE_H_
#define OBSERVABLE_H_
#include <boost/signal.hpp>
class Observable
{
public:
	typedef boost::signal<void()> Signal;
	typedef boost::signals::connection Connection;
private:
	Signal sig;
public:
	Connection connectObserver(Signal::slot_function_type subscriber)
	{
		return sig.connect(subscriber);
	}
	void disconnectObserver(Connection connection)
	{
		connection.disconnect();
	}
protected:
	void refresh()
	{
		sig();
	}
};

#endif /* OBSERVABLE_H_ */
