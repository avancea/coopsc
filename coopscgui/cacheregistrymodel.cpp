#include <sstream>
#include "cacheregistrymodel.h"

unsigned int CacheRegistryModel::getSize()
{
	if (storagemanager == NULL)
		return 0;
	return storagemanager->getRegions().size();
}

void CacheRegistryModel::getIds(std::vector<int>& ids)
{
	ids.clear();
	for (coopsc::StorageManager::RegionsType::iterator it = storagemanager->getRegions().begin(); it != storagemanager->getRegions().end(); it++)
		ids.push_back(it->first);

}

void CacheRegistryModel::get(int id, std::string& tablename, std::string& fields, std::string& predicate, int& size)
{
	coopsc::RegionPtr reg = storagemanager->getRegion(id);
	tablename = reg->getQuery().getTableName();

	coopsc::FieldsMask fieldsmask = reg->getQuery().getFields();
	const coopsc::Table& table = metadataregistry->getTable(tablename);

	fields = "";

	//all fields
	if (fieldsmask == table.getFieldsMask())
	{
		fields = "*";
	}
	else
	{
		for (unsigned int i = 0; fieldsmask; i++)
			if (fieldsmask & (1 << i))
			{
				if (fields != "")
					fields += ",";
				fields += table.getField(i).getName();
				fieldsmask &= ~(1 << i);
			}
	}
	std::stringstream ss;
	ss << reg->getQuery().getPredicate();
	predicate = ss.str();
	size = reg->getNoTuples();
}

void CacheRegistryModel::clear()
{
	storagemanager->clear();
}
