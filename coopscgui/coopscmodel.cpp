#include <iostream>
#include <boost/foreach.hpp>
#include <boost/asio.hpp>
#include <cctype>
#include "coopscmodel.h"
#include "logger.h"

using namespace boost::asio;

// Return the local ip of the computer
std::string CoopSCModel::getLocalIp()
{
	try
	{
		io_service io_service;
		ip::tcp::resolver resolver(io_service);
		ip::tcp::resolver::query query(ip::host_name(), "");
		ip::tcp::resolver::iterator it = resolver.resolve(query);

		while (it != ip::tcp::resolver::iterator())
		{
			ip::address addr = (it++)->endpoint().address();
			if (addr.is_v4())
				return addr.to_string();
		}
	} catch (...)
	{
	}
	return "127.0.0.1";
}

void CoopSCModel::execute(const std::string& sql)
{
	coopsc::ResultSetPtr result;

	//first, determining the type of query (select, delete, update, insert)
	std::string type;
	BOOST_FOREACH(char c, sql)
	{
		if (isspace(c))
		{
			if (type.empty())
				continue;
			else
				break;
		}
		type += toupper(c);
	}
	queryResultModel.setResult(result);
	if (type == "SELECT")
	{
		result = connection->query(sql);
		queryTreeModel.setQueryPlan(connection->getLastQueryPlan());
		queryResultModel.setResult(result);
	}
	else
	{
		connection->execute(sql);
		queryTreeModel.setQueryPlan(coopsc::QueryPlanPtr());
	}
}

//connects to the database
void CoopSCModel::connect()
{
	try
	{
		connection = boost::shared_ptr<coopsc::Connection>(new coopsc::Connection(dbname, dbhost, dbport, dbusr, dbpwd, coopschost,
				coopscport, (coopsc::CacheType) cachelevel, cachesize * 1024 * 1024, chimeraport, chimerabshost, chimerabsport));
	} catch (std::exception& e)
	{
		LOG_ERROR(e.what());
		throw;
	}
	try
	{
		connection->connect();
	} catch (std::exception& e)
	{
		LOG_ERROR(e.what());
		connection.reset();
		throw;
	}
	cacheRegistryModel.setStorageManager(&connection->getStorageManager());
	cacheRegistryModel.setMetadataRegistry(&connection->getMetadataRegistry());
	queryTreeModel.setQueryPlan(connection->getLastQueryPlan());
	refresh();
}

//disconnects from the database
void CoopSCModel::disconnect()
{
	connection.reset();
	cacheRegistryModel.setStorageManager(NULL);
	queryTreeModel.setQueryPlan(coopsc::QueryPlanPtr());
	refresh();
}
//returns true if the connection is established
bool CoopSCModel::connected()
{
	return (connection.get() != NULL);
}
