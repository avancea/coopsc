#ifndef COOPSCMODEL_H_
#define COOPSCMODEL_H_

#include <coopsc.h>
#include <boost/smart_ptr.hpp>
#include "observable.h"
#include "queryresultmodel.h"
#include "cacheregistrymodel.h"
#include "querytreemodel.h"

class CoopSCModel: public Observable
{
private:
	// database name, host, port, user, password
	std::string dbname;
	std::string dbhost;
	int dbport;
	std::string dbusr;
	std::string dbpwd;

	//chimera port
	int chimeraport;

	//chimera bootstrap host, port
	std::string chimerabshost;
	int chimerabsport;

	//CoopSC cache manager, host, port
	std::string coopschost;
	int coopscport;

	//cache level : 0 - no cache; 1 - local cache; 2 - cooperative cache
	int cachelevel;
	int cachesize;

	QueryResultModel queryResultModel;
	CacheRegistryModel cacheRegistryModel;
	QueryTreeModel queryTreeModel;
	boost::shared_ptr<coopsc::Connection> connection;

public:
	CoopSCModel() :
		dbport(5432), coopschost(CoopSCModel::getLocalIp()), coopscport(1999), cachelevel(2), cachesize(128)
	{

	}

	//returns the query result model;
	QueryResultModel& getQueryResultModel()
	{
		return queryResultModel;
	}

	//returns the cache registry model;
	CacheRegistryModel& getCacheRegistryModel()
	{
		return cacheRegistryModel;
	}

	QueryTreeModel& getQueryTreeModel()
	{
		return queryTreeModel;
	}

	//executes a sql query
	void execute(const std::string& sql);

	//connects to the database
	void connect();

	//disconnects from the database
	void disconnect();

	//returns true if the connection is established
	bool connected();

	//returns the database name
	const std::string& getDbName() const
	{
		return dbname;
	}

	//sets the database name
	void setDbName(const std::string& _dbname)
	{
		dbname = _dbname;
	}

	//returns the database host
	const std::string& getDbHost() const
	{
		return dbhost;
	}

	//sets the database name
	void setDbHost(const std::string& _dbhost)
	{
		dbhost = _dbhost;
	}

	//returns the database port
	int getDbPort() const
	{
		return dbport;
	}

	//sets the database port
	void setDbPort(int _dbport)
	{
		dbport = _dbport;
	}

	//returns the database user name
	const std::string& getDbUsr() const
	{
		return dbusr;
	}

	//sets the database user name
	void setDbUsr(const std::string& _dbusr)
	{
		dbusr = _dbusr;
	}

	//returns the database password
	const std::string& getDbPwd() const
	{
		return dbpwd;
	}

	//sets the database user name
	void setDbPwd(const std::string& _dbpwd)
	{
		dbpwd = _dbpwd;
	}

	//returns the cache manager host
	const std::string& getCoopSCHost() const
	{
		return coopschost;
	}

	//sets the database user name
	void setCoopSCHost(const std::string& _coopschost)
	{
		coopschost = _coopschost;
	}

	//returns the cache manager port
	int getCoopSCPort() const
	{
		return coopscport;
	}

	//sets the database port
	void setCoopSCPort(int _coopscport)
	{
		coopscport = _coopscport;
	}

	//returns the size of the cache
	int getCacheSize()
	{
		return cachesize;
	}

	//sets the size of the cache
	void setCacheSize(int _cachesize)
	{
		cachesize = _cachesize;
	}

	//returns the cache level
	int getCacheLevel()
	{
		return cachelevel;
	}

	//sets the size of the cache
	void setCacheLevel(int _cachelevel)
	{
		cachelevel = _cachelevel;
	}

	int getChimeraPort()
	{
		return chimeraport;
	}

	void setChimeraPort(int _chimeraport)
	{
		chimeraport = _chimeraport;
	}

	const std::string& getChimeraBootstrapHost()
	{
		return chimerabshost;
	}

	void setChimeraBootstrapHost(const std::string& _chimerabshost)
	{
		chimerabshost = _chimerabshost;
	}

	int getChimeraBootstrapPort()
	{
		return chimerabsport;
	}

	void setChimeraBootstrapPort(int _chimerabsport)
	{
		chimerabsport = _chimerabsport;
	}

	static std::string getLocalIp();
};

#endif /* COOPSCMODEL_H_ */
