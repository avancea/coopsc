cmake_minimum_required(VERSION 2.8)

include_directories (${CoopSC_SOURCE_DIR}/chimera)
include_directories (${CoopSC_SOURCE_DIR}/coopsc)

file (GLOB coopscgui_SRCS *.cpp) 

find_package (Boost 1.40 COMPONENTS program_options thread system signals iostreams serialization REQUIRED)
link_directories (${Boost_LIBRARY_DIRS})
include_directories (${Boost_INCLUDE_DIRS})

#find_package (OpenSSL REQUIRED)
#link_directories (${OpenSSL_LIBRARY_DIRS})
#include_directories (${OpenSSL_INCLUDE_DIRS})


find_package (PG REQUIRED)
include_directories (${PG_INCLUDE_DIRS})
link_directories (${PG_LIBRARY_DIRS})


find_package (PkgConfig)
pkg_check_modules (GTKMM gtkmm-2.4)
link_directories (${GTKMM_LIBRARY_DIRS})
include_directories (${GTKMM_INCLUDE_DIRS})


add_executable (coopscgui ${coopscgui_SRCS})
target_link_libraries (coopscgui coopsc)  
target_link_libraries (coopscgui ${PG_LIBRARIES}) 
target_link_libraries (coopscgui ${GTKMM_LIBRARIES}) 
target_link_libraries (coopscgui ${Boost_LIBRARIES})
target_link_libraries (coopscgui ${OPENSSL_LIBRARIES})
target_link_libraries (coopscgui chimera)  

