#include <gtkmm.h>
#include <iostream>
#include "logger.h"
#include "coopscmodel.h"
#include "coopscview.h"

int main(int argc, char *argv[])
{
	INIT_LOGGER(std::cout);

	Gtk::Main kit(argc, argv);

	CoopSCModel model;

	CoopSCView view(model);

	Gtk::Main::run(view);

	return 0;
}
