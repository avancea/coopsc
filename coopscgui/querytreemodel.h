#ifndef QUERYTREEMODEL_H_
#define QUERYTREEMODEL_H_
#include <coopsc.h>
#include <boost/smart_ptr.hpp>
#include "observable.h"

class QueryTreeModel: public Observable
{
private:
	coopsc::QueryPlanPtr plan;
public:
	QueryTreeModel();
	coopsc::QueryPlanPtr getQueryPlan()
	{
		return plan;
	}
	void setQueryPlan(coopsc::QueryPlanPtr _plan);
	virtual ~QueryTreeModel();
};

#endif /* QUERYTREEMODEL_H_ */
