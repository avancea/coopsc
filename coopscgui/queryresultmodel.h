#ifndef QUERYRESULTMODEL_H_
#define QUERYRESULTMODEL_H_
#include <coopsc.h>
#include <boost/signal.hpp>
#include "observable.h"

//encapsulates a result set
class QueryResultModel: public Observable
{
private:
	coopsc::ResultSetPtr result;
public:
	QueryResultModel();
	virtual ~QueryResultModel();
	void setResult(coopsc::ResultSetPtr _result);
	coopsc::ResultSetPtr getResult();
};

#endif /* QUERYRESULTMODEL_H_ */
