#include "queryresultmodel.h"

QueryResultModel::QueryResultModel()
{
}

QueryResultModel::~QueryResultModel()
{
}

void QueryResultModel::setResult(coopsc::ResultSetPtr _result)
{
	result = _result;
	refresh();
}

coopsc::ResultSetPtr QueryResultModel::getResult()
{
	return result;
}

