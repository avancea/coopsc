#ifndef QUERYTREEVIEW_H_
#define QUERYTREEVIEW_H_
#include <gtkmm.h>
#include "querytreemodel.h"
class CoopSCView;

class QueryTreeView: public Gtk::Frame
{
private:
	//Tree model columns:
	class ModelColumns: public Gtk::TreeModel::ColumnRecord
	{
	public:

		ModelColumns()
		{
			add(type);
			add(description);
		}

		Gtk::TreeModelColumn<Glib::ustring> type;
		Gtk::TreeModelColumn<Glib::ustring> description;
	};

	QueryTreeModel& model;
	CoopSCView& view;

	QueryTreeModel::Connection connection;
	Gtk::TreeView treeView;
	Glib::RefPtr<Gtk::TreeStore> refTreeModel;
	ModelColumns columns;
	Gtk::ScrolledWindow scrolledWindow;

	void fill(Gtk::TreeModel::Row& row, const coopsc::QueryPlan& plan);
	void fill(Gtk::TreeModel::Row& row, const coopsc::NodePtr& node);

public:
	QueryTreeView(QueryTreeModel& _model, CoopSCView& _view);
	void update();
	virtual ~QueryTreeView();
};

#endif /* QUERYTREEVIEW_H_ */
