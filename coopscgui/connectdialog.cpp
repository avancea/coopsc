#include "connectdialog.h"
#include "coopscview.h"

ConnectDialog::ConnectDialog(CoopSCModel& _model, CoopSCView& _view) :
	Gtk::Dialog("Connect", _view), model(_model), view(_view), table(9, 2), labeldbname("Database name ", 0, 0.5), labeldbhost("Host ", 0,
			0.5), labeldbport("Port ", 0, 0.5), labeldbusr("User ", 0, 0.5), labeldbpwd("Password ", 0, 0.5), labelcoopschost("CoopSC IP ",
			0, 0.5), labelcoopscport("CoopSC Port ", 0, 0.5), labelcachelevel("Cache Type ", 0, 0.5),
			labelcachesize("Cache Size ", 0, 0.5), labelchimeraport("Chimera Port", 0, 0.5), labelchimerabshost("Chimera Bootstrap Host",
					0, 0.5), labelchimerabsport("Chimera Bootstrap Port", 0, 0.5), ok(Gtk::Stock::OK)
{

	entrydbport.set_digits(0);
	entrydbport.set_range(1, 65535);
	entrydbport.set_increments(1, 1);
	entrydbport.set_value(5432);

	entrycoopscport.set_digits(0);
	entrycoopscport.set_range(1, 65535);
	entrycoopscport.set_increments(1, 1);
	entrycoopscport.set_value(1999);

	entrychimeraport.set_digits(0);
	entrychimeraport.set_range(1, 65535);
	entrychimeraport.set_increments(1, 1);
	entrychimeraport.set_value(9999);

	entrychimerabsport.set_digits(0);
	entrychimerabsport.set_range(1, 65535);
	entrychimerabsport.set_increments(1, 1);
	entrychimerabsport.set_value(9999);

	entrycachesize.set_digits(0);
	entrycachesize.set_range(1, 1024);
	entrycachesize.set_increments(1, 1);
	entrycachesize.set_value(128);

	entrydbname.set_text("cache_test");
	entrydbhost.set_text("192.41.135.199");
	entrydbusr.set_text("postgres");
	entrydbpwd.set_text("postgres");
	entrydbpwd.set_visibility(false);

	entrycoopschost.set_text(CoopSCModel::getLocalIp());

	table.attach(labeldbname, 0, 1, 0, 1);
	table.attach(entrydbname, 1, 2, 0, 1);

	table.attach(labeldbhost, 0, 1, 1, 2);
	table.attach(entrydbhost, 1, 2, 1, 2);

	table.attach(labeldbport, 0, 1, 2, 3);
	table.attach(entrydbport, 1, 2, 2, 3);

	table.attach(labeldbusr, 0, 1, 3, 4);
	table.attach(entrydbusr, 1, 2, 3, 4);

	table.attach(labeldbpwd, 0, 1, 4, 5);
	table.attach(entrydbpwd, 1, 2, 4, 5);

	table.attach(labelcoopschost, 0, 1, 5, 6);
	table.attach(entrycoopschost, 1, 2, 5, 6);

	table.attach(labelcoopscport, 0, 1, 6, 7);
	table.attach(entrycoopscport, 1, 2, 6, 7);

	table.attach(labelcachelevel, 0, 1, 7, 8);
	table.attach(combocachelevel, 1, 2, 7, 8);

	table.attach(labelcachesize, 0, 1, 8, 9);
	table.attach(entrycachesize, 1, 2, 8, 9);

	table.attach(labelchimeraport, 0, 1, 9, 10);
	table.attach(entrychimeraport, 1, 2, 9, 10);

	table.attach(labelchimerabshost, 0, 1, 10, 11);
	table.attach(entrychimerabshost, 1, 2, 10, 11);

	table.attach(labelchimerabsport, 0, 1, 11, 12);
	table.attach(entrychimerabsport, 1, 2, 11, 12);

	refListStore = Gtk::ListStore::create(columns);
	combocachelevel.set_model(refListStore);

	Gtk::TreeModel::Row row = *(refListStore->append());
	row[columns.colid] = 0;
	row[columns.cachetype] = "No cache";

	row = *(refListStore->append());
	row[columns.colid] = 1;
	row[columns.cachetype] = "Semantic Cache";

	row = *(refListStore->append());
	row[columns.colid] = 2;
	row[columns.cachetype] = "Cooperative Semantic Cache";

	combocachelevel.set_active(2);

	combocachelevel.pack_start(columns.colid);
	combocachelevel.pack_start(columns.cachetype);

	get_vbox()->pack_start(table);
	add_button(Gtk::Stock::OK, Gtk::RESPONSE_OK);
	add_button(Gtk::Stock::CANCEL, Gtk::RESPONSE_DELETE_EVENT);

	set_position(Gtk::WIN_POS_CENTER);
	set_resizable(false);
	set_modal(true);

	show_all_children();
}

