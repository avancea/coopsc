#include "cacheregistrydialog.h"
#include "coopscview.h"

CacheRegistryDialog::CacheRegistryDialog(CacheRegistryModel& _model, CoopSCView& _view) :
	Gtk::Dialog("Cache Registry", _view), model(_model), view(_view)
{
	refTreeModel = Gtk::ListStore::create(columns);
	treeView.set_model(refTreeModel);
	scrolledWindow.add(treeView);

	treeView.append_column("Id", columns.id);
	treeView.append_column("Table", columns.tablename);
	treeView.append_column("Fields", columns.fields);
	treeView.append_column("Predicate", columns.predicate);
	treeView.append_column("Size", columns.size);

	get_vbox()->pack_start(scrolledWindow);

	add_button(Gtk::Stock::OK, Gtk::RESPONSE_OK);
	add_button(Gtk::Stock::CLEAR, Gtk::RESPONSE_NO);

	set_position(Gtk::WIN_POS_CENTER);
	set_resizable(true);
	set_modal(true);

	resize(400, 200);
	show_all_children();

}

void CacheRegistryDialog::update()
{
	std::string tablename;
	std::string fields;
	std::string predicate;
	int size;

	refTreeModel->clear();
	std::vector<int> ids;
	model.getIds(ids);
	for (unsigned int i = 0; i < ids.size(); i++)
	{
		Gtk::TreeModel::Row row = *(refTreeModel->append());
		model.get(ids[i], tablename, fields, predicate, size);
		row[columns.id] = ids[i];
		row[columns.tablename] = tablename;
		row[columns.fields] = fields;
		row[columns.predicate] = predicate;
		row[columns.size] = size;
	}
}
CacheRegistryDialog::~CacheRegistryDialog()
{
	// TODO Auto-generated destructor stub
}
