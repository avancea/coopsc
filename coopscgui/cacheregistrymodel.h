#ifndef CACHEREGISTRYMODEL_H_
#define CACHEREGISTRYMODEL_H_
#include <string>
#include <coopsc.h>

class CacheRegistryModel
{
private:
	coopsc::StorageManager* storagemanager;
	coopsc::MetadataRegistry* metadataregistry;
public:
	CacheRegistryModel()
	{
		storagemanager = NULL;
	}
	unsigned int getSize();

	void get(int id, std::string& tablename, std::string& fields, std::string& predicate, int& size);

	void setStorageManager(coopsc::StorageManager* _storagemanager)
	{
		storagemanager = _storagemanager;
	}

	void setMetadataRegistry(coopsc::MetadataRegistry* _metadataregistry)
	{
		metadataregistry = _metadataregistry;
	}

	void getIds(std::vector<int>& ids);
	void clear();

	virtual ~CacheRegistryModel()
	{

	}
};

#endif /* CACHEREGISTRYMODEL_H_ */
