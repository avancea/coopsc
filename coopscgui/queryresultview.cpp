#include <boost/bind.hpp>
#include <sstream>
#include "queryresultview.h"
#include "coopscview.h"

QueryResultView::ModelColumns::ModelColumns(QueryResultModel& model)
{
	for (unsigned int i = 0; i < model.getResult()->getNoFields(); i++)
	{
		cols.push_back(Gtk::TreeModelColumn<Glib::ustring>());
		add(cols[i]);
	}
}

QueryResultView::QueryResultView(QueryResultModel& _model, CoopSCView& _view) :
	model(_model), view(_view)
{
	connection = model.connectObserver(boost::bind(&QueryResultView::update, this));
	scrolledWindow.add(treeView);
	add(scrolledWindow);
}

void QueryResultView::update()
{
	if (model.getResult().get() == NULL)
	{
		treeView.hide();
		return;
	}
	std::stringstream ss;
	ss << model.getResult()->getNoTuples() << " rows";
	view.setStatus(ss.str());
	columns = boost::shared_ptr<ModelColumns>(new ModelColumns(model));
	refTreeModel = Gtk::ListStore::create(*columns);
	treeView.set_model(refTreeModel);
	for (unsigned int i = 0; i < model.getResult()->getNoTuples(); i++)
	{
		Gtk::TreeModel::Row row = *(refTreeModel->append());
		for (unsigned int j = 0; j < model.getResult()->getNoFields(); j++)
			row[columns->col(j)] = model.getResult()->getValue(i, j);
	}
	treeView.remove_all_columns();
	for (unsigned int j = 0; j < model.getResult()->getNoFields(); j++)
	{
		treeView.append_column(model.getResult()->getFieldName(j), columns->col(j));
	}
	treeView.show();

}
