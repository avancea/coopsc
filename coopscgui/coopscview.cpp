#include "coopscview.h"

CoopSCView::CoopSCView(CoopSCModel& _model) :
	model(_model), controller(_model, *this), queryresult(_model.getQueryResultModel(), *this), querytreeview(model.getQueryTreeModel(),
			*this), connectDialog(_model, *this), cacheRegistryDialog(_model.getCacheRegistryModel(), *this)
{

	connection = model.connectObserver(boost::bind(&CoopSCView::update, this));
	set_title("CoopSC GUI");
	add(box);

	executeAction = Gtk::Action::create("Execute", Gtk::Stock::EXECUTE, "Execute", "Executes the query");
	connectAction = Gtk::Action::create("Connect", Gtk::Stock::CONNECT, "Connect", "Connects to the database server");
	disconnectAction = Gtk::Action::create("Disconnect", Gtk::Stock::DISCONNECT, "Disconnect", "Disconnects from the database server");
	cacheregistryAction = Gtk::Action::create("CacheRegistry", Gtk::Stock::INDEX, "Cache Registry", "Displays the cache registry");
	quitAction = Gtk::Action::create("Quit", Gtk::Stock::QUIT, "Quit", "Exits the application");

	refActionGroup = Gtk::ActionGroup::create();

	refActionGroup->add(executeAction, sigc::mem_fun(controller, &CoopSCController::execute));
	refActionGroup->add(connectAction, sigc::mem_fun(controller, &CoopSCController::connect));
	refActionGroup->add(disconnectAction, sigc::mem_fun(controller, &CoopSCController::disconnect));
	refActionGroup->add(cacheregistryAction, sigc::mem_fun(controller, &CoopSCController::showCacheRegistry));
	refActionGroup->add(quitAction, sigc::mem_fun(controller, &CoopSCController::quit));
	refUIManager = Gtk::UIManager::create();
	refUIManager->insert_action_group(refActionGroup);

	Glib::ustring ui_info = "<ui>"
		"  <toolbar  name='ToolBar'>"
		"    <toolitem action='Execute'/>"
		"    <toolitem action='Connect'/>"
		"    <toolitem action='Disconnect'/>"
		"    <toolitem action='CacheRegistry'/>"
		"    <toolitem action='Quit'/>"
		"  </toolbar>"
		"</ui>";

	try
	{
		refUIManager->add_ui_from_string(ui_info);
	} catch (const Glib::Error& ex)
	{
		std::cerr << "building menus failed: " << ex.what();
	}
	Gtk::Widget* pToolbar = refUIManager->get_widget("/ToolBar");

	sql.set_wrap_mode(Gtk::WRAP_WORD);

	sql_result.add1(sql);
	sql_result.add2(queryresult);
	sql_result.set_position(150);

	sql_result_plan.add1(sql_result);
	sql_result_plan.add2(querytreeview);
	sql_result_plan.set_position(500);

	box.pack_start(*pToolbar, Gtk::PACK_SHRINK);
	box.pack_start(sql_result_plan);
	box.pack_end(statusBar, Gtk::PACK_SHRINK);
	resize(800, 600);
	set_position(Gtk::WIN_POS_CENTER);

	statusBar.push("");

	show_all_children();
	update();
}

CoopSCView::~CoopSCView()
{
	model.disconnectObserver(connection);
}

std::string CoopSCView::getSql()
{
	return sql.get_buffer()->get_text().raw();
}

void CoopSCView::errorMessage(const std::string& msg)
{
	Gtk::MessageDialog dialog(*this, msg, false, Gtk::MESSAGE_ERROR);
	dialog.run();
}

void CoopSCView::setStatus(const std::string& status)
{
	statusBar.pop();
	statusBar.push(status);
}

void CoopSCView::update()
{
	if (model.connected())
	{
		executeAction-> set_sensitive(true);
		connectAction->set_sensitive(false);
		disconnectAction->set_sensitive(true);
		cacheregistryAction->set_sensitive(true);
		quitAction->set_sensitive(true);
		sql_result_plan.set_sensitive(true);

		std::stringstream ss;
		ss << "CoopSC GUI - " << model.getCoopSCHost() << ":" << model.getCoopSCPort();
		set_title(ss.str());

		setStatus("Connected");
	}
	else
	{
		executeAction->set_sensitive(false);
		connectAction->set_sensitive(true);
		disconnectAction->set_sensitive(false);
		cacheregistryAction->set_sensitive(false);
		quitAction->set_sensitive(true);
		sql_result_plan.set_sensitive(false);
		set_title("CoopSC GUI");
		setStatus("Disconnected");
	}
}
