#include <boost/bind.hpp>
#include <sstream>
#include "querytreeview.h"
#include "coopscview.h"
#include "logger.h"
#include "nodes.h"

QueryTreeView::QueryTreeView(QueryTreeModel& _model, CoopSCView& _view) :
	model(_model), view(_view)
{
	connection = model.connectObserver(boost::bind(&QueryTreeView::update, this));
	scrolledWindow.add(treeView);
	add(scrolledWindow);
	refTreeModel = Gtk::TreeStore::create(columns);
	treeView.set_model(refTreeModel);
	treeView.append_column("Type", columns.type);
	treeView.append_column("Description", columns.description);

	treeView.get_column(1)->set_sizing(Gtk::TREE_VIEW_COLUMN_FIXED);
	treeView.get_column(1)->set_fixed_width(1000);

	Gtk::CellRendererText* cell_renderer_text = dynamic_cast<Gtk::CellRendererText*> (treeView.get_column_cell_renderer(1));

	cell_renderer_text->property_wrap_mode() = Pango::WRAP_WORD;
	cell_renderer_text->property_wrap_width() = 500;

	treeView.show();
}

void QueryTreeView::fill(Gtk::TreeModel::Row& row, const coopsc::QueryPlan& plan)
{
	LOG_INFO("fill query plan :" << plan);
	fill(row, plan.getRoot());
}

void QueryTreeView::fill(Gtk::TreeModel::Row& row, const coopsc::NodePtr& node)
{

	coopsc::SelectProjectNode* selproj = dynamic_cast<coopsc::SelectProjectNode*> (node.get());
	if (selproj)
	{
		row[columns.type] = "SelectProject";

		Gtk::TreeModel::Row tablerow = *refTreeModel->append(row.children());
		tablerow[columns.type] = "Table";
		tablerow[columns.description] = selproj->getTableName();

		Gtk::TreeModel::Row fieldsrow = *refTreeModel->append(row.children());
		fieldsrow[columns.type] = "Fields";
		fieldsrow[columns.description] = selproj->getFieldsAsString();

		std::stringstream ss;
		ss << selproj->getPredicate();
		Gtk::TreeModel::Row predicaterow = *refTreeModel->append(row.children());
		predicaterow[columns.type] = "Predicate";
		predicaterow[columns.description] = ss.str();
	}

	if (dynamic_cast<coopsc::UnionNode*> (node.get()))
	{
		row[columns.type] = "Union";
	}

	if (dynamic_cast<coopsc::JoinNode*> (node.get()))
	{
		row[columns.type] = "Join";
	}

	coopsc::RegionNode* regionleaf = dynamic_cast<coopsc::RegionNode*> (node.get());
	if (regionleaf)
	{
		row[columns.type] = "Region";
		std::stringstream ss;
		ss << regionleaf->getId();
		row[columns.description] = ss.str();
	}

	coopsc::ServerNode* serverleaf = dynamic_cast<coopsc::ServerNode*> (node.get());
	if (serverleaf)
	{
		row[columns.type] = "Remainder";

		Gtk::TreeModel::Row tablerow = *refTreeModel->append(row.children());
		tablerow[columns.type] = "Table";
		tablerow[columns.description] = serverleaf->getQuery().getTableName();

		Gtk::TreeModel::Row fieldsrow = *refTreeModel->append(row.children());
		fieldsrow[columns.type] = "Fields";
		fieldsrow[columns.description] = serverleaf->getQuery().getFieldsAsString();

		std::stringstream ss;
		ss << serverleaf->getQuery().getPredicate();
		Gtk::TreeModel::Row predicaterow = *refTreeModel->append(row.children());
		predicaterow[columns.type] = "Predicate";
		predicaterow[columns.description] = ss.str();
	}

	coopsc::RemoteNode* remoteleaf = dynamic_cast<coopsc::RemoteNode*> (node.get());
	if (remoteleaf)
	{
		row[columns.type] = "Remote Probe";
		std::stringstream ss;
		ss << remoteleaf->getPeerId().first << ":" << remoteleaf->getPeerId().second;
		row[columns.description] = ss.str();
		Gtk::TreeModel::Row childrow = *refTreeModel->append(row.children());
		fill(childrow, remoteleaf->getQueryPlan().getRoot());
	}

	for (coopsc::Node::ChildrenType::const_iterator it = node->getChildren().begin(); it != node->getChildren().end(); it++)
	{
		Gtk::TreeModel::Row childrow = *refTreeModel->append(row.children());
		fill(childrow, *it);
	}
}

QueryTreeView::~QueryTreeView()
{
	connection.disconnect();
}

void QueryTreeView::update()
{
	refTreeModel->clear();
	if (model.getQueryPlan().get() == NULL)
	{
		treeView.hide();
		return;
	}
	Gtk::TreeModel::Row row = *refTreeModel->append();
	fill(row, *model.getQueryPlan());
	treeView.expand_all();
	treeView.show();
}
