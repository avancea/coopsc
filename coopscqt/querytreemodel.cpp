#include "querytreemodel.h"
#include "nodes.h"

QueryTreeModel::QueryTreeModel() {
	// TODO Auto-generated constructor stub

}

void QueryTreeModel::setQueryPlan(coopsc::QueryPlanPtr _plan) {
	clear();
	plan = _plan;
	setHorizontalHeaderItem(0, new QStandardItem("Type"));
	setHorizontalHeaderItem(1, new QStandardItem("Description"));

	if (plan.get() != NULL)
		fill(NULL, *plan);
	reset();
}

QueryTreeModel::~QueryTreeModel() {
	// TODO Auto-generated destructor stub
}

void QueryTreeModel::fill(QStandardItem* parent, const coopsc::QueryPlan& plan) {
	fill(parent, plan.getRoot());
}

void QueryTreeModel::fill(QStandardItem* parent, const coopsc::NodePtr& node) {

	QList<QStandardItem*> items;
	coopsc::SelectProjectNode* selproj =
			dynamic_cast<coopsc::SelectProjectNode*> (node.get());
	if (selproj) {
		QStandardItem* item = new QStandardItem(QString("SelectProject"));
		items << item;

		QList<QStandardItem*> tablerow;
		tablerow << (new QStandardItem(QString("Table"))) << new QStandardItem(
				QString(selproj->getTableName().c_str()));
		item->appendRow(tablerow);

		QList<QStandardItem*> fieldsrow;
		fieldsrow << (new QStandardItem(QString("Fields")))
				<< new QStandardItem(QString(
						selproj->getFieldsAsString().c_str()));
		item->appendRow(fieldsrow);

		std::stringstream ss;
		ss << selproj->getPredicate();

		QList<QStandardItem*> predicaterow;
		predicaterow << (new QStandardItem(QString("Predicate")))
				<< new QStandardItem(QString(ss.str().c_str()));
		item->appendRow(predicaterow);
	}
	if (dynamic_cast<coopsc::UnionNode*> (node.get())) {
		items << new QStandardItem(QString("Union"));
	}

	if (dynamic_cast<coopsc::JoinNode*> (node.get())) {
		items << new QStandardItem(QString("Join"));
	}

	coopsc::RegionNode* regionleaf =
			dynamic_cast<coopsc::RegionNode*> (node.get());
	if (regionleaf) {
		QStandardItem* item0 = new QStandardItem(QString("Region"));

		std::stringstream ss;
		ss << regionleaf->getId();

		QStandardItem* item1 = new QStandardItem(QString(ss.str().c_str()));
		items << item0 << item1;
	}

	coopsc::ServerNode* serverleaf =
			dynamic_cast<coopsc::ServerNode*> (node.get());
	if (serverleaf) {
		QStandardItem* item = new QStandardItem(QString("Remainder"));

		QList<QStandardItem*> tablerow;
		tablerow << (new QStandardItem(QString("Table")))
				<< (new QStandardItem(
						serverleaf->getQuery().getTableName().c_str()));
		item->appendRow(tablerow);

		QList<QStandardItem*> fieldsrow;
		fieldsrow << (new QStandardItem(QString("Fields")))
				<< (new QStandardItem(
						serverleaf->getQuery().getFieldsAsString().c_str()));
		item->appendRow(fieldsrow);

		std::stringstream ss;
		ss << serverleaf->getQuery().getPredicate();
		QList<QStandardItem*> predicaterow;
		predicaterow << (new QStandardItem(QString("Predicate")))
				<< (new QStandardItem(ss.str().c_str()));
		item->appendRow(predicaterow);

		items << item;
	}

	coopsc::RemoteNode* remoteleaf =
			dynamic_cast<coopsc::RemoteNode*> (node.get());
	if (remoteleaf) {
		QStandardItem* item0 = new QStandardItem(QString("Remote Probe"));

		std::stringstream ss;
		ss << remoteleaf->getPeerId().first << ":"
				<< remoteleaf->getPeerId().second;
		QStandardItem* item1 = new QStandardItem(QString(ss.str().c_str()));
		items << item0 << item1;

		fill(item0, remoteleaf->getQueryPlan().getRoot());
	}

	for (coopsc::Node::ChildrenType::const_iterator it =
			node->getChildren().begin(); it != node->getChildren().end(); it++) {
		fill(items.first(), *it);
	}

	if (parent != NULL)
		parent->appendRow(items);
	else
		insertRow(0, items);
}

Qt::ItemFlags QueryTreeModel::flags (const QModelIndex & index ) const
{
	return Qt::ItemIsSelectable | Qt::ItemIsEnabled;
}



