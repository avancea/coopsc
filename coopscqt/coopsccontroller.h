#ifndef COOPSCCONTROLLER_H_
#define COOPSCCONTROLLER_H_
#include "coopscmodel.h"


#include <QObject>
class CoopSCView;

class CoopSCController : public QObject
{
	Q_OBJECT

private:
	CoopSCModel& model;
	CoopSCView& view;
public:
	CoopSCController(CoopSCModel& _model, CoopSCView& _view) :
		model(_model), view(_view)
	{

	}
public slots :
	//executes a queries
	void execute();

	//connects to the database
	void connect();

	//disconnects from the database;
	void disconnect();

	//shows the cache registry
	void showCacheRegistry();

	//quits the application
	void quit();
};

#endif /* COOPSCCONTROLLER_H_ */
