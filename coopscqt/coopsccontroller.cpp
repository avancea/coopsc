#include <stdexcept>
#include <logger.h>
#include "coopsccontroller.h"
#include "coopscview.h"

//executes a queries
void CoopSCController::execute()
{
	try
	{
		model.execute(view.getSql());
	} catch (std::exception& e)
	{
		view.errorMessage(e.what());
	}
}

//connects to the database
void CoopSCController::connect()
{
	int result = view.getConnectDialog().exec();

	if (result != QDialog::Accepted)
		return;

	//sending the options to the model
	model.setDbName(view.getConnectDialog().getDbName());
	model.setDbHost(view.getConnectDialog().getDbHost());
	model.setDbPort(view.getConnectDialog().getDbPort());
	model.setDbUsr(view.getConnectDialog().getDbUsr());
	model.setDbPwd(view.getConnectDialog().getDbPwd());
	model.setCoopSCHost(view.getConnectDialog().getCoopSCHost());
	model.setCoopSCPort(view.getConnectDialog().getCoopSCPort());
	model.setCacheSize(view.getConnectDialog().getCacheSize());
	model.setCacheLevel(view.getConnectDialog().getCacheLevel());
	model.setChimeraPort(view.getConnectDialog().getChimeraPort());
	model.setChimeraBootstrapHost(view.getConnectDialog().getChimeraBootstrapHost());
	model.setChimeraBootstrapPort(view.getConnectDialog().getChimeraBootstrapPort());

	try
	{
		model.connect();
	} catch (std::exception& e)
	{
		view.errorMessage(e.what());
	}
}

//shows the cache registry
void CoopSCController::showCacheRegistry()
{
	model.getCacheRegistryModel().refresh();
	view.getCacheRegistryDialog().exec();

}

//quits the application
void CoopSCController::quit()
{
	view.close();
}

//disconnects from the database;
void CoopSCController::disconnect()
{
	model.disconnect();
}
