#ifndef QUERYTREEMODEL_H_
#define QUERYTREEMODEL_H_
#include <coopsc.h>
#include <QStandardItemModel>


class QueryTreeModel: public QStandardItemModel
{
private:
	coopsc::QueryPlanPtr plan;

	void fill(QStandardItem* parent, const coopsc::QueryPlan& plan);
	void fill(QStandardItem* parent, const coopsc::NodePtr& node);
public:
	QueryTreeModel();
	coopsc::QueryPlanPtr getQueryPlan()
	{
		return plan;
	}
	void setQueryPlan(coopsc::QueryPlanPtr _plan);
	Qt::ItemFlags flags (const QModelIndex & index ) const;
	virtual ~QueryTreeModel();
};

#endif /* QUERYTREEMODEL_H_ */
