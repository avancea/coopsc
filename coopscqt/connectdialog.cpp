#include "connectdialog.h"
#include "coopscview.h"
#include <QDialogButtonBox>



ConnectDialog::ConnectDialog(CoopSCModel& _model, CoopSCView& _view) : QDialog(&_view), model(_model), view(_view)
{
	setWindowTitle("Connect");

	entrydbport = new QSpinBox();
	entrydbport->setRange(1, 65535);
	entrydbport->setSizeIncrement(1, 1);
	entrydbport->setValue(5432);

	entrycoopscport = new QSpinBox();
	entrycoopscport->setRange(1, 65535);
	entrycoopscport->setSizeIncrement(1, 1);
	entrycoopscport->setValue(1999);


	entrychimeraport = new QSpinBox();
	entrychimeraport->setRange(1, 65535);
	entrychimeraport->setSizeIncrement(1, 1);
	entrychimeraport->setValue(9999);


	entrychimerabsport = new QSpinBox();
	entrychimerabsport->setRange(1, 65535);
	entrychimerabsport->setSizeIncrement(1, 1);
	entrychimerabsport->setValue(9999);


	entrycachesize= new QSpinBox();
	entrycachesize->setRange(1, 1024);
	entrycachesize->setSizeIncrement(1, 1);
	entrycachesize->setValue(128);

	entrydbname = new QLineEdit();
	entrydbhost = new QLineEdit();
	entrydbusr = new QLineEdit();
	entrydbpwd = new QLineEdit();
	entrycoopschost = new QLineEdit();
	entrychimerabshost = new QLineEdit();

	entrydbname->setText("cache_test");
	entrydbhost->setText("192.41.135.199");
	entrydbusr->setText("postgres");
	entrydbpwd->setText("postgres");
	entrydbpwd->setEchoMode(QLineEdit::Password);;
	entrycoopschost->setText(CoopSCModel::getLocalIp().c_str());

	combocachelevel = new QComboBox();
	combocachelevel->addItem("No Cache");
	combocachelevel->addItem("Semantic Cache");
	combocachelevel->addItem("Cooperative Semantic Cache");
	combocachelevel->setCurrentIndex(2);

	QGridLayout *l = new QGridLayout();
	l->addWidget(new QLabel("Database Name"), 0, 0);
	l->addWidget(entrydbname, 0, 1);


	l->addWidget(new QLabel("Host"), 1, 0);
	l->addWidget(entrydbhost, 1, 1);

	l->addWidget(new QLabel("Port"), 2, 0);
	l->addWidget(entrydbport, 2, 1);

	l->addWidget(new QLabel("User"), 3, 0);
	l->addWidget(entrydbusr, 3, 1);

	l->addWidget(new QLabel("Password"), 4, 0);
	l->addWidget(entrydbpwd, 4, 1);

	l->addWidget(new QLabel("CoopSC IP"), 5, 0);
	l->addWidget(entrycoopschost, 5, 1);

	l->addWidget(new QLabel("CoopSC Port"), 6, 0);
	l->addWidget(entrycoopscport, 6, 1);

	l->addWidget(new QLabel("Cache Type"), 7, 0);
	l->addWidget(combocachelevel, 7, 1);

	l->addWidget(new QLabel("Cache Size"), 8, 0);
	l->addWidget(entrycachesize, 8, 1);

	l->addWidget(new QLabel("Chimera Port"), 9, 0);
	l->addWidget(entrychimeraport, 9, 1);

	l->addWidget(new QLabel("Chimera Bootstrap Host"), 10, 0);
	l->addWidget(entrychimerabshost, 10, 1);

	l->addWidget(new QLabel("Chimera Bootstrap Port"), 11, 0);
	l->addWidget(entrychimerabsport, 11, 1);


	QDialogButtonBox* buttonBox = new QDialogButtonBox(Qt::Horizontal);

	buttonBox->addButton(QDialogButtonBox::Ok);
	buttonBox->addButton(QDialogButtonBox::Cancel);

    connect(buttonBox, SIGNAL(accepted()), this, SLOT(accept()));
    connect(buttonBox, SIGNAL(rejected()), this, SLOT(reject()));

	QWidget *w = new QWidget();
	w->setLayout(l);

	QVBoxLayout* vl = new QVBoxLayout();
	vl->addWidget(w);
	vl->addWidget(buttonBox);

	vl->setSizeConstraint(QLayout::SetFixedSize);

	setLayout(vl);
}
