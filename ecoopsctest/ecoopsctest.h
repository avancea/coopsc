/*
 * testthread.h
 *
 *  Created on: Dec 7, 2011
 *      Author: andrei
 */

#ifndef TESTTHREAD_H_
#define TESTTHREAD_H_
#include <string>
#include "ecoopsc.h"
#include "generator.h"

class ECoopSCTest
{
private:
	std::string manhost;
	int manport;
	int nosessions;
	int sessionsize;
	ecoopsc::Connection* con;
	ecoopsc::Generator& gen;
	std::vector<int> responsetimes;

public:
	ECoopSCTest(const std::string& _manhost, int _manport,
		 int _nosessions, int _sessionsize,
		 ecoopsc::Generator& _gen): manhost(_manhost), manport(_manport),
						   nosessions(_nosessions), sessionsize(_sessionsize),
						   con(NULL), gen(_gen)
	{

	}

	ECoopSCTest(const ECoopSCTest& that): manhost(that.manhost), manport(that.manport), nosessions(that.nosessions),
										  sessionsize(that.sessionsize),  con(that.con), gen(that.gen),
										  responsetimes(that.responsetimes)
	{

	}

	void operator()();
	const std::vector<int>&  result() const
	{
		return responsetimes;
	}

};

#endif /* TESTTHREAD_H_ */
