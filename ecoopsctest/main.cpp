#include <iostream>
#include <vector>
#include <string>
#include <boost/program_options.hpp>
#include "generator.h"
#include "ecoopsctest.h"
#include "postgrestest.h"

using namespace std;
using namespace boost;
using namespace boost::program_options;

std::string host;
int port;
int nothreads;
int center;
int deviation;
int nosessions;
int sessionsize;
bool useecoopsc;

void parseArgs(int argc, char **argv)
{
	options_description desc("Arguments");
	desc.add_options()
					("help", "produce help message")
					("host", value<string>()->default_value("127.0.0.1"), "manager host")
					("port", value<int>()->default_value(9999), "manager port")
					("useecoopsc", value<bool>()->default_value("true"), "useecoopsc")
					("nothreads", value<int>()->default_value(5), "number of threads")
					("nosessions", value<int>()->default_value(50), "number of sessions")
					("sessionsize", value<int>()->default_value(10), "size of sessions")
					("center", value<int>()->default_value(5000000), "center")
					("deviation", value<int>()->default_value(300000), "deviation");
	variables_map vm;
	store(parse_command_line(argc, argv, desc), vm);
	notify(vm);

	if (vm.count("help"))
	{
		cout << desc << "\n";
		exit(1);
	}

	if (vm.count("host"))
	{
		host = vm["host"].as<string>();
	}
	else
	{
		cout << "Host not set!\n";
		exit(1);
	}
	cout << "Host : " << host << endl;

	if (vm.count("port"))
	{
		port = vm["port"].as<int>();
	}
	else
	{
		cerr << "Port not set!\n";
		exit(1);
	}
	cout << "Port : " << port << endl;

	useecoopsc = vm["useecoopsc"].as<bool>();


	if (vm.count("nothreads"))
	{
		nothreads = vm["nothreads"].as<int>();
	}
	else
	{
		cerr << "No. of threads not set!\n";
		exit(1);
	}
	cout << "No. of  threads : " << nothreads << endl;


	if (vm.count("nosessions"))
	{
		nosessions = vm["nosessions"].as<int>();
	}
	else
	{
		cerr << "No. of sessions not set!\n";
		exit(1);
	}
	cout << "No. of  sessions : " << nosessions << endl;

	if (vm.count("sessionsize"))
	{
		sessionsize = vm["sessionsize"].as<int>();
	}
	else
	{
		cerr << "Session size not set!\n";
		exit(1);
	}
	cout << "Session size : " << sessionsize << endl;


	if (vm.count("center"))
	{
		center = vm["center"].as<int>();
	}
	else
	{
		cerr << "Center not set!\n";
		exit(1);
	}
	cout << "Center: " << center << endl;

	if (vm.count("deviation"))
	{
		deviation = vm["deviation"].as<int>();
	}
	else
	{
		cerr << "Deviation not set!\n";
		exit(1);
	}
	cout << "Deviation: " << deviation << endl;



}

int main(int argc, char **argv)
{
	parseArgs(argc, argv);
	std::vector<std::string> vfields; vfields.push_back("unique1");
	std::vector<int> vcenter; vcenter.push_back(center);
	std::vector<int> vdeviation; vdeviation.push_back(deviation);
	std::vector<int> vsize; vsize.push_back(10000);

    ecoopsc::Generator generator("wisconsin", vfields, vcenter, vdeviation, vsize);


    vector<thread*> threads;

    if (useecoopsc)
    {
		vector<ECoopSCTest*> tests;
		for (int i = 0; i < nothreads; i++)
		{
			tests.push_back(new ECoopSCTest(host, port, nosessions, sessionsize, generator));
			threads.push_back(new boost::thread(boost::ref(*tests[i])));;
		}
    }
    else
    {
 		vector<PostgresTest*> tests;
 		for (int i = 0; i < nothreads; i++)
 		{
 			tests.push_back(new PostgresTest(host, port, nosessions, sessionsize, generator));
 			threads.push_back(new boost::thread(boost::ref(*tests[i])));;
 		}
     }

    for (int i = 0; i < nothreads; i++)
	{
		threads[i]->join();
	}

}
