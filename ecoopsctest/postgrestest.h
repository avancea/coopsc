/*
 * postgrestest.h
 *
 *  Created on: Dec 18, 2011
 *      Author: andrei
 */

#ifndef POSTGRESTEST_H_
#define POSTGRESTEST_H_
#include <string>
#include "generator.h"

class PostgresTest
{
private:
	std::string host;
	int port;
	int nosessions;
	int sessionsize;

	ecoopsc::Generator& gen;
	std::vector<int> responsetimes;

public:
	PostgresTest(const std::string& _host, int _port,
		 int _nosessions, int _sessionsize,
		 ecoopsc::Generator& _gen): host(_host), port(_port),
						   nosessions(_nosessions), sessionsize(_sessionsize),
						   gen(_gen)
	{

	}

	PostgresTest(const PostgresTest& that): host(that.host), port(that.port), nosessions(that.nosessions),
										  sessionsize(that.sessionsize), gen(that.gen),
										  responsetimes(that.responsetimes)
	{

	}

	void operator()();
	const std::vector<int>&  result() const
	{
		return responsetimes;
	}

};


#endif /* POSTGRESTEST_H_ */
