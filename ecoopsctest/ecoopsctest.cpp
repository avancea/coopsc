#include <boost/progress.hpp>
#include <boost/thread.hpp>
#include <iostream>
#include <time.h>
#include "ecoopsctest.h"
#include "logger.h"

using namespace boost;


void ECoopSCTest::operator()()
{
	con = new ecoopsc::Connection(manhost, manport);
	con->connect();
	for (int i = 0; i < nosessions; i++)
	{
		system_time starttime = get_system_time();
		for (int j = 0; j < sessionsize; j++)
		{
			std::string query;
			try {
			query = gen.select();
			} catch (std::exception& e)
			{
				std::cerr << "a: " << e.what() << std::endl;
			}
			try {
			auto result = con->query(query);
			} catch (std::exception& e)
			{
				std::cerr << "b: " << e.what() << std::endl;
			}
		}
		system_time endtime = get_system_time();
		posix_time::time_duration duration = endtime - starttime;
		std::cout << duration.total_milliseconds() << std::endl;
		responsetimes.push_back(duration.total_milliseconds());
	}
	delete con;
}

