#ifndef __COOPSCTEST_GENERATOR
#define __COOPSCTEST_GENERATOR
#include <string>
#include <vector>
#include <utility>
#include <boost/thread.hpp>

namespace ecoopsc
{
//basic query generator
class Generator
{
private:
	std::string table;
	std::vector<std::string> fields;
	std::vector<int> center;
	std::vector<int> dev;
	std::vector<int> size;

	boost::mutex mutex;

	std::pair<int, int> randRange(int fno);
	std::string generateSelectPredicate(int fno);
	std::string generateUpdatePredicate(int fno);

public:
	Generator(const std::string& _table, const std::vector<std::string> _fields, std::vector<int> _center, std::vector<int> _dev,
			std::vector<int> _size) :
		table(_table), fields(_fields), center(_center), dev(_dev), size(_size)
	{
	}

	std::string select();

	std::string update();

};

}
#endif
