#include <boost/progress.hpp>
#include <boost/thread.hpp>
#include <boost/format.hpp>
#include <iostream>
#include <time.h>
#include <libpq-fe.h>
#include "postgrestest.h"

using namespace boost;

void PostgresTest::operator()()
{
	std::string conninfo;
	conninfo = (boost::format("hostaddr = \'%1%\' port = \'%2%\' dbname = \'%3%\' user = \'%4%\' "
		"password = \'%5%\' connect_timeout = \'10\' sslmode = disable") % host % port
			% "ecoopsctest" % "postgres" % "postgres").str();

	PQinitSSL(true);
	auto psql = PQconnectdb(conninfo.c_str());
	for (int i = 0; i < nosessions; i++)
	{
		system_time starttime = get_system_time();
		for (int j = 0; j < sessionsize; j++)
		{
			std::string query;
			query = gen.select();
			PGresult* res;
			res = PQexec(psql, query.c_str());
			if (PQresultStatus(res) != PGRES_TUPLES_OK)
			{
				throw std::runtime_error(PQerrorMessage(psql));
			}
		}
		system_time endtime = get_system_time();
		posix_time::time_duration duration = endtime - starttime;
		std::cout << duration.total_milliseconds() << std::endl;
		responsetimes.push_back(duration.total_milliseconds());
	}
}
