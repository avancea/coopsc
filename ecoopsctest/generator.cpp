#include <cmath>
#include <sstream>
#include "generator.h"
using namespace std;

namespace ecoopsc
{


double rand01()
{
	return ((double) rand() / ((double) (RAND_MAX) + (double) (1)));
}

double randNorm(double mean, double dev) /* normal random generator */
{
	double x1, x2, w, y1;
	static double y2;
	static int use_last = 0;

	if (use_last) /* use value from previous call */
	{
		y1 = y2;
		use_last = 0;
	}
	else
	{
		do
		{
			x1 = 2.0 * rand01() - 1.0;
			x2 = 2.0 * rand01() - 1.0;
			w = x1 * x1 + x2 * x2;
		} while (w >= 1.0);

		w = sqrt((-2.0 * log(w)) / w);
		y1 = x1 * w;
		y2 = x2 * w;
		use_last = 1;
	}

	return mean + y1 * dev;
}

pair<int, int> Generator::randRange(int fno)
{
	int middle;
	middle = randNorm(center[fno], dev[fno]);
	int first = middle - size[fno] / 2;
	int second = first + size[fno] - 1;
	return make_pair(first, second);
}

std::string Generator::generateUpdatePredicate(int fno)
{
	int middle;
	middle = randNorm(center[fno], dev[fno]);
	stringstream ss;
	ss << "(" << fields[fno] << " = " << middle << ")";
	return ss.str();
}

string Generator::generateSelectPredicate(int fno)
{
	pair<int, int> range = randRange(fno);
	stringstream ss;
	ss << "(" << (range.first - 1) << " < " << fields[fno] << " and " << fields[fno] << " < " << (range.second + 1) << ")";
	return ss.str();
}

//generates a random select query
string Generator::select()
{
	boost::mutex::scoped_lock lock(mutex);
	string result = string("select * from ") + table + " where";
	for (unsigned int i = 0; i < fields.size(); i++)
	{
		if (i)
			result += " and";
		result += " " + generateSelectPredicate(i);
	}
	return result;
}

string Generator::update()
{
	boost::mutex::scoped_lock lock(mutex);
	string result = string("update ") + table + " set two = 1 - two where";
	for (unsigned int i = 0; i < fields.size(); i++)
	{
		if (i)
			result += " and";
		result += " " + generateUpdatePredicate(i);
	}
	return result;
}

}
