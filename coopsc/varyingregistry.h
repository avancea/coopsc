/*
 * varyingregistry.h
 *
 *  Created on: Jul 16, 2012
 *      Author: andrei
 */

#ifndef VARYINGREGISTRY_H_
#define VARYINGREGISTRY_H_
#include <map>
#include <boost/thread.hpp>
#include <utility>

namespace coopsc
{

//stores varying data types
class VaryingRegistry
{
private:
	struct VaryingValue
	{
		unsigned int count; // no of tuples which have this value

		void *data;
		unsigned int size;
	};
	int maxid;
	unsigned int size;

	std::map<unsigned int, VaryingValue> values;

	boost::mutex mutex;
	unsigned int lastid;
public:

	VaryingRegistry() {lastid = 0; size = 0; }

	//stores and varying value
	//returns id
	int store(void* data, int size);

	//int entry is the specified ID is stored in a new tuple
	void store(int id);

	void remove(int id);

	unsigned int getSize();

	//		  data,  size
	std::pair<void*, int> get(int id);

};

}


#endif /* VARYINGREGISTRY_H_ */
