cmake_minimum_required(VERSION 2.8)

include_directories (${CoopSC_SOURCE_DIR}/chimera)

file (GLOB coopsc_SRCS *.cpp) 

find_package (Boost 1.40 COMPONENTS program_options thread system signals iostreams serialization REQUIRED)
link_directories (${Boost_LIBRARY_DIRS})
include_directories (${Boost_INCLUDE_DIRS})

find_package (PG REQUIRED)
include_directories (${PG_INCLUDE_DIRS})
link_directories (${PG_LIBRARY_DIRS})

add_library (coopsc ${coopsc_SRCS})

