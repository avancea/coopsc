#include <sstream>
#include <stdexcept>
#include <cstring>
#include <stdarg.h>
#include <boost/format.hpp>
#include <boost/asio.hpp>
#include <boost/archive/binary_oarchive.hpp>
#include <boost/archive/binary_iarchive.hpp>
#include "connection.h"
#include "logger.h"
#include "sqlparser.h"
#include "queryoptimizer.h"
#include "queryplanfactory.h"

namespace coopsc
{

// Return the local ip of the computer
std::string resolveHost(const std::string& host)
{
	try {
		boost::asio::io_service io_service;
		boost::asio::ip::tcp::resolver resolver(io_service);

		boost::asio::ip::tcp::resolver::query query(host, "");
		boost::asio::ip::tcp::resolver::iterator it = resolver.resolve(query);
		std::string result = host;
		while (it != boost::asio::ip::tcp::resolver::iterator())
		{
			boost::asio::ip::address addr = (it++)->endpoint().address();
			if (addr.is_v4())
			{
				std::string tmp = addr.to_string();
				result = tmp;
				break;
			}
		}
		return result;
	} catch (...) {
		return host;
	}
}




Connection::Connection(const std::string& _dbname, const std::string& _dbhost, int _dbport, const std::string& _dbusr, const std::string& _dbpwd,
		const std::string& _coopschost, int _coopscport, CacheType _cachetype, unsigned int _cachesize, const std::string& _chimerahost, int _chimeraport,
		const std::string& _chimerabshost, int _chimerabsport) :
	dbname(_dbname), dbhost(resolveHost(_dbhost)), dbport(_dbport), dbusr(_dbusr), dbpwd(_dbpwd), coopscid(resolveHost(_coopschost), _coopscport), cachetype(_cachetype), cachesize(
			_cachesize), connected(false), dbpool(*this), peerpool(*this), listener(*this), chimerahost(resolveHost(_chimerahost)), chimeraport(_chimeraport),
			chimerabshost(resolveHost(_chimerabshost)), chimerabsport(_chimerabsport), distributedIndex(*this), storagemanager(*this), cachepeerdata(true)
{

}

//connects to the database and to the cache manager
void Connection::connect()
{
	if (connected)
		throw std::runtime_error("Already connected");
	listener.start();
	dbpool.connect();
	dbpool.readMetadata();
	connected = true;
	if (cachetype == CooperativeCache)
	{
		distributedIndex.connect();
	}
}

void Connection::disconnect()
{
	if (!connected)
		throw std::runtime_error("Not connected");
	connected = false;
	listener.stop();
	dbpool.disconnect();
	if (cachetype == CooperativeCache)
	{
		distributedIndex.disconnect();
	}
}

void Connection::query(const Query& q, std::vector<RegionPtr>& result)
{
	{
		boost::mutex::scoped_lock lock(lastquerymutex);
		lastqueryplan.reset();
	}
	QueryPlanFactory factory(*this);

	try
	{
		NQuads nquads;
		q.getTimestampNQuads(nquads);
		NQuadTimestamps timestamps;
		dbpool.getNQuadsTimestamps(nquads, timestamps);

		int maxlevel = getMetadataRegistry().getTable(q.getTableName()).getNQuadMaxLevel(q.getPredicate().getFields());
		NQuads quads;
		q.getNQuads(maxlevel, quads);



		bool first = true;
		unsigned int localRewritingDuration = 0;
		unsigned int distributedRewritingDuration = 0;
		BOOST_FOREACH(NQuad& quad, quads)
		{
			Query qq(*this, q.getTableName(), q.getPredicate() && quad.getPredicate(), q.getFields(), q.getKey());
			NQuadTimestamps ts;

			for (NQuadTimestamps::const_iterator it = timestamps.begin(); it != timestamps.end(); it++)
			{
				if (quad.includes(it->first))
				{
					ts[it->first] = it->second;
				}
			}
			//rewriting using the local cache
			QueryPlan plan;

			{
				boost::system_time starttime = boost::get_system_time();

				plan = getStorageManager().rewrite(qq, ts);

				boost::system_time endtime = boost::get_system_time();
				boost::posix_time::time_duration duration = endtime - starttime;
				localRewritingDuration += duration.total_milliseconds();
			}
			if (cachetype == CooperativeCache)
			{
				boost::system_time starttime = boost::get_system_time();

				distributedIndex.optimize(plan, ts);

				boost::system_time endtime = boost::get_system_time();
				boost::posix_time::time_duration duration = endtime - starttime;
				distributedRewritingDuration += duration.total_milliseconds();
			}

			{
				boost::mutex::scoped_lock lock(lastquerymutex);
				lastqueryplan = QueryPlanPtr(new QueryPlan(plan));
				first = false;
			}
			RegionPtr reg = plan.execute(timestamps);
			result.push_back(reg);
		}
		getStatisticsRegistry().durationLocalRewriting(localRewritingDuration);
		getStatisticsRegistry().durationDistributedRewriting(distributedRewritingDuration);
	} catch (std::exception& e)
	{
		LOG_ERROR(e.what());
		throw;
	}
}

struct StatRAII
{
private:
	boost::system_time starttime;
	Connection& con;
public:
	StatRAII(Connection& _con) :
		con(_con)
	{
		starttime = boost::get_system_time();
		con.getStatisticsRegistry().queryBegin();
	}
	~StatRAII()
	{
		boost::system_time endtime = boost::get_system_time();
		boost::posix_time::time_duration d = endtime - starttime;
		con.getStatisticsRegistry().duration(d.total_milliseconds());
		con.getStatisticsRegistry().queryEnd();
	}
};

boost::mutex tmpmutex;

ResultSetPtr Connection::query(const std::string& q)
{
	//boost::mutex::scoped_lock lock(tmpmutex);
	if (!connected)
		throw std::runtime_error("Not connected");

	StatRAII statraii(*this);

	if (cachetype == NoCache)
	{
		return queryServer(q);
	}

	SQLParser parser(*this);
	std::vector<int> fields;
	Query qd;
	try
	{
		qd = parser.parse(q, fields);
		std::vector<RegionPtr> regions;
		query(qd, regions);
		RegionResultSet *rrs = new RegionResultSet(*this, qd.getTableName(), fields);
		BOOST_FOREACH(RegionPtr reg, regions)
						rrs->addRegion(reg);
		ResultSetPtr resultset = ResultSetPtr(rrs);
		return resultset;
	} catch (...)
	{

		LOG_INFO("Error");
		//parsing error! send the query directly to the server
		StatisticsRegistry::QueryStat& stat = getStatisticsRegistry().getCurrentStat();
		stat.tuplesLocal = 0;
		stat.tuplesPeers.clear();
		return queryServer(q);
	}

}

void Connection::execute(const std::string& s)
{
	if (!connected)
		throw std::runtime_error("Not connected");
	dbpool.execute(s);
}

ResultSetPtr Connection::queryServer(const std::string& q)
{
	lastqueryplan.reset();
	if (!connected)
		throw std::runtime_error("Not connected");
	ResultSetPtr result = dbpool.query(q);
	getStatisticsRegistry().tuplesServer(result->getNoTuples());
	return result;
}

Connection::~Connection()
{
	if (connected)
		disconnect();
}

}
