#ifndef RANGEPREDICATE_H_
#define RANGEPREDICATE_H_

#include <string>
#include <stdexcept>
#include <boost/archive/binary_oarchive.hpp>
#include <boost/archive/binary_iarchive.hpp>
#include <boost/serialization/tracking.hpp>
#include "metadataregistry.h"
#include "evaluator.h"
#include "predicatenode.h"

namespace coopsc
{

class Connection;

//Predicate
class Predicate
{
	friend std::ostream& operator <<(std::ostream& os, const Predicate& p);
	friend class boost::serialization::access;
	friend class Evaluator;
	friend class InterpreterEvaluator;
private:

	//the name of the table
	std::string tablename;
	PredicateNodePtr root;

	Connection* connection;

	template<class Archive>
	void serialize(Archive & ar, const unsigned int version)
	{
		ar & tablename;
		ar & root;
	}

	static void getBounding(const PredicateNodePtr node, FieldsMask fields, unsigned int p, std::vector<int>& minc, std::vector<int>& maxc);

public:
	static const int oo;

	//minus infinity
	static const int _oo;

	Predicate();

	//scalar initialization
	Predicate(Connection& _connection, const std::string& _tablename, bool value = false);

	//constructor
	Predicate(Connection& _connection, const std::string& _tablename, int fieldno, int start, int end);

	Predicate(const Predicate& that);

	void setConnection(Connection& _connection)
	{
		connection = &_connection;
	}

	Connection& getConnection() const
	{
		return *connection;
	}

	// Returns the name of the fields
	FieldsMask getFields() const
	{
		return root->getAllFields();
	}

	//returns the table id
	const std::string& getTableName() const
	{
		return tablename;
	}

	EvaluatorPtr createEvaluator(const std::vector<int>& offsets) const;

	bool isScalar() const
	{
		return root->isScalar();
	}

	Predicate& operator =(const Predicate& that);

	bool operator ==(const Predicate& p) const;

	bool operator !=(const Predicate& p) const
	{
		return !(*this == p);
	}

	//min coordonates 		max coordonates
	void getBounding(std::vector<int>& minc, std::vector<int>& maxc) const;

	//subsumption verification
	bool operator <(const Predicate& p) const;

	bool isTrue() const;

	bool isFalse() const;

	Predicate operator !() const;

	Predicate operator ||(const Predicate& p) const;

	Predicate operator &&(const Predicate& p) const;

	Predicate operator +(const Predicate& p) const
	{
		return (*this) || p;
	}

	Predicate& operator -=(const Predicate& p)
	{
		return (*this) = (*this) && (!p);
	}

	Predicate operator -(const Predicate& p) const
	{
		return (*this) && (!p);
	}

	Predicate operator -() const
	{
		return !(*this);
	}

};

typedef boost::shared_ptr<Predicate> PredicatePtr;

std::ostream& operator <<(std::ostream& os, const Predicate& p);

}
BOOST_CLASS_TRACKING(coopsc::Predicate, boost::serialization::track_never)

#endif /*RANGEPREDICATE_H_*/
