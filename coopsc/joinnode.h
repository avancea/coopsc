#ifndef JOINNODE_H_
#define JOINNODE_H_
#include "node.h"
namespace coopsc
{

//Join operator
//(calculates left joined with right)
class JoinNode: public Node
{
private:
	friend class boost::serialization::access;
	template<class Archive>
	void serialize(Archive & ar, const unsigned int version)
	{
		ar & boost::serialization::base_object<Node>(*this);
	}

protected:
	virtual RegionPtr executeNode(const std::vector<RegionPtr>& childrenresults, bool keepstatistics);
public:
	JoinNode()
	{
	}

	JoinNode(Connection& _connection, NodePtr _left, NodePtr _right) :
		Node(_connection, _left, _right)
	{
	}
	virtual void writeTo(std::ostream& s) const;
};
}

#endif /* JoinNodeNODE_H_ */
