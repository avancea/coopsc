#include <stdexcept>
#include <algorithm>
#include <boost/smart_ptr.hpp>
#include <boost/foreach.hpp>
#include <pthread.h>
#include "region.h"
#include "storagemanager.h"
#include "metadataregistry.h"
#include "connection.h"
#include "logger.h"

using namespace std;
namespace coopsc
{
int Region::noregionsallocated = 0;
Region::Region(Connection& _connection, const Query& _query) :
	connection(_connection), query(_query), indexfactory(*this)
{
	noregionsallocated++;
	tuplesize = -1;
	tupledata = NULL;
	capacity = 0;
	noTuples = 0;


	keysize = 0;
	tuplesize = 0;
	computeTupleSize();
	computeOffsetSize();
	pkeyindex = indexfactory.createIndex(0, keysize);
}

Region::Region(Connection& _connection, const Query& _query, const NQuadTimestamps& _timestamps) :
	connection(_connection), query(_query), indexfactory(*this), timestamps(_timestamps)
{
	noregionsallocated++;
	tuplesize = -1;
	tupledata = NULL;
	capacity = 0;
	noTuples = 0;


	keysize = 0;
	tuplesize = 0;
	computeTupleSize();
	computeOffsetSize();
	pkeyindex = indexfactory.createIndex(0, keysize);
}

Region::Region(Connection& _connection) :
	connection(_connection), query(), indexfactory(*this)
{
	noregionsallocated++;
	tuplesize = -1;
	tupledata = NULL;
	capacity = 0;
	noTuples = 0;
	keysize = 0;
	tuplesize = 0;


}

int Region::getTimestamp(const NQuad& nquad) const
{
	auto it = timestamps.find(nquad);
	if (it == timestamps.end())
	{
		throw std::runtime_error("NQuad not found");
	}
	return it->second;
}

std::ostream& operator <<(std::ostream& os, const Region& s)
{
	os << "(tupleSize = " << s.tuplesize << "), ";
	os << "(keySize = " << s.keysize << "), ";
	os << "(noTuples = " << s.noTuples << "), ";
	os << "(capacity = " << s.capacity << "), ";
	os << "(query = " << s.query << ")";
	return os;
}

void Region::reduceTimestamps()
{
	for (auto it = timestamps.begin(); it != timestamps.end();)
	{
		auto next = it;
		next++;
		if ((query.getPredicate() && (it->first.getPredicate())).isFalse())
		{
			timestamps.erase(it);
		}

		it = next;
	}
}

void Region::computeTupleSize()
{
	tuplesize = 0;
	keysize = 0;

	const Table& table = connection.getMetadataRegistry().getTable(query.getTableName());

	FieldsMask mask = query.getFields();

	for (unsigned int i = 0; mask; i++)
		if (test(mask, i))
		{
			tuplesize += table.getField(i).getSize();
			if (test(query.getKey(), i))
				keysize += table.getField(i).getSize();
			clear(mask, i);
		}
}

void Region::computeOffsetSize()
{
	int offset = 0;

	const Table& table = connection.getMetadataRegistry().getTable(query.getTableName());
	FieldsMask f[2] =
	{ query.getKey(), query.getFields() & (~query.getKey()) };
	for (int k = 0; k < 2; k++)
	{
		for (unsigned int i = 0; f[k]; i++)
			if (test(f[k], i))
			{
				offsetsize[i] = std::make_pair(offset, table.getField(i).getSize());
				offset += table.getField(i).getSize();
				clear(f[k], i);
			}
	}
}

void Region::reindex()
{
	pkeyindex->clear();
	char *ptr = (char*) tupledata;
	for (unsigned int i = 0; i < noTuples; i++)
	{
		index(ptr);
		ptr += tuplesize;
	}
}

struct CopyOp
{
	unsigned int from;
	unsigned int to;
	unsigned int size;
	CopyOp()
	{
	}
	CopyOp(unsigned int _from, unsigned int _to, unsigned int _size) :
		from(_from), to(_to), size(_size)
	{
	}
};



void* Region::insert()
{
	if (noTuples >= capacity)
	{
		if (capacity)
		setCapacity(capacity * 2);
		else
		setCapacity(30);
	}
	noTuples++;
	return tuple(noTuples - 1);
}


void Region::setCapacity(int _capacity)
{
	capacity = _capacity;
	if (tupledata == NULL)
	tupledata = (char*) malloc(_capacity * tuplesize);
	else
	tupledata = (char*) realloc(tupledata, _capacity * tuplesize);
	if (tupledata == NULL) throw std::runtime_error("Can not allocate!");
	capacity = _capacity;
	if (noTuples > capacity)
	noTuples = capacity;
}




//executes a union query
RegionPtr Region::unionq(const std::vector<RegionPtr>& that)
{

	int totaltuples;
	totaltuples = this->getNoTuples();
	Predicate pred = this->getQuery().getPredicate();
	NQuadTimestamps ts = timestamps;
	;
	for (unsigned int i = 0; i < that.size(); i++)
	{
		if (that[i]->query.getTableName() != this->query.getTableName())
			throw std::runtime_error("Can not union : different tables");
		if (that[i]->query.getFields() != this-> query.getFields())
			throw std::runtime_error("Can not union : different fields");
		totaltuples += (that[i]->getNoTuples());
		pred = pred || that[i]->getQuery().getPredicate();
		for (auto it = that[i]->timestamps.begin(); it != that[i]->timestamps.end(); it++)
		{
			auto jt = ts.find(it->first);
			if (jt != ts.end())
			{
				if (it->second != jt->second)
					throw std::runtime_error("Can not union : different snapshots");
			}
			else
				ts[it->first] = it->second;
		}
	}
	Query q(connection, this->query.getTableName(), pred, this->getQuery().getFields());

	RegionPtr result(new Region(connection, q));
	result->setCapacity(totaltuples);

	memcpy(result->tupledata, this->tupledata, this->getNoTuples() * this->getTupleSize());
	int p = this->getNoTuples();
	for (unsigned int i = 0; i < that.size(); i++)
	{
		memcpy((char*) result->tupledata + p * this->getTupleSize(), that[i]->tupledata, that[i]->getNoTuples() * that[i]->getTupleSize());
		p += that[i]->getNoTuples();
	}
	result->noTuples = totaltuples;
	result->timestamps = ts;
	result->reindex();
	return result;
}

//executes a join query
RegionPtr Region::join(RegionPtr that)
{
	if (that->query.getTableName() != this->query.getTableName())
		throw std::runtime_error("Can not join : different tables");
	if (that->query.getPredicate() != this->query.getPredicate())
	{
		throw std::runtime_error("Can not join : different predicates");
	}
	if (this->timestamps != that->timestamps)
		throw std::runtime_error("Can not join! : different snapshots");
	FieldsMask fields = (this->getQuery().getFields() | that->getQuery().getFields());
	FieldsMask key = this->getQuery().getKey();

	std::vector<CopyOp> thistocopy; //the memory that must be copied from "this" to the result
	std::vector<CopyOp> thattocopy; //the memory that must be copied from "that" to the result

	Query q(connection, this->query.getTableName(), this->getQuery().getPredicate(), fields, key);
	RegionPtr result(new Region(connection, q));
	result->setCapacity(this->getNoTuples());

	thistocopy.push_back(CopyOp(0, 0, keysize)); //copying the primary key from "this"

	FieldsMask mask;
	mask = (this->getQuery().getFields() & ~(query.getKey()));
	for (unsigned int i = 0; mask; i++)
		if (test(mask, i))
		{
			CopyOp& last = thistocopy[thistocopy.size() - 1];
			CopyOp op;
			op.from = this->getOffsetSize(i).first;
			op.to = result->getOffsetSize(i).first;
			op.size = this->getOffsetSize(i).second;

			if (last.from + last.size == op.from && last.to + last.size == op.to)
			{
				last.size += op.size;
			}
			else
				thistocopy.push_back(op);
			clear(mask, i);
		}

	// the rest of fields are copied from "that"
	mask = (fields & ~(this->getQuery().getFields()));
	for (unsigned int i = 0; mask; i++)
		if (test(mask, i))
		{
			CopyOp op;
			op.from = that->getOffsetSize(i).first;
			op.to = result->getOffsetSize(i).first;
			op.size = that->getOffsetSize(i).second;

			if (thattocopy.size() == 0)
			{
				thattocopy.push_back(op);
			}
			else
			{
				CopyOp& last = thattocopy[thattocopy.size() - 1];
				if (last.from + last.size == op.from && last.to + last.size == op.to)
				{
					last.size += op.size;
				}
				else
				{
					thattocopy.push_back(op);
				}
			}
			clear(mask, i);
		}

	char *t = (char*) tupledata;
	for (unsigned int i = 0; i < noTuples; i++)
	{
		//find the corresponding tuple from "that" from the primary key
		// (the primary key is always stored at the beginning of the tuple
		char *tt = (char*) that->pkeyindex->find(t);

		char* new_tuple = (char*) result->insert();

		//copying data
		for (auto it = thistocopy.begin(); it != thistocopy.end(); it++)
			memcpy(new_tuple + it->to, t + it->from, it->size);
		for (auto it = thattocopy.begin(); it != thattocopy.end(); it++)
			memcpy(new_tuple + it->to, tt + it->from, it->size);

		result->index(new_tuple);

		t += tuplesize;
	}
	result->timestamps = timestamps;
	return result;
}

//executes a select project query
RegionPtr Region::selectproject(FieldsMask fields, const Predicate& pred) //predicate
{
	if (query.getTableName() != pred.getTableName())
		throw std::runtime_error("Can not select!");
	if ((query.getFields() & fields) != fields)
	{
		throw std::logic_error("Can not select!");
	}

	//predicate fields
	FieldsMask predfields;
	predfields = pred.getFields();
	std::vector<int> predoffset;

	FieldsMask mask = predfields;
	for (unsigned int i = 0; mask; i++)
		if (test(mask, i))
		{
			auto jt = offsetsize.find(i);
			if (jt == offsetsize.end())
				throw std::logic_error("Can not select!");
			predoffset.push_back(jt->second.first);
			clear(mask, i);
		}

	RegionPtr result = RegionPtr(new Region(connection, Query(connection, query.getTableName(), pred && query.getPredicate(), fields)));

	std::vector<CopyOp> tocopy;

	//the key
	tocopy.push_back(CopyOp(0, 0, keysize));

	int offset = keysize;
	mask = (fields & ~(query.getKey()));

	for (unsigned int i = 0; mask; i++)
		if (test(mask, i))
		{
			CopyOp& last = tocopy[tocopy.size() - 1];
			if (last.from + last.size == offsetsize[i].first)
				last.size += offsetsize[i].second;
			else
				tocopy.push_back(CopyOp(offsetsize[i].first, offset, offsetsize[i].second));
			offset += offsetsize[i].second; //adding size

			clear(mask, i);
		}

	result->setCapacity(noTuples);
	char *t = (char*) tupledata; // first tuple

	EvaluatorPtr eval = pred.createEvaluator(predoffset);

	for (unsigned int i = 0; i < noTuples; i++)
	{
		if (eval->evaluate(t))
		{
			char* new_tuple = (char*) result->insert();

			for (auto it = tocopy.begin(); it != tocopy.end(); it++)
				memcpy(new_tuple + it->to, t + it->from, it->size);
			result->index(new_tuple);
		}
		t += tuplesize;
	}
	result->setCapacity(result->noTuples);
	result->timestamps = timestamps;
	result->reduceTimestamps();
	return result;
}

unsigned int Region::checkSum()
{
	unsigned int result = 0;
	for (unsigned int i = 0; i < tuplesize * noTuples; i++)
	{
		result += ((char*) tupledata)[i];
	}
	return result;
}

RegionResultSet::RegionResultSet(Connection& _connection, const std::string& _tablename, const std::vector<int>& _fields) :
	connection(_connection), tablename(_tablename), fields(_fields)
{
	notuples = 0;
	table = connection.getMetadataRegistry().getTable(tablename);
}

void RegionResultSet::addRegion(RegionPtr reg)
{
	if (reg->getNoTuples() == 0)
		return;
	if (regionindex.size() == 0)
	{
		for (auto it = fields.begin(); it != fields.end(); it++)
		{
			int f = *it;
			offset.push_back(reg->getOffsetSize(f).first);
		}
	}
	notuples += reg->getNoTuples();
	regionindex[notuples] = make_pair(notuples - reg->getNoTuples(), reg);
}

unsigned int RegionResultSet::getNoTuples() const
{
	return notuples;
}

unsigned int RegionResultSet::getNoFields() const
{
	return fields.size();
}

std::string RegionResultSet::getFieldName(int fieldno) const
{
	return table.getField(fields[fieldno]).getName();
}

std::string RegionResultSet::getValue(unsigned int tupleno, unsigned int fieldno) const
{
	if (tupleno >= getNoTuples())
		throw std::range_error("Range error : tupleno");
	if (fieldno >= fields.size())
		throw std::range_error("Range error : fieldno");
	Field field = table.getField(fields[fieldno]);

	auto it = regionindex.upper_bound(tupleno);
	RegionPtr region = it->second.second;
	unsigned int x = it->second.first;

	void *ptr = (char*) region->tuple(tupleno - x) + offset[fieldno];
	std::stringstream result;
	switch (field.getType())
	{
	case Field::STRING:
		result << (char*) ptr;
		break;
	case Field::INT:
		result << *((int*) ptr);
		break;
	case Field::BIGINT:
		result << *((long long*) ptr);
		break;
	case Field::DOUBLE:
		result << *((double*) ptr);
		break;
	default:
		throw std::runtime_error("Unknown field type");
	}
	return result.str();
}

std::string RegionResultSet::getValue(unsigned int tupleno, const std::string& field) const
{
	for (unsigned int i = 0; i < fields.size(); i++)
		if (table.getField(fields[i]).getName() == field)
			return getValue(tupleno, i);
	throw std::runtime_error("Field not found");
}

}
