#include <algorithm>
#include "logger.h"
#include "queryoptimizer.h"
#include "connection.h"
#include "nodes.h"
#include "queryplanfactory.h"
namespace coopsc
{

NodePtr QueryOptimizer::postorder(NodePtr node)
{
	if (node.get() == NULL)
		return node;

	//traversing the children
	for (auto it = node->getChildren().begin(); it != node->getChildren().end(); it++)
	{
		*it = postorder(*it);
	}
	return optimizeNode(node);
}

NodePtr ReducerOptimizer::optimizeNode(NodePtr node)
{

	QueryPlanFactory factory(connection);

	//join(remote(x), remote(y))  -> remote(join(x, y))
	JoinNode* join = dynamic_cast<JoinNode*> (node.get());
	if (join != NULL)
	{
		RemoteNode* left = dynamic_cast<RemoteNode*> (node->first().get());
		RemoteNode* right = dynamic_cast<RemoteNode*> (node->second().get());
		if (left == NULL || right == NULL) //not all children are remote
			return node;
		if (left->getPeerId() != right->getPeerId()) //different peers
			return node;
		QueryPlan plan = factory.queryPlan(factory.join(left->getQueryPlan().getRoot(), right->getQueryPlan().getRoot()));

		return factory.remote(plan, left->getPeerId());
	}

	//selectproject(remote(x)) -> remote(selectproject(x))
	SelectProjectNode* selproj = dynamic_cast<SelectProjectNode*> (node.get());
	if (selproj != NULL)
	{
		NodePtr child = node->first();

		RemoteNode* remotechild = dynamic_cast<RemoteNode*> (child.get());
		if (remotechild == NULL)
			return node;
		node->getChildren().clear();
		node->getChildren().push_back(remotechild->getQueryPlan().getRoot());
		remotechild->getQueryPlan().setRoot(node);

		return child;
	}

	UnionNode* unode = dynamic_cast<UnionNode*> (node.get());
	if (unode == NULL) //not an union node
		return node;

	// union (... union(x, y), ...) -> union(... .x, y, ...)
	Node::ChildrenType children;

	for (auto it = node->getChildren().begin(); it != node->getChildren().end(); it++)
		if (dynamic_cast<UnionNode*> (it->get()))
		{
			children.insert(children.end(), (*it)->getChildren().begin(), (*it)->getChildren().end());
		}
		else
			children.push_back(*it);

	// union (....server(pred1), server(pred2)...) -> union(...server(pred1 or pred2) ...)
	// union(... remote(x), remote(y), ...)  -> union(.... remote(union(x, y)), ...)
	// union(..., selectproject(region(X), p1), selectproject(region(X), p2), ....) ->
	//                  union(..., selectproject(region(X), p1 or p2), ...)
	NodePtr servernode;

	//ip        port
	std::map<std::pair<std::string, int>, NodePtr> remotenodes;

	std::map<int, NodePtr> selectregionnode;
	std::vector<NodePtr> regularnodes;

	for (auto it = children.begin(); it != children.end(); it++)
	{
		ServerNode* serverquery = dynamic_cast<ServerNode*> (it->get());
		if (serverquery != NULL)
		{
			if (servernode.get() == NULL)
				servernode = *it;
			else
			{
				std::string table = serverquery->getQuery().getTableName();
				Predicate pred = ((ServerNode*) servernode.get())->getQuery().getPredicate() || serverquery->getQuery().getPredicate();
				FieldsMask fields = serverquery->getQuery().getFields();
				FieldsMask key = serverquery->getQuery().getKey();
				((ServerNode*) servernode.get())->getQuery() = Query(connection, table, pred, fields, key);
			}
			continue;
		}
		RemoteNode* remotenode = dynamic_cast<RemoteNode*> (it->get());
		if (remotenode != NULL)
		{
			PeerId p = remotenode->getPeerId();
			if (remotenodes[p].get() == NULL)
				remotenodes[p] = *it;
			else
			{
				RemoteNode* r = (RemoteNode*) remotenodes[p].get();
				r->getQueryPlan().setRoot(factory.uni(r->getQueryPlan().getRoot(), remotenode->getQueryPlan().getRoot()));
			}
			continue;
		}
		SelectProjectNode* selproj = dynamic_cast<SelectProjectNode*> (it->get());
		if (selproj != NULL)
		{
			RegionNode* reg = dynamic_cast<RegionNode*> (selproj->first().get());
			if (reg != NULL)
			{
				if (selectregionnode[reg->getId()].get() == NULL)
					selectregionnode[reg->getId()] = *it;
				else
				{
					SelectProjectNode* s = (SelectProjectNode*) selectregionnode[reg->getId()].get();
					s->setPredicate(s->getPredicate() || selproj->getPredicate());
				}
				continue;
			}
		}
		regularnodes.push_back(*it);
	}

	node->getChildren().clear();
	node->getChildren().insert(node->getChildren().end(), regularnodes.begin(), regularnodes.end());

	for (auto it = selectregionnode.begin(); it != selectregionnode.end(); it++)
	{
		node->getChildren().push_back(it->second);
	}

	for (auto it = remotenodes.begin(); it != remotenodes.end(); it++)
		if (it->second.get() != NULL)
		{
			optimize(((RemoteNode*) (it->second.get()))->getQueryPlan());
			node->getChildren().push_back(it->second);
		}
	if (servernode.get() != NULL)
		node->getChildren().push_back(servernode);
	if (node->getChildren().size() == 1)
		return node->first();
	return node;
}

NodePtr SelectProjectOptimizer::optimizeNode(NodePtr node)
{
	SelectProjectNode* sp = dynamic_cast<SelectProjectNode*> (node.get());
	if (sp == NULL) //not a selectproject
		return node;
	NodePtr child = sp->first();
	RegionNode* regleaf = dynamic_cast<RegionNode*> (child.get());
	if (regleaf == NULL)
		return node;

	RegionPtr reg = connection.getStorageManager().getRegion(regleaf->getId());
	if (sp->getFields() == reg->getQuery().getFields() && reg->getQuery().getPredicate() < sp->getPredicate())
		return child;
	return node;
}

}

