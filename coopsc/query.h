#ifndef QUERYDESC_H_
#define QUERYDESC_H_

#include <set>
#include <string>
#include <sstream>
#include <algorithm>
#include <boost/archive/binary_oarchive.hpp>
#include <boost/archive/binary_iarchive.hpp>
#include <boost/serialization/tracking.hpp>
#include <boost/serialization/set.hpp>
#include <boost/smart_ptr.hpp>
#include "predicate.h"
#include "metadataregistry.h"
#include "nquad.h"

namespace coopsc
{

class Connection;

//the description of a query
class Query
{
	friend class boost::serialization::access;
	friend std::ostream& operator <<(std::ostream& os, const Query& q);
private:

	//the name of the table
	std::string tablename;

	Predicate predicate;

	//the mask of all fields
	FieldsMask fields;

	//the mask of the fields the form the primary key
	FieldsMask key;

	Connection* connection;

	template<class Archive>
	void serialize(Archive & ar, const unsigned int version)
	{
		ar & tablename;
		ar & predicate;
		ar & fields;
		ar & key;
	}

public:

	Query() :
		connection(NULL)
	{
	}

	Query(const Query& that) :
		tablename(that.tablename), predicate(that.predicate), fields(that.fields), key(that.key), connection(that.connection)
	{
	}

	Query(Connection& _connection, const std::string& _tablename, const Predicate& _predicate, FieldsMask _fields);

	Query(Connection& _connection, const std::string& _tablename, const Predicate& _predicate, FieldsMask _fields, FieldsMask _key);

	void setConnection(Connection& _connection)
	{
		connection = &_connection;
		predicate.setConnection(*connection);
	}

	//returns the predicate
	const Predicate& getPredicate() const
	{
		return predicate;
	}

	FieldsMask getFields() const
	{
		return fields;
	}

	FieldsMask getKey() const
	{
		return key;
	}

	//returns the name of the table
	const std::string& getTableName() const
	{
		return tablename;
	}

	std::string getFieldsAsString() const;

	bool verticallyContains(const Query& q) const;
	bool horizontallyContains(const Query& q) const;
	bool contains(const Query& q) const;
	bool verticallyIntersects(const Query& q, FieldsMask& result) const;
	bool horizontallyIntersect(const Query& q, Predicate& result) const;

	//returns the list of nquads of specified level that intersect the query
	void getNQuads(unsigned int level, NQuads& result) const;
	void getTimestampNQuads(NQuads& result) const;
};

typedef boost::shared_ptr<Query> QueryPtr;

std::ostream& operator <<(std::ostream& os, const Query& q);

}

BOOST_CLASS_TRACKING(coopsc::Query, boost::serialization::track_never)

#endif /*QUERYDESC_H_*/
