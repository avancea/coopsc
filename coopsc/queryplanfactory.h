#ifndef QUERYPLANFACTORY_H_
#define QUERYPLANFACTORY_H_
#include "queryplan.h"
#include "connection.h"

namespace coopsc
{

//Factory class for QueryPlan, Nodes and its subclasses
class QueryPlanFactory
{
private:
	Connection& connection;
public:
	QueryPlanFactory(Connection& _connection) :
		connection(_connection)
	{
	}

	//creates a select project node
	NodePtr selectProject(const std::string& tablename, FieldsMask fields, const Predicate& predicate, NodePtr child);

	//creates an union node
	NodePtr uni(NodePtr left, NodePtr right);

	//creates a join node
	NodePtr join(NodePtr left, NodePtr right);

	//creates a remote node
	NodePtr remote(QueryPlan plan, const PeerId& peerId);

	//creates a region node
	NodePtr region(int id);

	//creates a server query
	NodePtr server(const Query& query);

	//creates a query plan
	QueryPlan queryPlan(NodePtr node);

};

}

#endif /* QUERYPLANFACTORY_H_ */
