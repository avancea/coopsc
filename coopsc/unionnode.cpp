#include "unionnode.h"
#include "connection.h"

namespace coopsc
{
RegionPtr UnionNode::executeNode(const std::vector<RegionPtr>& childrenresults, bool keepstatistics)
{
	return childrenresults[0]->unionq(std::vector<RegionPtr>(childrenresults.begin() + 1, childrenresults.end()));
}

void UnionNode::writeTo(std::ostream& s) const
{
	s << "UnionNode(";
	auto it = getChildren().begin();
	(*it)->writeTo(s);
	it++;
	for (; it != getChildren().end(); it++)
	{
		s << ",";
		(*it)->writeTo(s);
	}
	s << ")";
}
}
