#include <stdexcept>
#include <boost/archive/binary_iarchive.hpp>
#include <boost/archive/binary_oarchive.hpp>
#include <boost/foreach.hpp>
#include <boost/lexical_cast.hpp>
#include <sstream>
#include <algorithm>
#include <iosfwd>
#include <boost/iostreams/categories.hpp>
#include <boost/iostreams/stream.hpp>
#include <boost/iostreams/filtering_stream.hpp>
#include <boost/range/iterator_range.hpp>

extern "C"
{
#include <chimera.h>
#include <route.h>
}
#include "logger.h"
#include "connection.h"
#include "distributedindex.h"
#include "queryplanfactory.h"
#include "nodes.h"
#include "queryoptimizer.h"

using namespace std;
using namespace boost;
using namespace boost::iostreams;

namespace coopsc
{

class RewritingChildrenIntegrator: public QueryOptimizer
{
private:
	Connection& con;
	std::string prefix;
	std::map<std::string, QueryPlan> result;
	NQuad nquad;
	QueryPlanFactory factory;
	int lastchild;
	NQuads children;
protected:
	NodePtr optimizeNode(NodePtr node);
public:
	RewritingChildrenIntegrator(Connection& _con, const std::string& _prefix, std::map<std::string, QueryPlan>& _result, NQuad& _nquad) :
		con(_con), prefix(_prefix), result(_result), nquad(_nquad), factory(con), lastchild(0)
	{

		unsigned int minlevel = con.getMetadataRegistry().getTable(nquad.getTableName()).getNQuadMinLevel(nquad.getFields());
		if (nquad.getLevel() > minlevel)
			nquad.getChildren(children);
	}
};

class DistributedIndexOptimizer: public QueryOptimizer
{
private:
	DistributedIndex& distributedIndex;
	const NQuadTimestamps& timestamps;
public:
	DistributedIndexOptimizer(DistributedIndex& _distributedIndex, const NQuadTimestamps& _timestamps) :
		distributedIndex(_distributedIndex), timestamps(_timestamps)
	{

	}
protected:
	NodePtr optimizeNode(NodePtr node);
};

std::map<ChimeraState*, DistributedIndex*> instances;

void deliveryHandler(ChimeraState *chimerastate, Key *key, Message *msg)
{
	DistributedIndex* instance = instances[chimerastate];
	if (!instance)
	{
		LOG_ERROR("instance is NULL");
		return;
	}
	instance->handleMessage(msg->type, msg->payload, msg->size);
}

void hashPeerId(Key* hashed, const PeerId& peerId)
{
	std::stringstream ss;
	ss << peerId;
	std::string s = ss.str();
	key_make_hash(hashed, const_cast<char*> (s.c_str()), s.size());
}

void hashNQuad(Key* hashed, const NQuad& quad)
{
	std::stringstream ss;
	ss << quad;
	std::string s = ss.str();
	key_make_hash(hashed, const_cast<char*> (s.c_str()), s.size());
}

DistributedIndex::DistributedIndex(Connection& _connection) :
	connection(_connection), chimerastate(NULL), lastsessionid(0), connected(false)
{
}

DistributedIndex::~DistributedIndex()
{
	if (chimerastate)
	{
		chimera_leave((ChimeraState*) chimerastate);
		instances.erase((ChimeraState*) chimerastate);
	}
}

void DistributedIndex::initChimeraMessages()
{
	chimera_register((ChimeraState*) chimerastate, ADD_REGION, 2);
	chimera_register((ChimeraState*) chimerastate, REMOVE_REGION, 1);
	chimera_register((ChimeraState*) chimerastate, REPLICATE_ADD_REGION, 2);
	chimera_register((ChimeraState*) chimerastate, REPLICATE_REMOVE_REGION, 2);
	chimera_register((ChimeraState*) chimerastate, REWRITE_REQUEST, 2);
	chimera_register((ChimeraState*) chimerastate, REWRITE_RESPONSE, 2);
	chimera_register((ChimeraState*) chimerastate, REMOVE_REGION_REVERSE, 2);
	chimera_deliver((ChimeraState*) chimerastate, deliveryHandler);
}

void DistributedIndex::connect()
{
	log_init();
	ChimeraState* state = chimera_init(connection.getChimeraPort(), connection.getChimeraHost().c_str());

	Key hashed;
	hashPeerId(&hashed, connection.getCoopSCId());
	chimera_setkey(state, hashed);

	chimerastate = state;
	if (!chimerastate)
		throw std::runtime_error("Error initializing chimera");
	initChimeraMessages();

	ChimeraHost* bh = NULL;
	if (connection.getChimeraBootstrapHost() != "")
		bh
				= host_get_by_name(state, const_cast<char*> (connection.getChimeraBootstrapHost().c_str()),
						connection.getChimeraBootstrapPort());
	chimera_join(state, bh);

	me = get_key_string(&((ChimeraGlobal*) state->chimera)->me->key);
	instances[state] = this;
	connected = true;
}

void DistributedIndex::disconnect()
{
	if (!connected)
		return;
	chimera_leave((ChimeraState*) chimerastate);
	connected = false;
}

void DistributedIndex::handleMessage(int type, char* payload, unsigned int size)
{
	if (!connected)
		return;
	try
	{
		switch (type)
		{
		case ADD_REGION:
			receiveAddRegion(payload, size);
			replicate(REPLICATE_ADD_REGION, payload, size);
			break;
		case REPLICATE_ADD_REGION:
			receiveAddRegion(payload, size);
			break;
		case REMOVE_REGION:
			receiveRemoveRegion(payload, size);
			replicate(REPLICATE_REMOVE_REGION, payload, size);
			break;
		case REPLICATE_REMOVE_REGION:
			receiveRemoveRegion(payload, size);
			break;
		case REWRITE_REQUEST:
			receiveRewriteRequest(payload, size);
			break;
		case REWRITE_RESPONSE:
			receiveRewriteResponse(payload, size);
			break;
		case REMOVE_REGION_REVERSE:
			receiveRemoveRegionReverse(payload, size);
			break;
		default:
			LOG_ERROR("Invalid message:" << type);
			return;
		}
	} catch (const std::exception& e)
	{
		LOG_ERROR("type : " << type);
		LOG_ERROR("Distributed index exception : " << e.what());
		throw;
	}
}

void DistributedIndex::replicate(int type, char* payload, unsigned int size)
{
	if (NO_REPLICAS == 0)
		return;
	ChimeraHost **leafset;
	leafset = route_neighbors((ChimeraState*) chimerastate, NO_REPLICAS);
	for (int i = 0; leafset[i] != NULL; i++)
	{
		chimera_send((ChimeraState*) chimerastate, leafset[i]->key, type, size, payload);
		host_release((ChimeraState*) chimerastate, leafset[i]);
	}
	free(leafset);
}

void DistributedIndex::receiveAddRegion(char *msg, unsigned int size)
{
	filtering_istream in(make_iterator_range(msg, msg + size));
	boost::archive::binary_iarchive archive(in);

	NQuad nquad;
	RegionDescriptor reg;
	NQuadTimestamps ts;

	archive >> nquad;
	archive >> reg;
	archive >> ts;

	nquad.setConnection(connection);
	reg.setConnection(connection);
	getNQuadNode(nquad)->addRegion(reg, ts);
}

void DistributedIndex::receiveRemoveRegion(char *msg, unsigned int size)
{
	filtering_istream in(make_iterator_range(msg, msg + size));
	boost::archive::binary_iarchive archive(in);

	NQuad nquad;
	RegionDescriptor reg;

	archive >> nquad >> reg;

	nquad.setConnection(connection);
	reg.setConnection(connection);

	getNQuadNode(nquad)->removeRegion(reg);
}

void DistributedIndex::receiveRewriteRequest(char *msg, unsigned int size)
{
	filtering_istream in(make_iterator_range(msg, msg + size));
	boost::archive::binary_iarchive archive(in);

	NQuad nquad;
	PeerId source;
	int reqId;
	std::string prefix;
	Query query;
	NQuadTimestamps ts;

	archive >> nquad >> source >> reqId >> prefix >> query >> ts;
	nquad.setConnection(connection);
	query.setConnection(connection);

	getNQuadNode(nquad)->rewrite(source, reqId, prefix, query, ts);
}

void DistributedIndex::receiveRewriteResponse(char *msg, unsigned int size)
{
	filtering_istream in(make_iterator_range(msg, msg + size));
	boost::archive::binary_iarchive archive(in);

	PeerId dest;
	int level;
	int sid;
	std::string prefix;
	QueryPlan plan;
	int childrenrequests;

	archive >> dest >> level >> sid >> prefix >> plan >> childrenrequests;


	if (dest != connection.getCoopSCId()) return;

	RewritingSession* rewsession;
	{
		boost::mutex::scoped_lock lock(indexmutex);
		if (rewsessions.find(sid) == rewsessions.end()) return;
		rewsession = rewsessions[sid].get();
	}

	boost::mutex::scoped_lock lock(rewsession->rewcondmutex);

	plan.setConnection(connection);
	rewsession->result[prefix] = plan;
	rewsession->nolevelrequests[level]--;
	rewsession->nolevelrequests[level - 1] += childrenrequests;

	bool finish = true;
	for (auto it = rewsession->nolevelrequests.begin(); it != rewsession->nolevelrequests.end(); it++)
	{
		if (it->second != 0)
		{
			finish = false;
			break;
		}
	}

	if (finish)
	{
		rewsession->rewritingfinished = true;
		rewsession->rewcond.notify_one();
	}
}

void DistributedIndex::receiveRemoveRegionReverse(char *msg, unsigned int size)
{
	filtering_istream in(make_iterator_range(msg, msg + size));
	boost::archive::binary_iarchive archive(in);
	int regId;
	archive >> regId;
	connection.getStorageManager().removeRegion(regId, false);
}

//adds a semantic region to the distributed index
void DistributedIndex::add(int regId, RegionPtr reg)
{
	unsigned int maxlevel = connection.getMetadataRegistry().getTable(reg->getQuery().getTableName()).getNQuadMaxLevel(
			reg->getQuery().getPredicate().getFields());

	NQuad quad;
	NQuad::getPredicateNQuad(reg->getQuery().getPredicate(), quad);

	NQuad quadmax = quad;
	if (quad.getLevel() > maxlevel)
		return;
	while (quadmax.getLevel() < maxlevel)
		quadmax = quadmax.getParent();

	addRegion(quadmax, RegionDescriptor(connection.getCoopSCId(), regId, reg->getQuery()), reg->getTimestamps());
}

//removes a semantic region from the index
void DistributedIndex::remove(int regId, RegionPtr reg)
{
	unsigned int maxlevel = connection.getMetadataRegistry().getTable(reg->getQuery().getTableName()).getNQuadMaxLevel(
			reg->getQuery().getPredicate().getFields());
	NQuad quad;
	NQuad::getPredicateNQuad(reg->getQuery().getPredicate(), quad);

	NQuad quadmax = quad;
	if (quad.getLevel() > maxlevel)
		return;
	while (quadmax.getLevel() < maxlevel)
		quadmax = quadmax.getParent();

	removeRegion(quadmax, RegionDescriptor(connection.getCoopSCId(), regId, reg->getQuery()));
}

//rewrites a query using the distributed index
QueryPlan DistributedIndex::rewrite(const Query& query, const NQuadTimestamps& timestamps)
{
	QueryPlanFactory factory(connection);

	unsigned int maxlevel = connection.getMetadataRegistry().getTable(query.getTableName()).getNQuadMaxLevel(
			query.getPredicate().getFields());
	NQuad quad;
	NQuad::getPredicateNQuad(query.getPredicate(), quad);
	if (quad.getLevel() > maxlevel)
		return factory.queryPlan(factory.server(query));
	while (quad.getLevel() < maxlevel)
		quad = quad.getParent();


	int sessionid;
	RewritingSession* rewsession;
	{
		boost::mutex::scoped_lock lock(indexmutex);
		sessionid = lastsessionid++;

		rewsessions[sessionid] = RewritingSessionPtr(new RewritingSession());
		rewsession = rewsessions[sessionid].get();
	}


	rewsession->result.clear();
	rewsession->nolevelrequests.clear();
	rewsession->nolevelrequests[quad.getLevel()] = 1;
	rewsession->rewritingfinished = false;

	boost::posix_time::ptime starttime = boost::get_system_time();
	boost::posix_time::ptime timeout = starttime + boost::posix_time::millisec(REWRITING_WAIT_TIME);

	rewrite(quad, connection.getCoopSCId(), sessionid, "", query, timestamps);
	{
		boost::mutex::scoped_lock lock(rewsession->rewcondmutex);
		if (!rewsession->rewritingfinished)
		{
			rewsession->rewcond.timed_wait(lock, timeout);
		}
	}

	if (rewsession->result.find("") == rewsession->result.end())
	{
		return factory.queryPlan(factory.server(query));
	}

	RewritingChildrenIntegrator rci(connection, "", rewsession->result, quad);
	rci.optimize(rewsession->result[""]);

	QueryPlan plan = rewsession->result[""];
	{
		boost::mutex::scoped_lock lock(indexmutex);
		rewsessions.erase(sessionid);
	}

	return plan;
}

NQuadNodePtr DistributedIndex::getNQuadNode(const NQuad& nquad)
{
	boost::mutex::scoped_lock lock(indexmutex);
	NQuadNodePtr qn = nodes[nquad];
	if (qn.get() != NULL)
		return qn;
	if (levelmutexes[nquad.getLevel()].get() == NULL)
		levelmutexes[nquad.getLevel()] = boost::shared_ptr<boost::shared_mutex>(new boost::shared_mutex());
	qn = NQuadNodePtr(new NQuadNode(*this, nquad, *levelmutexes[nquad.getLevel()]));
	nodes[nquad] = qn;
	return qn;
}

bool DistributedIndex::nquadExists(const NQuad& nquad)
{
	boost::mutex::scoped_lock lock(indexmutex);
	auto it = nodes.find(nquad);
	return it != nodes.end();
}

void DistributedIndex::addRegion(const NQuad& nquad, const RegionDescriptor& reg, const NQuadTimestamps& ts)
{
	if (nquadExists(nquad))
	{
		getNQuadNode(nquad)->addRegion(reg, ts);
		return;
	}
	std::vector<char> result;
	filtering_ostream out(back_inserter(result));
	boost::archive::binary_oarchive archive(out);

	archive << nquad << reg << ts;
	out.flush();
	Key hashed;
	hashNQuad(&hashed, nquad);

	chimera_send((ChimeraState*) chimerastate, hashed, ADD_REGION, result.size(), &result[0]);
}

void DistributedIndex::removeRegion(const NQuad& nquad, const RegionDescriptor& reg)
{
	if (nquadExists(nquad))
	{
		getNQuadNode(nquad)->removeRegion(reg);
		return;
	}
	std::vector<char> result;
	filtering_ostream out(back_inserter(result));
	boost::archive::binary_oarchive archive(out);

	archive << nquad << reg;
	out.flush();
	Key hashed;
	hashNQuad(&hashed, nquad);

	chimera_send((ChimeraState*) chimerastate, hashed, REMOVE_REGION, result.size(), &result[0]);
}

void DistributedIndex::removeRegionReverse(const PeerId& dest, int regId)
{
	std::vector<char> result;
	filtering_ostream out(back_inserter(result));
	boost::archive::binary_oarchive archive(out);

	archive << regId;
	out.flush();
	Key hashed;
	hashPeerId(&hashed, dest);

	chimera_send((ChimeraState*) chimerastate, hashed, REMOVE_REGION_REVERSE, result.size(), &result[0]);
}

void DistributedIndex::rewrite(const NQuad& nquad, const PeerId& source, int reqId, const std::string& prefix, const Query& query,
		const NQuadTimestamps& ts)
{
	if (nquadExists(nquad))
	{
		getNQuadNode(nquad)->rewrite(source, reqId, prefix, query, ts);
		return;
	}
	std::vector<char> result;
	filtering_ostream out(back_inserter(result));
	boost::archive::binary_oarchive archive(out);

	archive << nquad << source << reqId << prefix << query << ts;
	out.flush();
	Key hashed;
	hashNQuad(&hashed, nquad);
	chimera_send((ChimeraState*) chimerastate, hashed, REWRITE_REQUEST, result.size(), &result[0]);
}

void DistributedIndex::rewriteResponse(const PeerId& dest, int level, int reqId, const std::string& prefix, const QueryPlan& plan,
		int childrenrequests)
{
	std::vector<char> result;
	filtering_ostream out(back_inserter(result));
	boost::archive::binary_oarchive archive(out);

	archive << dest << level << reqId << prefix << plan << childrenrequests;
	out.flush();

	Key hashed;
	hashPeerId(&hashed, dest);

	chimera_send((ChimeraState*) chimerastate, hashed, REWRITE_RESPONSE, result.size(), &result[0]);
}

void DistributedIndex::optimize(QueryPlan& plan, const NQuadTimestamps& timestamps)
{
	DistributedIndexOptimizer distributedindexoptimizer(*this, timestamps);
	distributedindexoptimizer.optimize(plan);

	ReducerOptimizer reducer(connection);
	reducer.optimize(plan);
}

NodePtr DistributedIndexOptimizer::optimizeNode(NodePtr node)
{
	//rewriting the server query using the distributed index
	ServerNode* sn = dynamic_cast<ServerNode*> (node.get());
	if (sn == NULL)
		return node;

	const Query& query = sn->getQuery();

	return distributedIndex.rewrite(query, timestamps).getRoot();
}

NodePtr RewritingChildrenIntegrator::optimizeNode(NodePtr node)
{
	if (children.size() == 0)
		return node;
	ServerNode* sn = dynamic_cast<ServerNode*> (node.get());
	if (sn == NULL) //not a SeverNode
		return node;
	Query& query = sn->getQuery();

	std::vector<NodePtr> nodes;
	NodePtr r;

	BOOST_FOREACH(NQuad& nq, children)
	{
		Predicate p = query.getPredicate() && nq.getPredicate();
		if (!p.isFalse())
		{
			std::string newprefix;
			newprefix = prefix + ":" + boost::lexical_cast<std::string>(lastchild);
			auto it = result.find(newprefix);
			if (it != result.end())
			{
				RewritingChildrenIntegrator cint(con, newprefix, result, nq);
				cint.optimize(it->second);
				nodes.push_back(it->second.getRoot());
			}
			else
				nodes.push_back(factory.server(Query(con, query.getTableName(), p, query.getFields(), query.getKey())));
			lastchild++;
		}
	}
	if (nodes.size() == 0)
	{
		LOG_ERROR("prefix : " << prefix);
		LOG_ERROR("query : " << query);
		LOG_ERROR("nquad : " << nquad);
		LOG_ERROR("level : " << nquad.getLevel());
	}
	r = nodes[0];
	for (unsigned int i = 1; i < nodes.size(); i++)
		r = factory.uni(r, nodes[i]);
	return r;
}

}
