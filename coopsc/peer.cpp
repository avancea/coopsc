#include <sstream>
#include <fstream>
#include <boost/iostreams/categories.hpp>
#include <boost/iostreams/stream.hpp>
#include <boost/iostreams/filtering_stream.hpp>
#include <boost/range/iterator_range.hpp>
#include <boost/lexical_cast.hpp>
#include "peer.h"
#include "message.h"
#include "nodes.h"
#include "connection.h"

using boost::asio::ip::tcp;
using namespace boost::iostreams;

namespace coopsc
{

std::ostream& operator <<(std::ostream& os, const PeerId& peerId)
{
	os << peerId.first << ":" << peerId.second;
	return os;
}

Peer::Peer(Connection& _connection, const PeerId& _peerId) :
	connection(_connection), peerId(_peerId), zombie(false), connected(false)
{
}

Peer::~Peer()
{
	if (connected)
		disconnect();
}

void Peer::connect()
{
	if (connected)
		throw std::runtime_error("Already connected");
	try
	{
		zombie = false;
		stream = TcpIostreamPtr(new boost::asio::ip::tcp::iostream(peerId.first, boost::lexical_cast<std::string>(peerId.second)));
		thread = ThreadPtr(new boost::thread(boost::ref(*this)));
	} catch (...)
	{
		LOG_ERROR("Error connecting");
	}
	connected = true;
}

void Peer::quit()
{
	if (!connected)
		throw std::runtime_error("Not connected");
	boost::mutex::scoped_lock lock(mutex);
	OArchivePtr oa = OArchivePtr(new boost::archive::binary_oarchive(*stream)); //out archive
	(*oa) << MSG_QUIT;
	stream->flush();
}

void Peer::disconnect()
{
	if (!connected)
		throw std::runtime_error("Not connected");
	killthread();
	thread->join();
	try
	{
		quit();
		stream->close();
	} catch (...)
	{
		//quit or close may fail (e.g : when the tcp connection is lost)
		//we don't care
	}
}

void Peer::killthread()
{
	zombie = true;
	//adding a bogus job in order to wake up the thread
	jobs.push(Job());
}

void Peer::asyncExecute(const QueryPlan& plan, Callback callback)
{
	if (zombie)
		throw std::runtime_error("Can not accept new requests! Thread is dying... RIP");
	jobs.push(Job(plan, callback));
}

//executes a query
RegionPtr Peer::execute(const QueryPlan& plan)
{
	if (!connected)
		throw std::runtime_error("Not connected");
	boost::mutex::scoped_lock lock(mutex);

	int status;
	RegionPtr reg(new Region(connection));
	std::string error;

	boost::system_time starttime = boost::get_system_time();
	try
	{
		OArchivePtr oa = OArchivePtr(new boost::archive::binary_oarchive(*stream)); //out archive
		(*oa) << MSG_EXECUTE_ID << plan;
		stream->flush();

		IArchivePtr ia = IArchivePtr(new boost::archive::binary_iarchive(*stream)); //in archive
		(*ia) >> status;
		if (status == MSG_RESPONSE_OK)
		{
			(*ia) >> (*reg);
		}
		else
			(*ia) >> error;
	} catch (...)
	{
		LOG_ERROR("Exception peer : " << peerId.first << " " << peerId.second);
		//communication error
		killthread();
		throw;
	}

	boost::system_time endtime = boost::get_system_time();
	boost::posix_time::time_duration d = endtime - starttime;
	if (d.total_milliseconds() > 5000)
	{
		LOG_ERROR("Execute from : " << peerId << " lasted : " << d.total_milliseconds());
	}
	if (status != MSG_RESPONSE_OK)
		throw std::runtime_error(error);
	return reg;
}

void Peer::operator()()
{
	Job job;
	while (true)
	{
		jobs.wait_and_pop(job);
		if (zombie)
			break;
		try
		{
			RegionPtr result = execute(job.queryplan);
			job.callback(true, result, "");
		} catch (std::exception& e)
		{
			job.callback(false, RegionPtr(), e.what());
		}
	}
	//sending error notifications to all jobs waiting to be executed
	do
	{
		if (job.callback)
			job.callback(false, RegionPtr(), "thread is dying");
	} while (jobs.try_pop(job));
}

}
