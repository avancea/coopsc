#ifndef INDEX_H_
#define INDEX_H_
#include <boost/smart_ptr.hpp>
#include <stdexcept>
#include <map>
#include "metadataregistry.h"

namespace coopsc
{

class Region;

//index base class
class IndexBase
{
private:
	Region& region;

protected:
	//the offset of the field on which the index is created
	int fieldoffset;
	virtual void add(const void* ptr, int offset) = 0;
	virtual int findOffset(const void* ptr) = 0;

public:
	IndexBase(Region& _region, int _fieldOffset) :
		region(_region), fieldoffset(_fieldOffset)
	{
	}

	virtual ~IndexBase()
	{
	}

	//returns the a pointer to the tuple
	void* find(const void* ptr);

	virtual void clear() = 0;

	void indexTuple(const void* tuple);
};

typedef boost::shared_ptr<IndexBase> IndexPtr;

template<class T>
class Index: public IndexBase
{
private:
	//maps the value to the offset
	std::map<T, int> idx;
protected:

	void add(const void* ptr, int offset)
	{
		T val = *((const T*) ptr);
		idx[val] = offset;
	}

	int findOffset(const void* ptr)
	{
		T val = *((const T*) ptr);
		typename std::map<T, int>::iterator it;
		it = idx.find(val);
		if (it == idx.end())
			return false;
		return it->second;
		return true;
	}

	virtual void clear()
	{
		idx.clear();
	}
public:
	Index(Region& _region, int _fieldOffset) :
		IndexBase(_region, _fieldOffset)
	{

	}

};

class IndexFactory
{
private:
	Region& region;
public:
	explicit IndexFactory(Region& _region) :
		region(_region)
	{
	}

	IndexPtr createIndex(int start, int size);
};

}
#endif /*INDEX_H_*/
