#include <climits>
#include <iostream>
#include <boost/lexical_cast.hpp>
#include "predicatenode.h"
#include "predicate.h"
#include "logger.h"
namespace coopsc
{

using namespace std;
using namespace boost;

PredicateNode::PredicateNode(int _field, int first, int second)
{
	if (first > second)
	{
		scalar = true;
		field = -1;
		allfieldsmask = 0;
		valuescalar = false;
	}
	else if (first == Predicate::_oo && second == Predicate::oo)
	{
		scalar = true;
		field = -1;
		allfieldsmask = 0;
		valuescalar = true;
	}
	else
	{
		scalar = false;
		valuescalar = false;
		field = _field;
		allfieldsmask = 1 << _field;
		addChild(first, PredicateNodePtr(new PredicateNode(true)));
		if (second != Predicate::oo)
			addChild(second + 1, PredicateNodePtr(new PredicateNode(false)));
	}
}

bool PredicateNode::operator ==(const PredicateNode& that) const
{
	if (this->field != that.field)
		return false;
	if (this->allfieldsmask != that.allfieldsmask)
		return false;
	if (this->scalar != that.scalar)
		return false;
	if (this->valuescalar != that.valuescalar)
		return false;
	if (this->mchildren.size() != that.mchildren.size())
		return false;

	auto ia = this->mchildren.begin();
	auto ib = that.mchildren.begin();
	for (; ia != this->mchildren.end(); ia++, ib++)
	{
		if (ia->first != ib->first)
			return false;
	}

	ia = this->mchildren.begin();
	ib = that.mchildren.begin();
	for (; ia != this->mchildren.end(); ia++, ib++)
	{
		if ((*ia->second) != (*ib->second))
			return false;
	}
	return true;
}

PredicateNodePtr simplify(const PredicateNodePtr a)
{
	if (a->isScalar())
		return a;
	if (a->children().empty())
		return PredicateNodePtr(new PredicateNode(false));
	if (a->children().size() == 1 && a->children().begin()->first == Predicate::_oo)
	{
		return a->children().begin()->second;
	}
	return a;
}

PredicateNodePtr exec(PredicateNode::Operation op, const PredicateNodePtr a, const PredicateNodePtr b)
{
	if (op == PredicateNode::Or)
	{
		if (a->isTrue() || b->isTrue())
			return PredicateNodePtr(new PredicateNode(true));
		if (a->isFalse())
			return b;
		if (b->isFalse())
			return a;
	}
	if (op == PredicateNode::And)
	{
		if (a->isFalse() || b->isFalse())
			return PredicateNodePtr(new PredicateNode(false));
		if (a->isTrue())
			return b;
		if (b->isTrue())
			return a;
	}
	PredicateNode::ChildrenMap tmp;
	const PredicateNode::ChildrenMap* children_a;
	const PredicateNode::ChildrenMap* children_b;

	long field_a, field_b;
	if (a->getField() == b->getField())
	{
		field_a = a->getField();
		children_a = &a->children();
		field_b = b->getField();
		children_b = &b->children();
	}
	else if (a->getField() > b->getField())
	{
		tmp[Predicate::_oo] = a;
		field_a = b->getField();
		children_a = &tmp;
		field_b = b->getField();
		children_b = &b->children();
	}
	else
	{
		field_a = a->getField();
		children_a = &a->children();
		tmp[Predicate::_oo] = b;
		field_b = a->getField();
		children_b = &tmp;
	}

	auto ia = children_a->begin();
	auto ib = children_b->begin();
	PredicateNodePtr lasta(new PredicateNode(false));
	PredicateNodePtr lastb(new PredicateNode(false));
	PredicateNodePtr lastr(new PredicateNode(false));
	PredicateNodePtr result(new PredicateNode((int) field_a));

	while (ia != children_a->end() || ib != children_b->end())
	{
		int v;

		if ((ia != children_a->end()) && ((ib == children_b->end()) || (ia->first < ib->first)))
		{
			v = ia->first;
			lasta = ia->second;
			ia++;
		}
		else if ((ib != children_b->end()) && ((ia == children_a->end()) || (ib->first < ia->first)))
		{
			v = ib->first;
			lastb = ib->second;
			ib++;
		}
		else //both
		{
			v = ia->first;
			lasta = ia->second;
			ia++;
			lastb = ib->second;
			ib++;
		}
		PredicateNodePtr r = exec(op, lasta, lastb);
		if (*r != *lastr)
		{
			result->addChild(v, r);
			lastr = r;
		}
	}
	return simplify(result);
}

PredicateNodePtr neg(const PredicateNodePtr a)
{
	if (a->isTrue())
		return PredicateNodePtr(new PredicateNode(false));
	if (a->isFalse())
		return PredicateNodePtr(new PredicateNode(true));

	PredicateNodePtr result(new PredicateNode((int) a->getField()));
	if (a->children().find(Predicate::_oo) == a->children().end())
	{
		result->addChild(Predicate::_oo, PredicateNodePtr(new PredicateNode(true)));
	}

	for (auto it = a->children().begin(); it != a->children().end(); it++)
	{
		int v = it->first;
		PredicateNodePtr s = neg(it->second);

		if (!(v == Predicate::_oo && s->isFalse()))
			result->addChild(v, s);
	}
	return simplify(result);
}

int noTerms(const PredicateNodePtr a)
{
	int result = 0;
	PredicateNode::ChildrenMap::const_iterator it;
	for (it = a->children().begin(); it != a->children().end(); it++)
	{
		if (!it->second->isFalse())
			result++;
	}
	return result;
}

std::string toString(const PredicateNodePtr a, const std::vector<std::string>& fieldnames)
{
	if (a->isTrue())
		return "true";
	if (a->isFalse())
		return "false";

	map<string, string> toprint;
	map<string, int> toprintcount;

	for (auto it = a->children().begin(); it != a->children().end(); it++)
	{
		if (it->second->isFalse())
			continue;
		auto jt = it;
		jt++;

		string term;
		if (it->first != Predicate::_oo)
		{
			term += string("(") + lexical_cast<string> (it->first - 1) + " < " + fieldnames[a->getField()] + ")";
		}
		if (jt != a->children().end())
		{
			if (!term.empty())
				term += " and ";
			term += string("(") + fieldnames[a->getField()] + " < " + lexical_cast<string> (jt->first) + ")";
		}

		string t;
		if (!it->second->isTrue())
		{
			t = toString(it->second, fieldnames);
			if (noTerms(it->second) > 1)
			{
				t = string("(") + t + string(")");
			}
		}

		toprintcount[t]++;
		if (toprint[t].empty())
			toprint[t] = term;
		else
			toprint[t] += string(" or ") + term;
	}
	string result = "";
	for (auto it = toprint.begin(); it != toprint.end(); it++)
	{
		if (!result.empty())
			result += " or ";
		if (toprintcount[it->first] > 1 && !it->first.empty())
			result += string("(") + it->second + string(")");
		else
			result += it->second;
		if (!it->first.empty())
			result += string(" and ") + it->first;
	}
	return result;
}

}
