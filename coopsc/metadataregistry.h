#ifndef METADATAREGISTRY_H_
#define METADATAREGISTRY_H_
#include <string>
#include <set>
#include <map>
#include <stdexcept>
#include <vector>
#include <utility>

namespace coopsc
{

typedef long long FieldsMask;
inline void set(FieldsMask& mask, int p)
{
	mask |= (1 << p);
}

inline void clear(FieldsMask& mask, int p)
{
	mask &= ~(1 << p);
}

inline bool test(FieldsMask mask, int p)
{
	return mask & (1 << p);
}

class Field
{
public:
	typedef enum
	{
		INT, STRING, DOUBLE, DATE, BIGINT
	} FieldType;
private:
	std::string name;
	FieldType type;
	unsigned int size;

public:
	Field()
	{
	}
	Field(const std::string _name, FieldType _type, unsigned int _size, unsigned int _boxsize = 10000) :
		name(_name), type(_type), size(_size)
	{
	}

	const std::string& getName() const
	{
		return name;
	}

	const FieldType& getType() const
	{
		return type;
	}

	unsigned int getSize() const
	{
		return size;
	}

};

class Table
{
private:

	//table name
	std::string name;

	//all fields
	std::vector<Field> fields;

	//all fields mask
	FieldsMask fieldsmask;

	//key fields mask
	FieldsMask keymask;

	std::set<FieldsMask> boxes;
	std::map<FieldsMask, int> minlevels;
	std::map<FieldsMask, int> maxlevels;
	std::map<FieldsMask, int> tslevels;
public:
	Table(const std::string& _name) :
		name(_name), fieldsmask(0), keymask(0)
	{
	}

	Table()
	{
	}

	Table(const Table& table) :
		name(table.name), fields(table.fields), fieldsmask(table.fieldsmask), keymask(table.keymask)
	{
	}

	const std::string& getName() const
	{
		return name;
	}

	void addFields(const Field& field);

	void addKeyField(const Field& field);

	int getFieldNo(const std::string& name) const;

	FieldsMask getFieldsMask() const
	{
		return fieldsmask;
	}

	FieldsMask getKeyMask() const
	{
		return keymask;
	}

	FieldsMask getNonKeyFieldsMask() const;

	void getFields(FieldsMask mask, std::vector<Field>& result) const;

	void getFieldNames(FieldsMask mask, std::vector<std::string>& result) const;

	unsigned int getNoFields() const
	{
		return fields.size();
	}

	int getNQuadMinLevel(FieldsMask fields) const;

	int getNQuadMaxLevel(FieldsMask fields) const;

	int getNQuadTimestampLevel(FieldsMask fields) const;

	void addIndexLevels(FieldsMask fields, int minlevel, int maxlevel, int tslevel);

	const Field& getField(unsigned int fieldNo) const;
};

//Used to keep the structure of the tables
class MetadataRegistry
{
private:
	typedef std::vector<Table> TablesType;
	std::vector<Table> tables;

	std::map<int, std::string> types;

public:

	MetadataRegistry()
	{
	}

	MetadataRegistry(const MetadataRegistry&)= delete;
	MetadataRegistry& operator =(const MetadataRegistry&)= delete;

	//adds a new table
	void addTable(const Table& table)
	{
		tables.push_back(table);
	}

	//returns the table with the specified name
	Table& getTable(const std::string& name);

	void addType(int oid, const std::string& name)
	{
		types[oid] = name;
	}

	std::string getType(int oid);

};

}
#endif /*METADATAREGISTRY_H_*/
