#ifndef STATISTICSREGISTRY_H_
#define STATISTICSREGISTRY_H_

#include <string>
#include <map>
#include <set>
#include <utility>
#include <boost/thread.hpp>
#include <boost/tuple/tuple.hpp>
#include <boost/tuple/tuple_comparison.hpp>
#include "queryplan.h"
#include "peer.h"

namespace coopsc
{
class StatisticsRegistry
{
public:
	struct QueryStat
	{
		unsigned int tuplesServer;
		unsigned int tuplesLocal;
		std::map<PeerId, unsigned int> tuplesPeers;
		unsigned int timestamps;
		unsigned int duration;
		unsigned int durationLocalRewriting;
		unsigned int durationDistributedRewriting;

		QueryStat()
		{
			tuplesServer = 0;
			tuplesLocal = 0;
			tuplesPeers.clear();
			timestamps = 0;
			duration = 0;
			durationLocalRewriting = 0;
			durationDistributedRewriting = 0;
		}

		QueryStat(const QueryStat& that) :
			tuplesServer(that.tuplesServer), tuplesLocal(that.tuplesLocal), tuplesPeers(that.tuplesPeers), timestamps(that.timestamps),
					duration(that.duration), durationLocalRewriting(that.durationLocalRewriting), durationDistributedRewriting(
							that.durationDistributedRewriting)
		{
		}

		QueryStat& operator =(const QueryStat& that)
		{
			tuplesServer = that.tuplesServer;
			tuplesLocal = that.tuplesLocal;
			tuplesPeers = that.tuplesPeers;
			timestamps = that.timestamps;
			duration = that.duration;
			durationLocalRewriting = that.durationLocalRewriting;
			durationDistributedRewriting = that.durationDistributedRewriting;
			return *this;
		}

	};
	typedef std::vector<QueryStat> QueryStats;

private:
	QueryStats stats;
	QueryStat stat;
	boost::mutex mutex;
	bool started;

public:
	StatisticsRegistry() :
		started(false)
	{
	}

	StatisticsRegistry(const StatisticsRegistry&)= delete;
	StatisticsRegistry& operator =(const StatisticsRegistry&)= delete;

	void start();
	void stop();
	void clear();

	void queryBegin();
	void queryEnd();

	void tuplesServer(unsigned int notuples);
	void tuplesPeer(const PeerId& peerId, unsigned int notuples);
	void tuplesLocal(unsigned int notuples);
	void timestamps(unsigned int ts);

	void duration(unsigned int d);
	void durationLocalRewriting(unsigned int d);
	void durationDistributedRewriting(unsigned int d);

	const QueryStats& getQueryStats() const
	{
		return stats;
	}

	QueryStat& getCurrentStat()
	{
		return stat;
	}

};
}

#endif /* STATISTICSREGISTRY_H_ */
