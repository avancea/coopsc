#include <sstream>
#include "query.h"
#include "connection.h"
#include "logger.h"

namespace coopsc
{

std::ostream& operator <<(std::ostream& os, const Query& q)
{
	const Table& table = q.connection->getMetadataRegistry().getTable(q.tablename);
	unsigned int nofields = table.getNoFields();
	os << "select ";

	bool first = true;

	FieldsMask f[2] =
	{ q.key, q.fields & (~q.key) };

	for (int k = 0; k < 2; k++)
		for (unsigned int i = 0; i < nofields; i++)
			if (test(f[k], i))
			{
				if (!first)
					os << ", ";
				first = false;
				os << table.getField(i).getName();
				clear(f[k], i);
			}

	os << " from " << table.getName() << " where " << q.predicate;
	return os;
}

std::string Query::getFieldsAsString() const
{
	const Table& table = connection->getMetadataRegistry().getTable(tablename);
	FieldsMask mask = fields;
	if (mask == table.getFieldsMask())
		return "*";
	std::string s = "";
	for (unsigned int i = 0; mask; i++)
		if (test(mask, i))
		{
			if (s != "")
				s += ", ";
			s += table.getField(i).getName();
			clear(mask, i);
		}
	return s;
}

Query::Query(Connection& _connection, const std::string& _tablename, const Predicate& _predicate, FieldsMask _fields) :
	tablename(_tablename), predicate(_predicate), fields(_fields), connection(&_connection)
{
	key = connection->getMetadataRegistry().getTable(tablename).getKeyMask();

	//key must be present in the attribute list
	if ((fields & key) != key)
		throw std::runtime_error("Primary key not selected");

	//the fields from the predicate must in present in the select clause
	if ((fields & predicate.getFields()) != predicate.getFields())
		throw std::runtime_error("The fields from the predicate must in present in the select clause");
}

Query::Query(Connection& _connection, const std::string& _tablename, const Predicate& _predicate, FieldsMask _fields, FieldsMask _key) :
	tablename(_tablename), predicate(_predicate), fields(_fields), key(_key), connection(&_connection)
{
	//key must be present in the attribute list
	if ((fields & key) != key)
		throw std::runtime_error("Primary key not selected");

	//the fields from the predicate must in present in the select clause
	if ((fields & predicate.getFields()) != predicate.getFields())
		throw std::runtime_error("The fields from the predicate must in present in the select clause");
}

bool Query::verticallyContains(const Query& q) const
{
	if (q.getTableName() != tablename)
		return false;
	return (fields & q.fields) == q.fields; // all the fields from q are contained in this->fields
}

bool Query::horizontallyContains(const Query& q) const
{
	if (q.getTableName() != tablename)
		return false;
	return q.predicate < predicate;
}

bool Query::contains(const Query& q) const
{
	return verticallyContains(q) && horizontallyContains(q);
}

bool Query::verticallyIntersects(const Query& q, FieldsMask& result) const
{
	if (q.getTableName() != tablename)
		return false;

	result = fields & q.fields; //intersection
	if (result == (key | q.getPredicate().getFields() | getPredicate().getFields())) // the intersection  only contains the primary key and
	{ //the predicate fields
		result = 0;
		return false;
	}
	//yap, they intersect
	return true;
}

bool Query::horizontallyIntersect(const Query& q, Predicate& result) const
{
	if (q.getTableName() != tablename)
		return false;
	result = predicate && q.predicate;
	return !result.isFalse();
}

bool next(std::vector<int>& box, const std::vector<int>& minb, const std::vector<int>& maxb)
{
	unsigned int p;
	for (p = 0; p < box.size(); p++)
		if (box[p] != maxb[p])
			break;
	if (p == box.size())
		return false;
	box[p]++;
	for (unsigned int i = 0; i < p; i++)
		box[i] = minb[i];
	return true;
}

void Query::getTimestampNQuads(NQuads& result) const
{
	int tslevel = connection->getMetadataRegistry().getTable(tablename).getNQuadTimestampLevel(predicate.getFields());

	getNQuads(tslevel, result);
}

//returns the boxes with intersect the query
void Query::getNQuads(unsigned int level, NQuads& result) const
{
	NQuads a, b;

	NQuad nquad;
	NQuad::getPredicateNQuad(getPredicate(), nquad);
	while (nquad.getLevel() < level)
		nquad = nquad.getParent();

	unsigned int l = nquad.getLevel();
	a.push_back(nquad);

	NQuads children;
	while (l > level)
	{
		b.clear();
		BOOST_FOREACH(NQuad& nquad, a)
		{
			nquad.getChildren(children);
			BOOST_FOREACH(NQuad& child, children)
			{
				if (!((child.getPredicate() && getPredicate()).isFalse()))
				{
					b.push_back(child);
				}
			}
		}
		l--;
		a.swap(b);
	}
	result.swap(a);
}
}
