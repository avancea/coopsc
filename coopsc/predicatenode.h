#ifndef PREDICATENODE_H_
#define PREDICATENODE_H_
#include <map>
#include <boost/smart_ptr.hpp>
#include <boost/archive/binary_oarchive.hpp>
#include <boost/archive/binary_iarchive.hpp>
#include <boost/serialization/shared_ptr.hpp>
#include <boost/serialization/map.hpp>
#include <boost/serialization/tracking.hpp>
#include "metadataregistry.h"
namespace coopsc
{

class PredicateNode;
typedef boost::shared_ptr<PredicateNode> PredicateNodePtr;

class PredicateNode
{
public:
	enum Operation
	{
		Or, And
	};

	typedef std::map<long, PredicateNodePtr> ChildrenMap;
private:

	bool scalar;
	int field;
	FieldsMask allfieldsmask;

	bool valuescalar; //only used when scalar == true

	ChildrenMap mchildren; //only used when scalar == false

	friend class boost::serialization::access;
	template<class Archive>
	void serialize(Archive & ar, const unsigned int version)
	{
		ar & scalar & field & allfieldsmask & valuescalar & mchildren;
	}
public:
	PredicateNode(bool value = false) :
		scalar(true), field(-1), allfieldsmask(0), valuescalar(value)
	{
	}

	PredicateNode(int _field) :
		scalar(false), field(_field), allfieldsmask(1 << _field), valuescalar(false)
	{
	}

	PredicateNode(int _field, int first, int second);

	void addChild(int key, PredicateNodePtr node)
	{
		mchildren[key] = node;
		allfieldsmask |= node->getAllFields();
	}

	int getField() const
	{
		return field;
	}

	bool isScalar() const
	{
		return scalar;
	}

	bool isTrue() const
	{
		return scalar && valuescalar;
	}

	bool isFalse() const
	{
		return scalar && !valuescalar;
	}

	FieldsMask getAllFields() const
	{
		return allfieldsmask;
	}

	const ChildrenMap& children() const
	{
		return mchildren;
	}

	bool operator ==(const PredicateNode& that) const;
	bool operator !=(const PredicateNode& that) const
	{
		return !(*this == that);
	}
};

PredicateNodePtr exec(PredicateNode::Operation op, const PredicateNodePtr a, const PredicateNodePtr b);
PredicateNodePtr neg(const PredicateNodePtr a);

std::string toString(const PredicateNodePtr a, const std::vector<std::string>& fieldnames);

}

#endif /* PREDICATENODE_H_ */
