#include <stdexcept>
#include <boost/format.hpp>
#include <string.h>
#include <sstream>
#include <boost/tokenizer.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/foreach.hpp>
#include "connection.h"
#include "databaseconnection.h"
#include "metadataregistry.h"
#include "logger.h"



const char* READ_POSTGRESQL_METADATA = "select table_name, column_name, data_type, character_maximum_length "
	"from information_schema.COLUMNS where table_schema = 'public' order by table_name, ordinal_position";
const char* READ_KEY = "SELECT column_name FROM information_schema.key_column_usage WHERE table_name = \'%1%\'";
const int TABLE_NAME_INDEX = 0;
const int COLUMN_NAME_INDEX = 1;
const int DATA_TYPE_INDEX = 2;
const int CHAR_MAX_SIZE_INDEX = 3;

const char* READ_COOPSC_METADATA = "select table_name, fields, minlevel, maxlevel, tslevel from coopsc.metadata";
const int COOPSC_TABLE_NAME_INDEX = 0;
const int COOPSC_FIELDS_INDEX = 1;
const int COOPSC_MINLEVEL_INDEX = 2;
const int COOPSC_MAXLEVEL_INDEX = 3;
const int COOPSC_TSLEVEL_INDEX = 4;

const char* GET_TYPES = "select oid, typname from  pg_catalog.pg_type";
const int TYPES_OID_INDEX = 0;
const int TYPES_NAME_INDEX = 1;

namespace coopsc
{

ServerResultSet::ServerResultSet(Connection& _connection, PGresult* _res) :
	connection(_connection), res(_res)
{

}

ServerResultSet::~ServerResultSet()
{
	PQclear(res);
}

unsigned int ServerResultSet::getNoTuples() const
{
	return PQntuples(res);
}

unsigned int ServerResultSet::getNoFields() const
{
	return PQnfields(res);
}

std::string ServerResultSet::getFieldName(int fieldno) const
{
	return std::string(PQfname(res, fieldno));
}

std::string ServerResultSet::getValue(unsigned int tupleno, unsigned int fieldno) const
{
	return std::string(PQgetvalue(res, tupleno, fieldno));
}

std::string ServerResultSet::getValue(unsigned int tupleno, const std::string& field) const
{
	return getValue(tupleno, PQfnumber(res, field.c_str()));
}


std::string ServerResultSet::getFieldType(int fieldno) const
{
	return connection.getMetadataRegistry().getType(PQftype(res, fieldno));
}

std::string ServerResultSet::getFieldType(const std::string& field) const
{
	return getFieldType(PQfnumber(res, field.c_str()));
}

int ServerResultSet::getFieldSize(int fieldno) const
{
	return PQfsize(res, fieldno);
}

int ServerResultSet::getFieldSize(const std::string& field) const
{
	return getFieldSize(PQfnumber(res, field.c_str()));
}



DatabaseConnection::DatabaseConnection(Connection& _connection, JobQueue& _jobs) :
	connection(_connection), psql(NULL), mustdie(false), connected(false), jobs(_jobs)
{
}

std::string DatabaseConnection::escape(const std::string& s)
{
	char buf[2 * s.size() + 1]; //NOT ANSI compliant
	int error;
	int size = PQescapeStringConn(psql, buf, s.c_str(), s.size(), &error);
	if (error)
		throw std::runtime_error("Error escaping");
	buf[size] = 0;
	return std::string(buf);
}

void DatabaseConnection::connect()
{
	if (connected)
		throw std::runtime_error("Already connected");
	connected = true;
	thread = ThreadPtr(new boost::thread(boost::ref(*this)));
	std::string conninfo;
	conninfo = (boost::format("hostaddr = \'%1%\' port = \'%2%\' dbname = \'%3%\' user = \'%4%\' "
		"password = \'%5%\' connect_timeout = \'10\' sslmode = disable") % connection.getDbHost() % connection.getDbPort()
			% connection.getDbName() % connection.getDbUsr() % connection.getDbPwd()).str();

	PQinitSSL(true);
	psql = PQconnectdb(conninfo.c_str());
	if (PQstatus(psql) != CONNECTION_OK)
	{
		throw std::runtime_error(std::string("Connection to database failed : ") + PQerrorMessage(psql));
	}
}

void DatabaseConnection::readPrimaryKey(const std::string& tablename, std::set<std::string>& key)
{
	key.clear();
	PGresult* res;
	res = PQexec(psql, (boost::format(READ_KEY) % escape(tablename)).str().c_str());
	if (PQresultStatus(res) != PGRES_TUPLES_OK)
		throw std::runtime_error(PQerrorMessage(psql));
	for (int k = 0; k < PQntuples(res); k++)
		key.insert(std::string(PQgetvalue(res, k, 0)));
	PQclear(res);
}

void DatabaseConnection::readCoopSCMetadata()
{
	if (!connected)
		throw std::runtime_error("Not connected");
	boost::lock_guard<boost::recursive_mutex> lock(dbmutex);
	PGresult* res;
	res = PQexec(psql, READ_COOPSC_METADATA);
	if (PQresultStatus(res) != PGRES_TUPLES_OK)
		throw std::runtime_error(PQerrorMessage(psql));
	for (int k = 0; k < PQntuples(res); k++)
	{
		try
		{
			std::string tablename(PQgetvalue(res, k, COOPSC_TABLE_NAME_INDEX));
			std::string fields(PQgetvalue(res, k, COOPSC_FIELDS_INDEX));
			std::string minlevel(PQgetvalue(res, k, COOPSC_MINLEVEL_INDEX));
			std::string maxlevel(PQgetvalue(res, k, COOPSC_MAXLEVEL_INDEX));
			std::string tslevel(PQgetvalue(res, k, COOPSC_TSLEVEL_INDEX));

			Table& table = connection.getMetadataRegistry().getTable(tablename);

			FieldsMask fieldsmask = 0;
			std::map<int, int> dims;

			boost::char_separator<char> sep(",");
			boost::tokenizer<boost::char_separator<char>> tokfields(fields, sep);

			for (auto it = tokfields.begin(); it != tokfields.end(); it++)
			{
				int fno = table.getFieldNo((*it));
				fieldsmask |= (1 << fno);
			}

			table.addIndexLevels(fieldsmask, boost::lexical_cast<int>(minlevel), boost::lexical_cast<int>(maxlevel), boost::lexical_cast<int>(tslevel) );
  	     }
		catch (std::exception& e)
		{
			LOG_ERROR(e.what());
		}
	}
	PQclear(res);


}

void DatabaseConnection::readPostgreSQLMetadata()
{
	if (!connected)
	throw std::runtime_error("Not connected");
	boost::lock_guard<boost::recursive_mutex> lock(dbmutex);
	PGresult* res;
	res = PQexec(psql, READ_POSTGRESQL_METADATA);
	if (PQresultStatus(res) != PGRES_TUPLES_OK)
	throw std::runtime_error(PQerrorMessage(psql));

	Table table("");
	std::set<std::string> key;
	for (int k = 0; k < PQntuples(res); k++)
	{
		std::string tablename(PQgetvalue(res, k, TABLE_NAME_INDEX));
		std::string columnname(PQgetvalue(res, k, COLUMN_NAME_INDEX));
		std::string datatype(PQgetvalue(res, k, DATA_TYPE_INDEX));
		if (table.getName() != tablename)
		{
			if (table.getName() != "")
			connection.getMetadataRegistry().addTable(table);
			table = Table(tablename);
			readPrimaryKey(tablename, key);
		}

		Field field;
		if (datatype == "integer")
		{
			field = Field(columnname, Field::INT, sizeof(int));
		}
		else if (datatype == "character varying" || datatype == "character")
		{
			field = Field(columnname, Field::STRING, boost::lexical_cast<int>(PQgetvalue(res, k, CHAR_MAX_SIZE_INDEX)) + 1);
		}
		else if (datatype == "double")
		{
			field = Field(columnname, Field::DOUBLE, sizeof(double));
		}
		else if (datatype == "date")
		{
			field = Field(columnname, Field::STRING, 11);
		}
		else if (datatype == "bigint")
		{
			field = Field(columnname, Field::BIGINT, sizeof(long long));
		}
		else LOG_ERROR("unknown type : " << datatype);

		if (key.find(std::string(columnname)) != key.end())
		table.addKeyField(field);
		else
		table.addFields(field);

	}
	connection.getMetadataRegistry().addTable(table);
	PQclear(res);

	res = PQexec(psql, GET_TYPES);
	if (PQresultStatus(res) != PGRES_TUPLES_OK)
		throw std::runtime_error(PQerrorMessage(psql));
	for (int k = 0; k < PQntuples(res); k++)
	{
		try
		{
			std::string type(PQgetvalue(res, k, TYPES_NAME_INDEX));
			std::string soid(PQgetvalue(res, k, TYPES_OID_INDEX));
			int oid = boost::lexical_cast<int>(soid);
			connection.getMetadataRegistry().addType(oid, type);
		}
		catch (std::exception& e)
		{
			LOG_ERROR(e.what());
		}
	}
	PQclear(res);

}

void DatabaseConnection::readMetadata()
{
	readPostgreSQLMetadata();
	readCoopSCMetadata();
}


RegionPtr DatabaseConnection::query(const Query& q)
{
	if (!connected)
	throw std::runtime_error("Not connected");
	boost::lock_guard<boost::recursive_mutex> lock(dbmutex);

	std::vector<int> offsets;
	std::vector<Field> fields;

	const Table& table = connection.getMetadataRegistry().getTable(q.getTableName());

	PGresult* res;
	res = PQexec(psql, "BEGIN;");
	if (PQresultStatus(res) != PGRES_COMMAND_OK) throw std::runtime_error(PQerrorMessage(psql));
	res = PQexec(psql, "SET TRANSACTION ISOLATION LEVEL SERIALIZABLE;");
	if (PQresultStatus(res) != PGRES_COMMAND_OK) throw std::runtime_error(PQerrorMessage(psql));

	NQuads nquads;
	NQuadTimestamps timestamps;
	q.getTimestampNQuads(nquads);
	getNQuadsTimestamps(nquads, timestamps);

	//inits the region
	RegionPtr result(new Region(this->connection, q, timestamps));

	//key attributes	 non key attributes
	FieldsMask f[2] =
	{	q.getKey(), q.getFields() & (~q.getKey())};

	for (unsigned int k = 0; k < 2; k++)
	{
		for (unsigned int i = 0; f[k]; i++)
		if (test(f[k], i)) //bit "i" is set

		{
			fields.push_back(table.getField(i));
			offsets.push_back(result->getOffsetSize(i).first);
			clear(f[k], i); //removing but "i"
		}
	}

	std::stringstream ss;
	ss << q;
	res = PQexec(psql, ss.str().c_str());

	if (PQresultStatus(res) != PGRES_TUPLES_OK)
	{
		throw std::runtime_error(PQerrorMessage(psql));
	}

	int nFields = PQnfields(res);
	for (int k = 0; k < PQntuples(res); k++)
	{
		char *b = (char*) result->insert();
		for (int i = 0; i < nFields; i++)
		serialize(fields[i], PQgetvalue(res, k, i), b + offsets[i]);
		result->index(b);
	}
	PQclear(res);
	res = PQexec(psql, "COMMIT;");
	if (PQresultStatus(res) != PGRES_COMMAND_OK) throw std::runtime_error(PQerrorMessage(psql));
	PQclear(res);
	return result;
}


ResultSetPtr DatabaseConnection::query(const std::string& q)
{
	if (!connected)
	throw std::runtime_error("Not connected");
	boost::lock_guard<boost::recursive_mutex> lock(dbmutex);
	if (psql == NULL)
	throw std::runtime_error("Not connected");
	PGresult* res;
	res = PQexec(psql, q.c_str());
	if (PQresultStatus(res) != PGRES_TUPLES_OK)
	{
		throw std::runtime_error(PQerrorMessage(psql));
	}
	return ResultSetPtr(new ServerResultSet(connection, res));
}


void DatabaseConnection::execute(const std::string& s)
{
	if (!connected)
	throw std::runtime_error("Not connected");
	boost::lock_guard<boost::recursive_mutex> lock(dbmutex);
	if (psql == NULL)
	throw std::runtime_error("Not connected");
	PGresult* res;
	res = PQexec(psql, s.c_str());
	if (PQresultStatus(res) != PGRES_COMMAND_OK)
	{
		throw std::runtime_error(PQerrorMessage(psql));
	}
	PQclear(res);
}

void DatabaseConnection::getNQuadsTimestamps(const NQuads& nquads, NQuadTimestamps& result)
{
	if (nquads.empty()) return;
	connection.getStatisticsRegistry().timestamps(nquads.size());

	std::string tablename = nquads[0].getTableName();
	FieldsMask fields = nquads[0].getFields();
	const Table& table = connection.getMetadataRegistry().getTable(tablename);
	int level = table.getNQuadTimestampLevel(fields);

	std::set<std::string> sfields;
	for (unsigned int i = 0; i < table.getNoFields(); i++)
	if (fields & (1 << i))
	{
		sfields.insert(table.getField(i).getName());
	}
	std::string timestamptable = "coopsc." + tablename;
	BOOST_FOREACH(std::string name, sfields)
	{
		timestamptable += "_" + name;
	}
	timestamptable += "_timestamps";

	std::string sql = "select ";
	bool first = true;
	for (unsigned int i = 0; i < table.getNoFields(); i++)
	if (fields & (1 << i))
	{
		if (!first) sql += ",";
		sql += "nquad_" + table.getField(i).getName();
		first = false;
	}
	sql += ",time_stamp from " + timestamptable + " where ";

	bool firstnquad = true;
	BOOST_FOREACH(const NQuad& nquad, nquads)
	{
		if (nquad.getTableName() != tablename || nquad.getFields() != fields)
		throw std::runtime_error("NQuads not from the same table/fields");
		if (!firstnquad) sql += "or "; firstnquad = false;

		bool firstfield = true; int k = 0;
		for (unsigned int i = 0; i < table.getNoFields(); i++)
		if (fields & (1 << i))
		{
			if (!firstfield) sql += "and "; firstfield = false;
			sql += std::string("(") + "nquad_" + table.getField(i).getName() + " = " + boost::lexical_cast<std::string>(nquad.getCoords()[k] >> level) + ") ";
			k++;
		}
	}

	result.clear();
	BOOST_FOREACH(const NQuad& nquad, nquads)
	{
		result[nquad] = 1;
	}

	if (!connected)
	throw std::runtime_error("Not connected");
	boost::lock_guard<boost::recursive_mutex> lock(dbmutex);
	if (psql == NULL)
	throw std::runtime_error("Not connected");


	PGresult* res;
	res = PQexec(psql, sql.c_str());
	if (PQresultStatus(res) != PGRES_TUPLES_OK)
	{
		throw std::runtime_error(PQerrorMessage(psql));
	}

	int nFields = PQnfields(res);

	for (int k = 0; k < PQntuples(res); k++)
	{
		std::vector<int> coords;
		for (int i = 0; i < nFields - 1; i++)
		coords.push_back(boost::lexical_cast<int>(PQgetvalue(res, k, i)) << level);
		int timestamp = boost::lexical_cast<int>(PQgetvalue(res, k, nFields - 1));
		result[NQuad(connection, tablename, fields, level, coords)] = timestamp;
	}
	PQclear(res);
}

void DatabaseConnection::serialize(const Field& field, const char* value, char *dest)
{
	switch (field.getType())
	{
		case Field::INT:
			*((int*) dest) = boost::lexical_cast<int>(value);
		break;
		case Field::DOUBLE:
			*((double*) dest) = boost::lexical_cast<double>(value);
		break;
		case Field::BIGINT:
			*((long long*) dest) = boost::lexical_cast<long long>(value);
		break;
		case Field::STRING:
			memset(dest, 0, field.getSize());
			unsigned int l = strlen(value) + 1;
			if (l > field.getSize())
			l = field.getSize();
			memcpy(dest, value, l);
		break;

	}
}

void DatabaseConnection::operator()()
{
	Job job;
	while (true)
	{
		jobs.wait_and_pop(job);
		if (mustdie)
		break;
		try
		{
			RegionPtr result = query(*job.query);
			job.callback(true, result, "");
		}
		catch (std::exception& e)
		{
			LOG_ERROR("Error :" << e.what());
			job.callback(false, RegionPtr(), e.what());
		}
	}
	//sending error notifications to all jobs waiting to be executed
	do
	{
		if (job.callback)
		job.callback(false, RegionPtr(), "thread is dying");
	}while (jobs.try_pop(job));

}

void DatabaseConnection::disconnect()
{
	if (!connected)
	throw std::runtime_error("Not connected");
	//stopping the tread
	mustdie = true;
	//adding a bogus job in order to wake up the thread
	jobs.push(Job());
	thread->join();
	if (psql == NULL)
	return;
	PQfinish(psql);
	psql = NULL;
	connected = false;
}


}
