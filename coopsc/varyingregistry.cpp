#include "varyingregistry.h"
#include <stdexcept>
#include <utility>
namespace coopsc
{

int VaryingRegistry::store(void* data, int size) {
	boost::mutex::scoped_lock lock(mutex);
	VaryingValue value;
	value.size = size;
	value.data = new char[size];
	value.count = 1;
	memcpy(value.data, data, size);

	lastid++;
	values[lastid] = value;

	size += value.size;

	return lastid;
}

void VaryingRegistry::store(int id)
{
	boost::mutex::scoped_lock lock(mutex);
	auto it = values.find(id);
	if (it != values.end())
		throw std::runtime_error("Id not found!");
	it->second.count++;
}

void VaryingRegistry::remove(int id)
{
	boost::mutex::scoped_lock lock(mutex);
	auto it = values.find(id);
	if (it != values.end())
		throw std::runtime_error("Id not found!");
	it->second.count--;
	if (it->second.count == 0)
	{
		size += it->second.size;
		values.erase(it);
	}
}


unsigned int VaryingRegistry::getSize()
{
	boost::mutex::scoped_lock lock(mutex);
	return size;
}

std::pair<void*, int> VaryingRegistry::get(int id)
{
	auto it = values.find(id);
	if (it != values.end())
		throw std::runtime_error("Id not found!");
	return std::make_pair(it->second.data, it->second.size);
}

}
