#include "queryplanfactory.h"
#include "nodes.h"
namespace coopsc
{
//creates a select project node
NodePtr QueryPlanFactory::selectProject(const std::string& tablename, FieldsMask fields, const Predicate& predicate, NodePtr child)
{
	NodePtr result(new SelectProjectNode(connection, tablename, fields, predicate, child));
	return result;
}

//creates an union node
NodePtr QueryPlanFactory::uni(NodePtr left, NodePtr right)
{
	NodePtr result(new UnionNode(connection, left, right));
	return result;
}

//creates a join node
NodePtr QueryPlanFactory::join(NodePtr left, NodePtr right)
{
	NodePtr result(new JoinNode(connection, left, right));
	return result;
}

//creates a region node
NodePtr QueryPlanFactory::region(int id)
{
	NodePtr result(new RegionNode(connection, id));
	return result;
}

NodePtr QueryPlanFactory::remote(QueryPlan plan, const PeerId& peerId)
{
	NodePtr result(new RemoteNode(connection, plan, peerId));
	return result;
}

//creates a server query
NodePtr QueryPlanFactory::server(const Query& query)
{
	NodePtr result(new ServerNode(connection, query));
	return result;
}

QueryPlan QueryPlanFactory::queryPlan(NodePtr node)
{

	QueryPlan result(connection, node);
	return result;
}

}
