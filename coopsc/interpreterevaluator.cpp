#include "interpreterevaluator.h"
#include "predicate.h"
#include "logger.h"
namespace coopsc
{

InterpreterEvaluator::InterpreterEvaluator(const Predicate& pred, const std::vector<int>& offsets) :
	Evaluator(pred, offsets)
{
	FieldsMask mask = pred.getFields();
	int k = 0;
	for (unsigned int i = 0; mask; i++)
		if (test(mask, i))
		{
			if (i >= allfieldsoffsets.size())
			{
				allfieldsoffsets.resize(i + 1, -1);
			}
			allfieldsoffsets[i] = offsets[k];
			k++;
			clear(mask, i);
		}
}

bool InterpreterEvaluator::evaluate(void *tuple)
{
	return evaluate(getPredicate().root, tuple);
}

bool InterpreterEvaluator::evaluate(const PredicateNodePtr node, void* data)
{
	if (node.get() == NULL)
		return false;
	if (node->isTrue())
		return true;
	if (node->isFalse())
		return false;
	int field = node->getField();
	if ((unsigned int) field >= allfieldsoffsets.size() || allfieldsoffsets[field] < 0)
		throw std::runtime_error("Invalid offset");
	int val = *((int*) ((char*) data + allfieldsoffsets[field]));
	PredicateNodePtr last;
	for (auto it = node->children().begin(); it != node->children().end(); it++)
	{
		if (val < it->first)
		{
			if (last.get() == NULL)
				return false;
			return evaluate(last, data);
		}
		last = it->second;
	}
	return evaluate(last, data);
}

}
