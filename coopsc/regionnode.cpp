
#include "regionnode.h"
#include "connection.h"
#include "logger.h"

namespace coopsc
{

RegionPtr RegionNode::executeNode(const std::vector<RegionPtr>& childrenresults, bool keepstatistics)
{
	RegionPtr reg = getConnection().getStorageManager().getRegion(id);
	getConnection().getStorageManager().useRegion(id);

	if (keepstatistics)
	{
		getConnection().getStatisticsRegistry().tuplesLocal(reg->getNoTuples());
	}
	return reg;
}

void RegionNode::writeTo(std::ostream& s) const
{
	s << "region(" << id << ")";
}

}
