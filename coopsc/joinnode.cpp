#include <stdexcept>
#include "joinnode.h"
namespace coopsc
{

RegionPtr JoinNode::executeNode(const std::vector<RegionPtr>& childrenresults, bool keepstatistics)
{
	return childrenresults[0]->join(childrenresults[1]);
}

void JoinNode::writeTo(std::ostream& s) const
{
	s << "JoinNode(";
	first()->writeTo(s);
	s << ",";
	second()->writeTo(s);
	s << ")";
}
}
