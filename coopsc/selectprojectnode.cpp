#include "selectprojectnode.h"
#include "connection.h"
namespace coopsc
{

RegionPtr SelectProjectNode::executeNode(const std::vector<RegionPtr>& childrenresults, bool keepstatistics)
{
	RegionPtr result = childrenresults[0]->selectproject(fields, predicate);

	if (keepstatistics)
	{
		getConnection().getStatisticsRegistry().tuplesLocal(-childrenresults[0]->getNoTuples());
		getConnection().getStatisticsRegistry().tuplesLocal(result->getNoTuples());
	}
	return result;
}

std::string SelectProjectNode::getFieldsAsString() const
{
	const Table& table = getConnection().getMetadataRegistry().getTable(tablename);
	FieldsMask mask = fields;
	if (mask == table.getFieldsMask())
		return "*";
	std::string s = "";
	for (unsigned int i = 0; mask; i++)
		if (test(mask, i))
		{
			if (s != "")
				s += ", ";
			s += table.getField(i).getName();
			clear(mask, i);
		}
	return s;
}

void SelectProjectNode::writeTo(std::ostream& s) const
{
	const Table& table = getConnection().getMetadataRegistry().getTable(tablename);
	FieldsMask mask = fields;
	s << "SelectProjectNode(";
	bool first = true;
	for (unsigned int i = 0; mask; i++)
		if (test(mask, i))
		{
			if (!first)
				s << " ";
			s << table.getField(i).getName();
			first = false;

			clear(mask, i);
		}
	s << ",";
	s << predicate;
	s << ",";
	(*getChildren().begin())->writeTo(s);
	s << ")";
}

}
