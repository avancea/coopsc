
#include <boost/archive/binary_oarchive.hpp>
#include <boost/archive/binary_iarchive.hpp>
#include <boost/iostreams/categories.hpp>
#include <boost/iostreams/stream.hpp>
#include <boost/iostreams/filtering_stream.hpp>
#include <boost/range/iterator_range.hpp>
#include "peerserver.h"
#include "connection.h"
#include "message.h"
#include "logger.h"
#include "nodes.h"

typedef boost::shared_ptr<boost::archive::binary_oarchive> OArchivePtr;
typedef boost::shared_ptr<boost::archive::binary_iarchive> IArchivePtr;

using boost::asio::ip::tcp;
using namespace boost::iostreams;

namespace coopsc
{

void PeerServer::operator()()
{
	OArchivePtr oa;
	IArchivePtr ia;
	int status = MSG_RESPONSE_OK;
	try
	{
		for (;;)
		{
			ia = IArchivePtr(new boost::archive::binary_iarchive(*stream));
			int command;
			(*ia) >> command;
			LOG_INFO("command: " << command);

			switch (command)
			{
			case MSG_QUIT:
				return;

			case MSG_EXECUTE_ID:
			{
				boost::system_time starttime = boost::get_system_time();

				QueryPlan plan;
				(*ia) >> plan;
				plan.setConnection(connection);
				oa = OArchivePtr(new boost::archive::binary_oarchive(*stream));
				try
				{
					RegionPtr reg = plan.execute(NQuadTimestamps(), false);
					status = MSG_RESPONSE_OK;
					(*oa) << status;
					stream->flush();
					(*oa) << (*reg);
					stream->flush();

				} catch (std::exception& e)
				{
					status = MSG_RESPONSE_ERROR;
					(*oa) << status;
					std::string error = e.what();
					(*oa) << error;
					stream->flush();
				}

				boost::system_time endtime = boost::get_system_time();
				boost::posix_time::time_duration d = endtime - starttime;
				if (d.total_milliseconds() > 5000)
				{
					std::cout << "Send lasted : " << d.total_milliseconds() << std::endl;
					LOG_ERROR("Send lasted : " << d.total_milliseconds());
				}

			}
				break;
			}
		}
	} catch (...)
	{
		LOG_ERROR("Communication error! Thread stopped!");
	}
}

}
