#ifndef INTERPRETEREVALUATOR_H_
#define INTERPRETEREVALUATOR_H_

#include <vector>
#include "evaluator.h"
#include "predicate.h"
namespace coopsc
{
//a simple interpreted evaluator
class InterpreterEvaluator: public Evaluator
{
private:
	std::vector<int> allfieldsoffsets;
	bool evaluate(PredicateNodePtr node, void* data);
public:
	InterpreterEvaluator(const Predicate& pred, const std::vector<int>& offsets);
	virtual bool evaluate(void *tuple);
};

}

#endif /* INTERPRETEREVALUATOR_H_ */
