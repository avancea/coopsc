#ifndef _NQUAD_H
#define _NQUAD_H
#include <vector>
#include <boost/smart_ptr.hpp>
#include <boost/serialization/tracking.hpp>
#include <boost/serialization/vector.hpp>
#include <ostream>
#include "predicate.h"
#include "metadataregistry.h"
#include "predicate.h"

namespace coopsc
{
class NQuad;
typedef std::vector<NQuad> NQuads;

class Connection;
class NQuad
{
	friend class boost::serialization::access;
private:
	Connection* connection;
	std::string tablename;
	FieldsMask fields;
	std::vector<int> coords;
	unsigned int l; //level

	template<class Archive>
	void serialize(Archive & ar, const unsigned int version)
	{
		ar & tablename;
		ar & fields;
		ar & coords;
		ar & l;
	}

public:
	NQuad()
	{
		connection = NULL;
		tablename = "";
		fields = 0;
		l = 0;
	}

	NQuad(const NQuad& that);

	//returns the quad with the minimum level that contains the specified predicate
	static void getPredicateNQuad(const Predicate& pred, NQuad& result);

	NQuad(Connection& _connection, const std::string& _tablename, FieldsMask _fields, unsigned int _l, const std::vector<int>& _coords);

	void setConnection(Connection& _connection)
	{
		connection = &_connection;
	}

	unsigned int getDimensions() const
	{
		return coords.size();
	}

	unsigned int getLevel() const
	{
		return l;
	}

	const std::vector<int>& getCoords() const
	{
		return coords;
	}

	NQuad getParent() const;
	void getChildren(NQuads& children) const;

	const std::string& getTableName() const
	{
		return tablename;
	}

	FieldsMask getFields() const
	{
		return fields;
	}

	//returns the predicate associate with the box
	Predicate getPredicate() const;

	bool includes(const NQuad& that) const;

};

bool operator <(const NQuad& q1, const NQuad& q2);
std::ostream& operator <<(std::ostream& os, const NQuad& q);
bool operator ==(const NQuad& q1, const NQuad& q2);

//associates boxes with timestamps
typedef std::map<NQuad, int> NQuadTimestamps;
}

#endif
