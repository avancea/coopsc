#ifndef COOPSCCONNECTION_H_
#define COOPSCCONNECTION_H_


#include <string>
#include <libpq-fe.h>
#include "storagemanager.h"
#include "query.h"
#include "metadataregistry.h"
#include "query.h"
#include "peerpool.h"
#include "listener.h"
#include "resultset.h"
#include "databasepool.h"
#include "distributedindex.h"
#include "statisticsregistry.h"
#include "peer.h"

namespace coopsc
{
typedef enum
{
	NoCache = 0, LocalCache, CooperativeCache
} CacheType;

//A connection (session) to a specific database and to a specific P2P overlay
class Connection
{
private:
	// database name, host, port, user, password
	std::string dbname;
	std::string dbhost;
	int dbport;
	std::string dbusr;
	std::string dbpwd;

	PeerId coopscid;

	//cache level : 0 - no cache; 1 - local cache; 2 - cooperative cache
	CacheType cachetype;

	//the size of the cache, in bytes
	unsigned int cachesize;

	bool connected;

	//The database connection
	DatabasePool dbpool;

	//the peer pool;
	PeerPool peerpool;

	//the server
	Listener listener;

	MetadataRegistry metadataregistry;

	//the port on which chimera is listening
	int chimeraport;

	//the host on which chimera is listening
	std::string chimerahost;

	//chimera bootstrap host
	std::string chimerabshost;

	//chimera bootstrap port
	int chimerabsport;

	DistributedIndex distributedIndex;

	//Storage manager
	StorageManager storagemanager;


	//true if coopsc caches data received from other peers
	bool cachepeerdata;

	//the query plan of the last executed query
	QueryPlanPtr lastqueryplan;
	boost::mutex lastquerymutex;

	StatisticsRegistry statisticsregistry;

	void query(const Query& q, std::vector<RegionPtr>& result);

	RegionPtr queryNQuad(const Query& q);

public:
	Connection();

	//constructor
	Connection(const std::string& _dbname, const std::string& _dbhost,
			int _dbport, //database name, host, port
			const std::string& _dbusr,
			const std::string& _dbpwd, //user, password
			const std::string& _coopschost,
			int _coopscport, //coopsc host, port
			CacheType _cachetype = CooperativeCache, unsigned int _cachesize = 128 * 1024 * 1024, const std::string& _chimerahost = "",
			int _chimeraport = 9999, const std::string& _chimerabshost = "", int _chimerabsport = -1);

	//disable copy constructor
	Connection(const Connection&)= delete;

	//disable assignment operator
	Connection& operator =(const Connection&)= delete;

	//connects to the database server, starts the listener
	void connect();
	void disconnect();

	//executes the specified query on the server(without using the cache)
	ResultSetPtr queryServer(const std::string& q);

	//executes a select query
	ResultSetPtr query(const std::string& q);

	//executes an insert/update statement
	void execute(const std::string& s);

	//returns the query plan of the last executed query
	QueryPlanPtr getLastQueryPlan()
	{
		return lastqueryplan;
	}

	CacheType getCacheType()
	{
		return cachetype;
	}

	//returns the database name
	const std::string& getDbName() const
	{
		return dbname;
	}

	//returns the database host
	const std::string& getDbHost() const
	{
		return dbhost;
	}

	//returns the database port
	int getDbPort() const
	{
		return dbport;
	}

	//returns the database user name
	const std::string& getDbUsr() const
	{
		return dbusr;
	}

	//returns the database password
	const std::string& getDbPwd() const
	{
		return dbpwd;
	}

	// a peer identity of the session (host, port)
	const PeerId& getCoopSCId() const
	{
		return coopscid;
	}

	// returns the type of the cache
	CacheType getCacheType() const
	{
		return cachetype;
	}

	// returns the size of the cache (in bytes)
	unsigned int getCacheSize() const
	{
		return cachesize;
	}

	DatabasePool& getDatabasePool()
	{
		return dbpool;
	}

	//Cache registry
	StorageManager& getStorageManager()
	{
		return storagemanager;
	}

	//returns the peer pool
	PeerPool& getPeerPool()
	{
		return peerpool;
	}

	MetadataRegistry& getMetadataRegistry()
	{
		return metadataregistry;
	}

	StatisticsRegistry& getStatisticsRegistry()
	{
		return statisticsregistry;
	}

	//chimera host
	const std::string& getChimeraHost()
	{
		return chimerahost;
	}

	int getChimeraPort()
	{
		return chimeraport;
	}

	//chimera bootstrap host
	const std::string& getChimeraBootstrapHost()
	{
		return chimerabshost;
	}

	//chimera bootstrap port
	int getChimeraBootstrapPort()
	{
		return chimerabsport;
	}

	DistributedIndex& getDistributedIndex()
	{
		return distributedIndex;
	}

	bool getCachePeerData() const
	{
		return cachepeerdata;
	}

	void setCachePeerData(bool value)
	{
		cachepeerdata = value;
	}

	//destructor
	virtual ~Connection();
};

}
#endif /*COOPSCCONNECTION_H_*/

