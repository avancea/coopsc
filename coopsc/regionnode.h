#ifndef REGIONNODE_H_
#define REGIONNODE_H_
#include "node.h"
namespace coopsc
{

//region leaf
class RegionNode: public Node
{
private:
	friend class boost::serialization::access;
	template<class Archive>
	void serialize(Archive & ar, const unsigned int version)
	{
		ar & boost::serialization::base_object<Node>(*this);
		ar & id;
	}
	//the name of the region
	int id;
protected:
	virtual RegionPtr executeNode(const std::vector<RegionPtr>& childrenresults, bool keepstatistics);
public:
	RegionNode()
	{
	}

	RegionNode(Connection& _connection, int _id) :
		Node(_connection), id(_id)
	{
	}

	int getId() const
	{
		return id;
	}
	virtual void writeTo(std::ostream& s) const;
};

}

#endif
