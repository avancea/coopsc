#ifndef NODE_H_
#define NODE_H_
#include <boost/smart_ptr.hpp>
#include <boost/archive/binary_oarchive.hpp>
#include <boost/archive/binary_iarchive.hpp>
#include <boost/serialization/shared_ptr.hpp>
#include <boost/serialization/vector.hpp>
#include <boost/serialization/tracking.hpp>
#include <vector>
#include <sstream>
#include "region.h"
#include "nquad.h"
namespace coopsc
{
class Connection;

class Node;
typedef boost::shared_ptr<Node> NodePtr;

//generic query node
class Node
{
public:
	typedef std::vector<NodePtr> ChildrenType;
private:
	//the children
	ChildrenType children;
	Connection* connection;

	friend class boost::serialization::access;
	template<class Archive>
	void serialize(Archive & ar, const unsigned int version)
	{
		ar & children;
	}
protected:
	virtual RegionPtr executeNode(const std::vector<RegionPtr>& childrenresults, bool keepstatistics) = 0;
	virtual void preExecuteNode()
	{
	}
public:
	Node() :
		connection(NULL)
	{
	}

	Node(Connection& _connection) :
		connection(&_connection)
	{

	}

	//creates a node with a single child
	Node(Connection& _connection, NodePtr child) :
		connection(&_connection)
	{
		children.push_back(child);
	}

	//creates a nodes with two nodes
	Node(Connection& _connection, NodePtr leftchild, NodePtr rightchild) :
		connection(&_connection)
	{
		children.push_back(leftchild);
		children.push_back(rightchild);
	}

	virtual void setConnection(Connection& _connection);

	Connection& getConnection() const
	{
		return *connection;
	}

	virtual ~Node()
	{
	}

	ChildrenType& getChildren()
	{
		return children;
	}

	NodePtr first()
	{
		return *(children.begin());
	}

	NodePtr second()
	{
		ChildrenType::iterator it = children.begin();
		it++;
		return *it;;
	}

	const NodePtr first() const
	{
		return *(children.begin());
	}

	const NodePtr second() const
	{
		ChildrenType::const_iterator it = children.begin();
		it++;
		return *it;;
	}

	const ChildrenType& getChildren() const
	{
		return children;
	}

	void preExecute();

	//executes the query and returns the result
	RegionPtr execute(const NQuadTimestamps &ts, bool keepstatistics = true);

	virtual void writeTo(std::ostream& s) const = 0;
};

std::ostream& operator <<(std::ostream& s, const Node& node);

}

#endif /* NODE_H_ */
