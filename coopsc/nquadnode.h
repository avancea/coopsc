#ifndef _NQUAD_NODE_H
#define _NQUAD_NODE_H
#include <utility>
#include <boost/tuple/tuple.hpp>
#include <boost/thread.hpp>
#include <boost/tuple/tuple_comparison.hpp>
#include <boost/multi_index_container.hpp>
#include <boost/multi_index/ordered_index.hpp>
#include <boost/multi_index/key_extractors.hpp>
#include <cstdlib>
#include <ctime>
#include <map>
#include "nquad.h"
#include "queryoptimizer.h"
#include "query.h"
#include "regiondescriptor.h"

namespace coopsc
{
class DistributedIndex;
class NQuadChildrenRewriter;

class NQuadNode
{
	friend class NQuadChildrenRewriter;
private:
	typedef std::set<RegionDescriptor> RegionsSet;
	DistributedIndex &dindex;
	NQuad nquad;
	boost::shared_mutex& mutex;

	RegionsSet regions;
	NQuadTimestamps timestamps;

	std::vector<NQuad> children;
	std::map<NQuad, unsigned int> childrensize;

	//deletes all entries that intersect the specified nquad
	void clean(const NQuad& nq);
public:
	NQuadNode(DistributedIndex &_dindex, const NQuad& _nquad, boost::shared_mutex& _mutex);
	void addRegion(const RegionDescriptor& reg, const NQuadTimestamps& ts);
	void removeRegion(const RegionDescriptor& reg);
	void rewrite(const PeerId& source, int reqId, //request ID
			const std::string& prefix, const Query& query, const NQuadTimestamps& ts);
};

typedef boost::shared_ptr<NQuadNode> NQuadNodePtr;
}

#endif

