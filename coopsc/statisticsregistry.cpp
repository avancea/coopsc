#include "statisticsregistry.h"
#include "nodes.h"
namespace coopsc
{

void StatisticsRegistry::start()
{
	boost::lock_guard<boost::mutex> lock(mutex);
	started = true;
}

void StatisticsRegistry::stop()
{
	boost::lock_guard<boost::mutex> lock(mutex);
	started = false;
}

void StatisticsRegistry::clear()
{
	boost::lock_guard<boost::mutex> lock(mutex);
	stats.clear();
	stat = QueryStat();
}

void StatisticsRegistry::tuplesServer(unsigned int notuples)
{
	boost::lock_guard<boost::mutex> lock(mutex);
	if (!started)
		return;
	stat.tuplesServer += notuples;
}

void StatisticsRegistry::tuplesPeer(const PeerId& peerId, unsigned int notuples)
{
	boost::lock_guard<boost::mutex> lock(mutex);
	if (!started)
		return;
	stat.tuplesPeers[peerId] += notuples;
}

void StatisticsRegistry::tuplesLocal(unsigned int notuples)
{
	boost::lock_guard<boost::mutex> lock(mutex);
	if (!started)
		return;
	stat.tuplesLocal += notuples;
}

void StatisticsRegistry::timestamps(unsigned int ts)
{
	boost::lock_guard<boost::mutex> lock(mutex);
	if (!started)
		return;
	stat.timestamps += ts;
}

void StatisticsRegistry::duration(unsigned int d)
{
	boost::lock_guard<boost::mutex> lock(mutex);
	if (!started)
		return;
	stat.duration = d;
}

void StatisticsRegistry::durationLocalRewriting(unsigned int d)
{
	boost::lock_guard<boost::mutex> lock(mutex);
	if (!started)
		return;
	stat.durationLocalRewriting = d;
}

void StatisticsRegistry::durationDistributedRewriting(unsigned int d)
{
	boost::lock_guard<boost::mutex> lock(mutex);
	if (!started)
		return;
	stat.durationDistributedRewriting = d;
}

void StatisticsRegistry::queryBegin()
{
	stat = QueryStat();
}

void StatisticsRegistry::queryEnd()
{
	stats.push_back(stat);
}

}
