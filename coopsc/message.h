#ifndef GLOBAL_H_
#define GLOBAL_H_
namespace coopsc
{
const int MSG_EXECUTE_ID = 200;
const int MSG_QUIT = 300;
const int MSG_RESPONSE_OK = 400;
const int MSG_RESPONSE_ERROR = 500;
}
#endif
