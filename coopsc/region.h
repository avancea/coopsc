#ifndef REGION_H_
#define REGION_H_

#include <set>
#include <map>
#include <string>
#include <utility>
#include <sys/time.h>
#include <sstream>
#include <boost/smart_ptr.hpp>
#include <boost/shared_array.hpp>
#include <boost/archive/binary_oarchive.hpp>
#include <boost/archive/binary_iarchive.hpp>
#include <boost/serialization/tracking.hpp>
#include <boost/serialization/map.hpp>
#include <boost/serialization/binary_object.hpp>
#include <boost/foreach.hpp>
#include "logger.h"
#include "predicate.h"
#include "query.h"
#include "index.h"
#include "query.h"
#include "resultset.h"
#include "nquad.h"

namespace coopsc
{

//CoopSC connection forward declaration
class Connection;

//region forward declaration
class Region;
typedef boost::shared_ptr<Region> RegionPtr;

//Semantic region
class Region
{
	friend class boost::serialization::access;
	friend std::ostream& operator <<(std::ostream& os, const Region& s);
private:

	//reference to the cache manager
	Connection& connection;

	//stores the offset and size for each field
	typedef std::map<unsigned int, std::pair<unsigned int, unsigned int>> OffsetSizeType;
	OffsetSizeType offsetsize;

	//the size of a tuple, in bytes
	unsigned int tuplesize;

	// the size of the key, in bytes
	unsigned int keysize;

	//number of tuples stored
	unsigned int noTuples;

	Query query;

	IndexFactory indexfactory;

	//primary key index
	IndexPtr pkeyindex;

	//the region content
	void* tupledata;
	//the capacity of the buffer, in number of tuples
	unsigned int capacity;


	NQuadTimestamps timestamps;

	template<class Archive>
	void save(Archive & ar, const unsigned int version) const
	{
		ar & offsetsize;
		ar & tuplesize;
		ar & keysize;
		ar & noTuples;
		ar & query;

		unsigned int l = timestamps.size();
		ar & l;
		for (std::map<NQuad, int>::const_iterator it = timestamps.begin(); it != timestamps.end(); it++)
		{
			ar & it->first & it->second;
		}
		ar.save_binary(tupledata, noTuples * tuplesize);
	}

	template<class Archive>
	void load(Archive & ar, const unsigned int version)
	{
		ar & offsetsize;
		ar & tuplesize;
		ar & keysize;
		ar & noTuples;
		ar & query;

		unsigned int l;
		ar & l;
		for (unsigned int i = 0; i < l; i++)
		{
			NQuad nquad;
			int timestamp;
			ar & nquad & timestamp;
			nquad.setConnection(connection);
			timestamps[nquad] = timestamp;
		}

		capacity = noTuples;
		tupledata = new char[noTuples * tuplesize];

		ar.load_binary(tupledata, noTuples * tuplesize);
		pkeyindex = indexfactory.createIndex(0, keysize);
		query.setConnection(connection);
		reindex();
	}
	BOOST_SERIALIZATION_SPLIT_MEMBER()

	//computes the offsetSize structure
	//to be called after all the fields were added
	void computeOffsetSize();

	//computes the tuple size
	//to be called after all the fields were added
	void computeTupleSize();

	//removes unnecessary timestamps
	void reduceTimestamps();

	//no copy constructor
	Region(const Region&);

	//no assignment operator
	Region& operator =(const Region&);

	static int noregionsallocated;
public:

	explicit Region(Connection& _connection);

	Region(Connection& _connection, const Query& _query);

	Region(Connection& _connection, const Query& _query, const NQuadTimestamps& _timestamps);

	//destructor
	~Region()
	{
		noregionsallocated--;
		LOG_INFO("Removing region! " << noregionsallocated);
		free(tupledata);
	}

	const Query& getQuery() const
	{
		return query;
	}

	const NQuadTimestamps& getTimestamps() const
	{
		return timestamps;
	}

	int getTimestamp(const NQuad& box) const;

	int getTupleSize()
	{
		return tuplesize;
	}

	int getNoTuples() const
	{
		return noTuples;
	}

	//returns a pointer to the beginning of the
	//kth tuple
	void* tuple(unsigned int k)
	{
		return (char*) tupledata + k * tuplesize;
	}

	void* getData()
	{
		return tupledata;
	}

	//inserts a new tuples
	void* insert();



	//stores a chiar varying value; returns the index
	int storeVarying(const char* value);


	//indexes the tuple that start at t
	void index(void *t)
	{
		pkeyindex->indexTuple(t);
	}

	//indexes the kth tuple
	void index(unsigned int k)
	{
		index((char*) tupledata + k * tuplesize);
	}

	std::pair<int, int> getOffsetSize(int fieldno)
	{
		return offsetsize[fieldno];
	}

	//sets the capacity of the buffer, in number of tuples
	void setCapacity(int _capacity);


	//reindexes the region
	void reindex();

	//executes a select project query
	RegionPtr selectproject(FieldsMask fields, const Predicate& pred); //predicate

	//executes a union query
	RegionPtr unionq(const std::vector<RegionPtr>& that);

	//executes a join query
	RegionPtr join(RegionPtr that);

	//computes the hash
	unsigned int checkSum();

	//returns the size of the regions (in bytes)
	unsigned int size() const
	{
		return noTuples * tuplesize;
	}

};

std::ostream& operator <<(std::ostream& os, const Region& s);

class RegionResultSet: public ResultSet
{
private:
	friend class boost::serialization::access;
	Connection& connection;

	std::map<unsigned int, std::pair<unsigned int, RegionPtr> > regionindex;
	int notuples;

	std::string tablename;
	//the fields the in the right order
	std::vector<int> fields;

	//the offset of each field
	std::vector<int> offset;

	Table table;

public:
	RegionResultSet(Connection& _connection, const std::string& _tablename, const std::vector<int>& _fields);
	void addRegion(RegionPtr reg);
	~RegionResultSet()
	{
	}
	virtual unsigned int getNoTuples() const;
	virtual unsigned int getNoFields() const;
	virtual std::string getFieldName(int fieldno) const;
	virtual std::string getValue(unsigned int tupleno, unsigned int fieldno) const;
	virtual std::string getValue(unsigned int tupleno, const std::string& field) const;

	virtual std::string getFieldType(int fieldno) const {return "";};
	virtual std::string getFieldType(const std::string& field) const {return "";};
	virtual int getFieldSize(int fieldno) const {return 0;};
	virtual int getFieldSize(const std::string& field) const {return 0;};

};

}
;
BOOST_CLASS_TRACKING(coopsc::Region, boost::serialization::track_never)
BOOST_CLASS_TRACKING(coopsc::RegionResultSet, boost::serialization::track_never)

#endif
