#include "index.h"
#include "region.h"

namespace coopsc
{

IndexPtr IndexFactory::createIndex(int offset, int size)
{
	switch (size)
	{
	case sizeof(int):
		return IndexPtr(new Index<int> (region, offset));
		break;
	case sizeof(long long):
		return IndexPtr(new Index<long long> (region, offset));
		break;
	default:
		throw std::logic_error("can not index");
	}
}

//returns the a pointer to the tuple
void* IndexBase::find(const void* ptr)
{
	int offset = findOffset(ptr);
	return (char*) region.getData() + offset;
}

void IndexBase::indexTuple(const void* tuple)
{
	add((char*) tuple + fieldoffset, (char*) tuple - (char*) region.getData());
}

}
