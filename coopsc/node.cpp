#include "node.h"
#include "connection.h"

namespace coopsc
{
std::ostream& operator <<(std::ostream& s, const Node& node)
{
	node.writeTo(s);
	return s;
}

//executes the query and returns the result
RegionPtr Node::execute(const NQuadTimestamps &ts, bool keepstatistics)
{
	std::vector<RegionPtr> childrenresults;
	std::string error;

	for (auto it = children.begin(); it != children.end(); it++)
	{
		try
		{
			childrenresults.push_back((*it)->execute(ts, keepstatistics));
		} catch (std::runtime_error& e)
		{
			error = e.what();
		}
	}
	if (error != "")
		throw std::runtime_error(error);

	RegionPtr result = executeNode(childrenresults, keepstatistics);
	const NQuadTimestamps& rts = result->getTimestamps();
	for (auto it = ts.begin(); it != ts.end(); it++)
	{
		auto jt = rts.find(it->first);
		if (jt == rts.end())
			continue;
		if (it->second != jt->second)
			throw std::runtime_error("Invalid timestamp");
	}
	return result;
}

void Node::preExecute()
{
	for (auto it = children.begin(); it != children.end(); it++)
		(*it)->preExecute();
	return preExecuteNode();
}

void Node::setConnection(Connection& _connection)
{
	connection = &_connection;
	for (auto it = children.begin(); it != children.end(); it++)
		(*it)->setConnection(*connection);
}

}
