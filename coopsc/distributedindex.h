#ifndef DISTRIBUTEDINDEX_H_
#define DISTRIBUTEDINDEX_H_
#include <string>
#include <map>
#include <utility>
#include <boost/smart_ptr.hpp>
#include <boost/tuple/tuple.hpp>
#include <boost/thread.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/tuple/tuple_comparison.hpp>
#include <boost/multi_index_container.hpp>
#include <boost/multi_index/ordered_index.hpp>
#include <boost/multi_index/key_extractors.hpp>
#include <cstdlib>
#include <ctime>
#include "region.h"
#include "queryoptimizer.h"
#include "queryplan.h"
#include "nquad.h"
#include "nquadnode.h"
#include "regiondescriptor.h"
namespace coopsc
{

class Connection;

class DistributedIndex
{
	friend class NQuadNode;
	friend class NQuadChildrenRewriter;
private:

	struct RewritingSession
	{
		boost::condition_variable rewcond;
		boost::mutex rewcondmutex;
		//the result of the current rewriting
		std::map<std::string, QueryPlan> result;
		std::map<int, int> nolevelrequests;
		bool rewritingfinished;
	};
	typedef boost::shared_ptr<RewritingSession> RewritingSessionPtr;

	Connection& connection;

	std::string me;

	//it's actually a ChimeraState struct
	//including "chimera.h" in this file generates a strange  error
	void *chimerastate;



	boost::mutex indexmutex;

	std::map<NQuad, NQuadNodePtr> nodes;

	//last session id
	int lastsessionid;
	std::map<int, RewritingSessionPtr> rewsessions;

	bool connected;

	std::map<int, boost::shared_ptr<boost::shared_mutex>> levelmutexes;

	//number of replicas
	static const int NO_REPLICAS = 0;

	//message types
	static const int ADD_REGION = 150;
	static const int REMOVE_REGION = 151;
	static const int REPLICATE_ADD_REGION = 152;
	static const int REPLICATE_REMOVE_REGION = 153;
	static const int REWRITE_REQUEST = 154;
	static const int REWRITE_RESPONSE = 155;
	//send from the distributed index to the nodes
	static const int REMOVE_REGION_REVERSE = 157;

	static const int REWRITING_WAIT_TIME = 200;

	void initChimeraMessages();
	void receiveAddRegion(char *msg, unsigned int size);
	void receiveRemoveRegion(char *msg, unsigned int size);
	void receiveRewriteRequest(char *msg, unsigned int size);
	void receiveRewriteResponse(char *msg, unsigned int size);
	void receiveRemoveRegionReverse(char *msg, unsigned int size);

	//send the specified message to replicas
	void replicate(int type, char* payload, unsigned int size);

	DistributedIndex(const DistributedIndex&);
	DistributedIndex& operator =(const DistributedIndex&);

	NQuadNodePtr getNQuadNode(const NQuad& nquad);
	bool nquadExists(const NQuad& nquad);

	void addRegion(const NQuad& nquad, const RegionDescriptor& reg, const NQuadTimestamps& ts);
	void removeRegion(const NQuad& nquad, const RegionDescriptor& reg);

	void removeRegionReverse(const PeerId& dest, int regId);
	void rewrite(const NQuad& nquad, const PeerId& source, int reqId, const std::string& prefix, const Query& query, const NQuadTimestamps& ts);
	void rewriteResponse(const PeerId& source, int level, int reqId, const std::string& prefix, const QueryPlan& plan, int childrenrequests);

public:

	DistributedIndex(Connection& _connection);

	~DistributedIndex();

	Connection& getConnection()
	{
		return connection;
	}

	void connect();
	void disconnect();

	//adds a semantic region to the distributed index
	void add(int regId, RegionPtr reg);

	//removes a semantic region from the index
	void remove(int regId, RegionPtr reg);

	//rewrites a query using the distributed index
	QueryPlan rewrite(const Query& query, const NQuadTimestamps& timestamps);

	//Optimizes a query plan by trying to rewrite ServerNodes using the
	//distributed index
	void optimize(QueryPlan& plan, const NQuadTimestamps& timestamps);

	void handleMessage(int type, char* payload, unsigned int size);

};
}

#endif /* DISTRIBUTEDINDEX_H_ */

