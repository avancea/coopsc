#ifndef SELECTPROJECT_H_
#define SELECTPROJECT_H_
#include "node.h"

namespace coopsc
{
//selection
class SelectProjectNode: public Node
{
private:
	friend class boost::serialization::access;
	template<class Archive>
	void serialize(Archive & ar, const unsigned int version)
	{
		ar & boost::serialization::base_object<Node>(*this);
		ar & tablename;
		ar & fields;
		ar & predicate;
	}
	std::string tablename;
	FieldsMask fields;
	Predicate predicate;
protected:
	virtual RegionPtr executeNode(const std::vector<RegionPtr>& childrenresults, bool keepstatistics);
public:
	SelectProjectNode()
	{
	}

	SelectProjectNode(Connection& _connection, const std::string& _tablename, FieldsMask _fields, const Predicate& _predicate,
			NodePtr _child) :
		Node(_connection, _child), tablename(_tablename), fields(_fields), predicate(_predicate)
	{
	}

	virtual void setConnection(Connection& connection)
	{
		Node::setConnection(connection);
		predicate.setConnection(connection);
	}

	FieldsMask getFields() const
	{
		return fields;
	}

	std::string getFieldsAsString() const;

	const std::string& getTableName() const
	{
		return tablename;
	}

	const Predicate& getPredicate() const
	{
		return predicate;
	}

	void setPredicate(const Predicate& pred)
	{
		predicate = pred;
	}

	virtual void writeTo(std::ostream& s) const;
};

}

#endif /* SELECTPROJECT_H_ */
