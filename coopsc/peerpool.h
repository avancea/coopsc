#ifndef COOPSCCONNECTIONPOOL_H_
#define COOPSCCONNECTIONPOOL_H_
#include <string>
#include <map>
#include <utility>
#include "peer.h"
namespace coopsc
{
class Connection;

class PeerPool
{
private:
	Connection& connection;
	std::map<PeerId, PeerPtr> pool;
public:
	PeerPool(Connection& _connection) :
		connection(_connection)
	{
	}

	//disable copy constructor
	PeerPool(const PeerPool&)= delete;

	//disable assignment
	PeerPool& operator =(PeerPool&)= delete;

	void remove(const PeerId& peerId);

	PeerPtr get(const PeerId& peerId);
};

}

#endif /*COOPSCCONNECTIONPOOL_H_*/
