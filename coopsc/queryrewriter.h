#ifndef QUERYREWRITER_H_
#define QUERYREWRITER_H_
#include "query.h"
#include "queryplan.h"
#include "storagemanager.h"
#include "queryoptimizer.h"
#include "nodes.h"
#include "queryplanfactory.h"

namespace coopsc
{
class Connection;

class QueryRewriter
{
private:
	Connection& connection;

	template<class InputIterator>
	NodePtr createRegionNode(InputIterator it)
	{
		QueryPlanFactory factory(connection);
		NodePtr regnode = factory.region(it->getId());

		if (it->remote())
			regnode = factory.remote(factory.queryPlan(regnode), it->getPeerId());
		return regnode;
	}

	//the recursive rewriting method
	template<class InputIterator>
	NodePtr rewriteRec(InputIterator first, InputIterator last, const Query& query)
	{
		QueryPlanFactory factory(connection);
		for (InputIterator it = first; it != last; it++)
		{

			//different tables
			if (query.getTableName() != it->getQuery().getTableName())
				continue;

			if (query.getPredicate().getFields() != it->getQuery().getPredicate().getFields())
				continue;

			bool verticallyContains = it->getQuery().verticallyContains(query);
			bool horizontallyContains = it->getQuery().horizontallyContains(query);
			bool verticallyIntersects = false;
			bool horizontallyIntersects = false;
			Predicate intersPred;
			FieldsMask intersFields;

			if (!verticallyContains)
				verticallyIntersects = it->getQuery().verticallyIntersects(query, intersFields);
			else
				verticallyIntersects = true;

			if (!horizontallyContains)
				horizontallyIntersects = it->getQuery().horizontallyIntersect(query, intersPred);
			else
				horizontallyIntersects = true;

			if (verticallyContains && horizontallyContains) // total containment
				return factory.selectProject(query.getTableName(), query.getFields(), query.getPredicate(), createRegionNode(it));

			if (verticallyContains && horizontallyIntersects)
			{
				NodePtr n1 = factory.selectProject(query.getTableName(), query.getFields(), intersPred, createRegionNode(it));

				it++;
				NodePtr n2 = rewriteRec(it, last, Query(connection, query.getTableName(), query.getPredicate() - intersPred,
						query.getFields(), query.getKey()));
				return factory.uni(n1, n2);
			}

			if (horizontallyContains && verticallyIntersects)
			{
				FieldsMask diffFields;
				diffFields = ((query.getFields() & (~intersFields)) | query.getKey() | query.getPredicate().getFields());

				NodePtr n1 = factory.selectProject(query.getTableName(), intersFields, query.getPredicate(), createRegionNode(it));

				it++;
				NodePtr n2 =
						rewriteRec(it, last, Query(connection, query.getTableName(), query.getPredicate(), diffFields, query.getKey()));
				return factory.join(n1, n2);
			}

			if (horizontallyIntersects && verticallyIntersects)
			{
				FieldsMask diffFields;
				diffFields = ((query.getFields() & (~intersFields)) | query.getKey() | query.getPredicate().getFields());
				Predicate diffPred = query.getPredicate() - intersPred;

				NodePtr n1 = factory.selectProject(query.getTableName(), intersFields, intersPred, createRegionNode(it));

				it++;
				InputIterator jt = it;
				NodePtr n2 = rewriteRec(it, last, Query(connection, query.getTableName(), intersPred, diffFields, query.getKey()));
				NodePtr n3 = rewriteRec(jt, last, Query(connection, query.getTableName(), diffPred, query.getFields(), query.getKey()));
				return factory.uni(factory.join(n1, n2), n3);
			}

		}
		return factory.server(query);
	}

	//no copy constructor
	QueryRewriter(const QueryRewriter&);

	//no assignment operator
	QueryRewriter& operator =(const QueryRewriter&);

public:
	QueryRewriter(Connection& _connection) :
		connection(_connection)
	{
	}

	//rewrites the query using the entries specified by an iterator

	template<class InputIterator>
	QueryPlan rewrite(InputIterator first, InputIterator last, const Query& query)
	{
		QueryPlanFactory factory(connection);

		QueryPlan result = factory.queryPlan(rewriteRec(first, last, query));
		SelectProjectOptimizer selectprojectoptimizer(connection);
		selectprojectoptimizer.optimize(result);

		ReducerOptimizer reducer(connection);
		reducer.optimize(result);
		return result;
	}
};

}

#endif /* QUERYREWRITER_H_ */
