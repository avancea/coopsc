#include <stdexcept>
#include <climits>
#include <iterator>
#include <algorithm>
#include <math.h>
#include <sstream>
#include "predicate.h"
#include "connection.h"
#include "logger.h"
#include "interpreterevaluator.h"
using namespace std;

namespace coopsc
{

//infinity
const int Predicate::oo = INT_MAX;

//minus infinity
const int Predicate::_oo = INT_MIN;

Predicate::Predicate() :
	root(new PredicateNode()), connection(NULL)
{

}

Predicate::Predicate(Connection& _connection, const std::string& _tablename, bool value) :
	tablename(_tablename), root(new PredicateNode(value)), connection(&_connection)
{
}

Predicate::Predicate(const Predicate& that) :
	tablename(that.tablename), root(that.root), connection(that.connection)
{
}

Predicate::Predicate(Connection& _connection, const std::string& _tablename, int fieldno, int start, int end) :
	tablename(_tablename), root(new PredicateNode(fieldno, start, end)), connection(&_connection)
{
}

Predicate& Predicate::operator =(const Predicate& that)
{
	tablename = that.tablename;
	root = that.root;
	connection = that.connection;
	return *this;
}
EvaluatorPtr Predicate::createEvaluator(const std::vector<int>& offsets) const
{
	return EvaluatorPtr(new InterpreterEvaluator(*this, offsets));
}

Predicate Predicate::operator ||(const Predicate& p) const
{
	if (tablename != p.tablename)
		throw std::runtime_error("Different tables!");
	Predicate result(*connection, tablename);
	result.root = exec(PredicateNode::Or, root, p.root);
	return result;
}

Predicate Predicate::operator &&(const Predicate& p) const
{
	if (tablename != p.tablename)
		throw std::runtime_error("Different tables!");
	Predicate result(*connection, tablename);
	result.root = exec(PredicateNode::And, root, p.root);
	return result;
}

//negation
Predicate Predicate::operator !() const
{
	Predicate result(*connection, tablename);
	result.root = neg(root);
	return result;
}

bool Predicate::isTrue() const
{
	return root->isTrue();
}

bool Predicate::isFalse() const
{
	return root->isFalse();
}

bool Predicate::operator ==(const Predicate& p) const
{
	return tablename == p.tablename && (*root) == (*p.root);
}

void Predicate::getBounding(const PredicateNodePtr node, FieldsMask fields, unsigned int p, std::vector<int>& minc, std::vector<int>& maxc)
{
	if (node->isScalar())
		return;
	if (fields == 0)
		return;
	if (minc.size() >= p)
		minc.resize(p + 1, Predicate::oo);
	if (maxc.size() >= p)
		maxc.resize(p + 1, Predicate::_oo);

	int field;
	for (field = 0; (fields & (1 << field)) == 0; field++)
		;

	if (field != node->getField())
	{
		minc[p] = Predicate::_oo;
		maxc[p] = Predicate::oo;
		getBounding(node, fields & ~(1 << field), p + 1, minc, maxc);
		return;
	}
	PredicateNode::ChildrenMap::const_iterator it = node->children().begin();
	if (minc[p] > it->first)
		minc[p] = it->first;

	for (it = node->children().begin(); it != node->children().end(); it++)
	{
		if (it->second->isFalse())
			continue;

		int w;
		PredicateNode::ChildrenMap::const_iterator jt = it;
		jt++;
		if (jt == node->children().end())
			w = Predicate::oo;
		else
			w = jt->first - 1;

		if (maxc[p] < w)
			maxc[p] = w;

		getBounding(it->second, fields & ~(1 << field), p + 1, minc, maxc);
	}
}

void Predicate::getBounding(std::vector<int>& minc, std::vector<int>& maxc) const
{
	getBounding(root, root->getAllFields(), 0, minc, maxc);
}

//subsumption verification
bool Predicate::operator <(const Predicate& p) const
{
	try
	{
		return ((*this) || p) == p;
	} catch (...)
	{
		return false;
	}
}

ostream& operator <<(ostream& os, const Predicate& p)
{
	std::vector<string> fields;
	const Table& table = p.connection->getMetadataRegistry().getTable(p.tablename);
	table.getFieldNames(table.getFieldsMask(), fields);
	os << toString(p.root, fields);
	return os;
}
}
