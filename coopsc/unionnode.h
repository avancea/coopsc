#ifndef UNIONNODE_H_
#define UNIONNODE_H_
#include "node.h"
namespace coopsc
{
//Executes the union of its children
class UnionNode: public Node
{
private:
	friend class boost::serialization::access;
	template<class Archive>
	void serialize(Archive & ar, const unsigned int version)
	{
		ar & boost::serialization::base_object<Node>(*this);
	}
protected:
	virtual RegionPtr executeNode(const std::vector<RegionPtr>& childrenresults, bool keepstatistics);
public:
	UnionNode()
	{
	}

	//constructs a union node that executes the union of two sub-elements
	//new sub-elements can be added bye accessing the children vector (through
	// the getChildren accessor)
	UnionNode(Connection& _connection, NodePtr _left, NodePtr _right) :
		Node(_connection, _left, _right)
	{
	}
	virtual void writeTo(std::ostream& s) const;
};

}

#endif /* UnionNodeNODE_H_ */
