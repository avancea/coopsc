#ifndef EVALUATOR_H_
#define EVALUATOR_H_

#include <vector>
#include <boost/smart_ptr.hpp>

namespace coopsc
{

//just a predicate
class Predicate;

//handles the evaluation of predicates
class Evaluator
{
private:
	//the predicate to be evaluated
	const Predicate& pred;

	//the offsets of the fields  contained in the predicates relative to the beginning of the tuple
	std::vector<int> offsets;

protected:
	const Predicate& getPredicate()
	{
		return pred;
	}

	std::vector<int>& getOffsets()
	{
		return offsets;
	}
public:
	//the predicate to be evaluate
	//the offsets of fields
	Evaluator(const Predicate& _pred, const std::vector<int>& _offsets) :
		pred(_pred), offsets(_offsets)
	{

	}

	virtual ~Evaluator()
	{

	}

	//return true if the tuple given as parameter match the condition expressed by the predicate
	virtual bool evaluate(void *tuple) = 0;

};

typedef boost::shared_ptr<Evaluator> EvaluatorPtr;

}

#endif /* EVALUATOR_H_ */
