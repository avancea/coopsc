#ifndef SERVERNODE_H_
#define SERVERNODE_H_
#include <tuple>
#include "logger.h"
#include "node.h"
#include "concurrent_queue.h"
namespace coopsc
{
/*
 Executes the specified query directly on the server.
 This node is executed asynchronously. The preExecuteNode method initiate the query execution
 by calling DatabaseConnection::asyncQuery. After the result is received from the server,
 the database connection notifies the ServerNode by calling the method setResult.
 ServerNode::executeNode blocks the current thread until  it receives the notification from the database
 connection
 */
class ServerNode: public Node
{
private:
	friend class boost::serialization::access;
	template<class Archive>
	void serialize(Archive & ar, const unsigned int version)
	{
		ar & boost::serialization::base_object<Node>(*this);
		ar & query;
	}
	//successful	result		  error
	typedef std::tuple<bool, RegionPtr, std::string> Result;
	concurrent_queue<Result>* result;

	Query query;
protected:
	virtual void preExecuteNode();
	virtual RegionPtr executeNode(const std::vector<RegionPtr>& childrenresults, bool keepstatistics);
public:
	ServerNode() :
		result(NULL)
	{
	}

	ServerNode(Connection& _connection, Query _query) :
		Node(_connection), result(NULL), query(_query)
	{
	}
	~ServerNode()
	{
		if (result)
			delete result;
	}

	virtual void setConnection(Connection& connection)
	{
		Node::setConnection(connection);
		query.setConnection(connection);
	}

	const Query& getQuery() const
	{
		return query;
	}

	Query& getQuery()
	{
		return query;
	}

	void setResult(bool successful, RegionPtr region, std::string error);

	virtual void writeTo(std::ostream& s) const;
};
}

#endif /* SERVERNODE_H_ */
