#include "nquad.h"
#include "connection.h"
namespace coopsc
{
NQuad::NQuad(const NQuad& that) :
	connection(that.connection), tablename(that.tablename), fields(that.fields), coords(that.coords), l(that.l)
{
}

NQuad::NQuad(Connection& _connection, const std::string& _tablename, FieldsMask _fields, unsigned int _l, const std::vector<int>& _coords) :
	connection(&_connection), tablename(_tablename), fields(_fields), coords(_coords), l(_l)
{
	FieldsMask tmp = fields;
	unsigned int n = 0;
	while (tmp)
	{
		n++;
		tmp &= (tmp - 1);
	}
	if (n != coords.size())
		throw std::runtime_error("Can not create NQuad : invalid coordonates");
	for (unsigned int i = 0; i < coords.size(); i++)
		if ((coords[i] & ((1 << l) - 1)) != 0)
			throw std::runtime_error("Can not create NQuad : invalid coordonates");
}

void NQuad::getPredicateNQuad(const Predicate& pred, NQuad& result)
{

	std::vector<int> coords, maxc;

	unsigned int l = 0;
	pred.getBounding(coords, maxc);
	unsigned int n = coords.size();

	for (unsigned int i = 0; i < n; i++)
	{
		if (coords[i] < 0 && maxc[i] >= 0) throw std::runtime_error("Can not determine quad! Different signs!");

	}

	for (unsigned int i = 0; i < n; i++)
	{
		int tmp = coords[i] ^ maxc[i];
		unsigned int ll = 0;
		while (tmp)
		{
			ll++;
			tmp = tmp / 2;
		}
		if (ll > l)
			l = ll;
	}
	for (unsigned int i = 0; i < n; i++)
	{
		coords[i] = coords[i] & ~((1 << l) - 1);
	}
	result = NQuad(pred.getConnection(), pred.getTableName(), pred.getFields(), l, coords);
}

NQuad NQuad::getParent() const
{
	std::vector<int> pcoords = coords;
	unsigned int n = coords.size();
	for (unsigned int i = 0; i < n; i++)
	{
		pcoords[i] = coords[i] & ~((1 << (l + 1)) - 1);
	}
	return NQuad(*connection, tablename, fields, l + 1, pcoords);
}

void NQuad::getChildren(NQuads& children) const
{
	children.clear();
	if (l == 0)
		return;
	std::vector<int> ccoords = coords;
	unsigned int n = coords.size();
	for (int k = 0; k < (1 << n); k++)
	{
		for (unsigned int i = 0; i < n; i++)
			if (k & (1 << i))
				ccoords[i] = coords[i] + (1 << (l - 1));
			else
				ccoords[i] = coords[i];
		children.push_back(NQuad(*connection, tablename, fields, l - 1, ccoords));
	}
}

//return the predicate associate with the box
Predicate NQuad::getPredicate() const
{
	const Table& table = connection->getMetadataRegistry().getTable(tablename);
	unsigned int nofields = table.getNoFields();

	Predicate result(*connection, tablename, true);

	int k = 0;
	for (unsigned int i = 0; i < nofields; i++)
		if (test(fields, i))
		{
			int start = coords.at(k);
			int end = coords.at(k) + (1 << l) - 1;
			result = result && Predicate(*connection, tablename, i, start, end);
			k++;
		}
	return result;
}

bool operator <(const NQuad& q1, const NQuad& q2)
{
	if (q1.getTableName() < q2.getTableName())
		return true;
	if (q1.getTableName() > q2.getTableName())
		return false;
	if (q1.getFields() < q2.getFields())
		return true;
	if (q1.getFields() > q2.getFields())
		return false;
	if (q1.getLevel() < q2.getLevel())
		return true;
	if (q1.getLevel() > q2.getLevel())
		return false;
	for (unsigned int i = 0; i < q1.getCoords().size(); i++)
	{
		if (q1.getCoords()[i] < q2.getCoords()[i])
			return true;
		if (q1.getCoords()[i] > q2.getCoords()[i])
			return false;
	}
	return false;
}

bool NQuad::includes(const NQuad& that) const
{
	if (getTableName() != that.getTableName())
		return false;
	if (getFields() != that.getFields())
		return false;
	if (getLevel() < that.getLevel())
		return false;
	unsigned int n = coords.size();
	for (unsigned int i = 0; i < n; i++)
		if (that.coords[i] < coords[i] || coords[i] + (1 << l) - 1 < that.coords[i])
			return false;
	return true;
}

std::ostream& operator <<(std::ostream& os, const NQuad& q)
{
	os << q.getTableName() << " " << q.getFields() << " " << q.getLevel() << " ";
	BOOST_FOREACH(int k, q.getCoords())
	{
		os << k << " ";
	}
	return os;
}

bool operator ==(const NQuad& q1, const NQuad& q2)
{
	return q1.getTableName() == q2.getTableName() && q1.getFields() == q2.getFields() && q1.getLevel() == q2.getLevel() && q1.getCoords()
			== q2.getCoords();
}
}
