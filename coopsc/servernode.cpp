#include <boost/bind.hpp>
#include "servernode.h"
#include "connection.h"
namespace coopsc
{

void ServerNode::preExecuteNode()
{
	if (!result)
		result = new concurrent_queue<Result> ();
	getConnection().getDatabasePool().asyncQuery(query, boost::bind(&ServerNode::setResult, this, _1, _2, _3));
}

RegionPtr ServerNode::executeNode(const std::vector<RegionPtr>& childrenresults, bool keepstatistics)
{
	Result r;
	result->wait_and_pop(r);
	if (std::get<0>(r)) //successful
	{
		getConnection().getStorageManager().addRegion(std::get<1>(r));
		if (keepstatistics)
		{
			getConnection().getStatisticsRegistry().tuplesServer(std::get<1>(r)->getNoTuples());
		}
		return std::get<1>(r);
	}
	throw std::runtime_error(std::get<2>(r));

}

void ServerNode::setResult(bool successful, RegionPtr region, std::string error)
{
	if (result)
		result->push(Result(successful, region, error));
}

void ServerNode::writeTo(std::ostream& s) const
{
	s << "server(" << query << ")";
}

}
