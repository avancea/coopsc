#include <stdexcept>
#include "queryplan.h"
#include "connection.h"
#include "nodes.h"
#include "logger.h"
namespace coopsc
{
void QueryPlan::setConnection(Connection& _connection)
{
	if (connection)
		return;
	connection = &_connection;
	root->setConnection(*connection);
}

std::ostream& operator <<(std::ostream& s, const QueryPlan& plan)
{
	s << "plan : " << *plan.getRoot();
	return s;
}
}
