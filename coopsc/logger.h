#ifndef LOGGER_H_
#define LOGGER_H_
#define DISABLE_MULTITHREAD_LOGGER
/*
 * Logging library
 *
 * How to use :
 * INIT_LOGGER (streamfile) - redirects the logs to an output stream or to a text file
 * LOG_ERROR (a << b << c) -  	  prints an error log
 * LOG_WARNING (a << b << c) -    prints a warning log *
 * LOG_INFO (a << b << c) -       prints an info log
 *
 * Configuration macros :
 * DISABLE_LOGGER  			- disables the logger
 * DISABLE_ERROR_LOGGING    - disables the printing of error logs
 * DISABLE_WARNING_LOGGING  - disables the printing of warning logs
 * DISABLE_INFO_LOGGING     - disables the printing of info logs
 *
 * DISABLE_MULTITHREAD_LOGGER - disables multithread synchronization
 *
 */
#include <iostream>
#include <fstream>
#include <stdexcept>
#include <string>
#include <ctime>
#include <sstream>
#include <unistd.h>

#ifndef DISABLE_MULTITHREAD_LOGGER
#include <boost/thread.hpp>
#endif

#define INIT_LOGGER(filestream) coopsc::Logger::init(filestream)

#ifndef DISABLE_LOGGER

#ifndef DISABLE_MULTITHREAD_LOGGER
#define __LOG(type, msg) {						\
    boost::unique_lock<boost::mutex> lock(coopsc::Logger::get().getMutex()); \
    (coopsc::Logger::get().stream()  << coopsc::Logger::get().getHostName() << " : " <<  coopsc::Logger::getDateTime() \
     <<  " " << type << " (" << __FILE__ << ", "  << __func__ << ", " << __LINE__  << ") : " \
     << msg << std::endl).flush();					\
  }while (0)
#else
#define __LOG(type, msg)  (coopsc::Logger::get().stream() << coopsc::Logger::get().getHostName() << " : "  << coopsc::Logger::getDateTime() \
			   <<  " " << type << " (" << __FILE__ << ", "  << __func__ << ", " << __LINE__  << ") : " \
			   << msg << std::endl).flush();

#endif

#ifndef DISABLE_ERROR_LOGGING
#define LOG_ERROR(msg)   __LOG("ERROR  ", msg)
#else
#define LOG_ERROR(msg)   0
#endif

#ifndef DISABLE_WARNING_LOGGING
#define LOG_WARNING(msg) __LOG("WARNING", msg)
#else
#define LOG_WARNING(msg) 0
#endif

#ifndef DISABLE_INFO_LOGGING
#define LOG_INFO(msg)    __LOG("INFO   ", msg)
#else
#define LOG_INFO(msg)   0
#endif

#else

#define LOG(type, a) 0

#define LOG_ERROR(a)   0
#define LOG_WARNING(a) 0
#define LOG_INFO(a)    0

#endif

namespace coopsc
{

//Logger class
class Logger
{
private:
	static Logger* instance;

	static std::ofstream* fstream;

	char hostname[200];

	std::ostream& out;

#ifndef DISABLE_MULTITHREAD_LOGGER
	boost::mutex m;
#endif

	Logger(std::ostream& _out) :
		out(_out)
	{
		gethostname(hostname, sizeof(hostname));
	}

public:

	//logs to console
	static void init(std::ostream& out)
	{
		instance = new Logger(out);
	}

	//logs to a specified file
	static void init(const std::string& fileName)
	{
		fstream = new std::ofstream(fileName.c_str(), std::ios_base::app);
		instance = new Logger(*fstream);
	}

	//closes the logger
	static void close()
	{
		if (instance == NULL)
			throw std::runtime_error("Logger not initialized!");
		delete instance;
		delete fstream;
	}

	static Logger& get()
	{
		if (instance == NULL)
			throw std::runtime_error("Logger not initialized!");
		return *instance;
	}

	std::ostream& stream()
	{
		return out;
	}

#ifndef DISABLE_MULTITHREAD_LOGGER
	boost::mutex& getMutex()
	{
		return m;
	}
#endif

	static std::string getDateTime()
	{
		std::time_t t;
		std::time(&t);
		std::string time(std::ctime(&t));
		time.resize(time.size() - 1);
		return time;
	}

	char* getHostName()
	{
		return hostname;
	}

};

}
#endif /*LOGGER_H_*/
