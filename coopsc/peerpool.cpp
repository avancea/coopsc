#include "peerpool.h"
using namespace std;
namespace coopsc
{

void PeerPool::remove(const PeerId& peerId)
{
	pool.erase(peerId);
}

PeerPtr PeerPool::get(const PeerId& peerId)
{
	auto it = pool.find(peerId);
	if (it != pool.end())
	{
		if (it->second->isZombie())
			it->second.reset();
		else
			return it->second;
	}
	PeerPtr peer = PeerPtr(new Peer(connection, peerId));
	peer->connect();
	pool[peerId] = peer;
	return peer;
}

}
