#include <string>
#include <cstdlib>
#include <cstring>
#include <algorithm>
#include <utility>
#include <boost/archive/binary_oarchive.hpp>
#include <boost/archive/binary_iarchive.hpp>
#include <algorithm>
#include <stdexcept>
#include <boost/foreach.hpp>
#include <boost/bind.hpp>
#include <boost/iterator_adaptors.hpp>
#include <boost/iterator/transform_iterator.hpp>
#include "logger.h"
#include "storagemanager.h"
#include "connection.h"
#include "queryoptimizer.h"
#include "queryrewriter.h"
#include "queryplanfactory.h"

using boost::any_cast;
using namespace std;

namespace coopsc
{
StorageManager::~StorageManager()
{
	clear();
}

void StorageManager::add(int regid, RegionPtr region)
{
	NQuads nquads;
	region->getQuery().getTimestampNQuads(nquads);

	bool canadd = true;
	BOOST_FOREACH(NQuad& nquad, nquads)
		{
			int regts = region->getTimestamps().find(nquad)->second;
			if (regts < timestamps[nquad])
				canadd = false;

			updateTimestamp(nquad, regts);
		}

	if (!canadd)
	{
		return;
	}

	if (connection.getCacheType() == CooperativeCache)
		connection.getDistributedIndex().add(regid, region);

	BOOST_FOREACH(NQuad& nquad, nquads)
	{
		int regts = region->getTimestamps().find(nquad)->second;
		timestamps[nquad] = regts;
		nquadtimestampindex[nquad].insert(regid);
	}

	regions[regid] = region;
	accesslist.push_back(regid);
	size += region->size();
}

void StorageManager::removeRegion(int regid, bool removefromdistributedindex)
{
	boost::unique_lock<boost::recursive_mutex> lock(mutex);

	if (regions.find(regid) == regions.end())
	{
		return;
	}
	if (connection.getCacheType() == CooperativeCache && removefromdistributedindex)
		connection.getDistributedIndex().remove(regid, regions[regid]);
	size -= regions[regid]->size();

	NQuads nquads;
	regions[regid]->getQuery().getTimestampNQuads(nquads);
	BOOST_FOREACH(NQuad& nquad, nquads)
	{
		nquadtimestampindex[nquad].erase(regid);
	}

	regions.erase(regid);
	accesslist.remove(regid);
}

void StorageManager::updateTimestamp(NQuad& nquad, int timestamp)
{
	if (timestamp <= timestamps[nquad])
		return;
	std::set<int> regs = nquadtimestampindex[nquad];
	BOOST_FOREACH(int r, regs)
	{
		removeRegion(r);
	}
	timestamps[nquad] = timestamp;
}

void StorageManager::useRegion(int regid)
{
	boost::unique_lock<boost::recursive_mutex> lock(mutex);
	accesslist.remove(regid);
	accesslist.push_back(regid);
}

void StorageManager::addRegion(RegionPtr region)
{
	boost::unique_lock<boost::recursive_mutex> lock(mutex);
	if (region->size() > connection.getCacheSize())
	{
		return;
	}
	int regid = nextId();
	add(regid, region);
	while (size > connection.getCacheSize())
	{
		LOG_INFO("Removing region : " << size << " " << connection.getCacheSize());
		removeRegion(*accesslist.begin());
	}
}

int StorageManager::nextId()
{
	lastid++;
	return lastid;
}

RegionPtr StorageManager::getRegion(int id)
{
	boost::unique_lock<boost::recursive_mutex> lock(mutex);
	auto it = regions.find(id);
	if (it == regions.end())
	{
		LOG_ERROR("Region lost : " << id);
		throw std::runtime_error("Region not found");
	}
	return it->second;
}

void StorageManager::clear()
{
	boost::unique_lock<boost::recursive_mutex> lock(mutex);
	for (auto it = regions.begin(); it != regions.end();)
	{
		auto jt = it;
		jt++;
		removeRegion(it->first);
		it = jt;
	}
	regions.clear();
	size = 0;
}

struct RegionIteratorAdapter
{
	int id;
	Region& reg;

	RegionIteratorAdapter(int _id, Region& _reg) :
		id(_id), reg(_reg)
	{
	}

	int getId() const
	{
		return id;
	}

	const Query& getQuery() const
	{
		return reg.getQuery();
	}

	bool remote() const
	{
		return false;
	}
	PeerId getPeerId() const
	{
		throw std::runtime_error("Not a remote region");
	}

};

RegionIteratorAdapter iteratorTransformer(StorageManager& storage, int id)
{
	return RegionIteratorAdapter(id, *storage.getRegion(id));
}

//rewrites the query using the content of the local cache
QueryPlan StorageManager::rewrite(const Query& query, const NQuadTimestamps& ts)
{
	boost::unique_lock<boost::recursive_mutex> lock(mutex);
	NQuads nquads;
	query.getTimestampNQuads(nquads);
	bool canrewrite = true;




	std::set<int> candidates;

	BOOST_FOREACH(NQuad& nquad, nquads)
	{
		auto it = ts.find(nquad);

		if (it == ts.end())
		{
			LOG_ERROR("Something is wrong!");
			continue;
		}

		int regts = it->second;
		if (regts < timestamps[nquad])
			canrewrite = false;
		updateTimestamp(nquad, regts);
	}

	BOOST_FOREACH(NQuad& nquad, nquads)
	{
		auto it = ts.find(nquad);
		if (it == ts.end())
		{
			LOG_ERROR("Something is wrong!");
			continue;
		}
		std::set<int> temp_set;
		std::set_union(candidates.begin(), candidates.end(), nquadtimestampindex[nquad].begin(),
				nquadtimestampindex[nquad].end(), std::inserter(temp_set, temp_set.begin()));
		candidates.swap(temp_set);
	}

	if (!canrewrite)
	{
		QueryPlanFactory factory(connection);
		return factory.queryPlan(factory.server(query));
	}
	QueryRewriter rewriter(connection);
	return rewriter.rewrite(boost::make_transform_iterator(candidates.begin(), std::bind1st(std::ptr_fun(iteratorTransformer), *this)),
			boost::make_transform_iterator(candidates.end(), std::bind1st(std::ptr_fun(iteratorTransformer), *this)), query);
}

}
