#ifndef DATABASECONNECTION_H_
#define DATABASECONNECTION_H_
#include <string>
#include <set>
#include <boost/function.hpp>
#include <libpq-fe.h>
#include "region.h"
#include "concurrent_queue.h"

namespace coopsc
{

class Connection;

// A connection to a specific PostgreSQL database which supports query execution
class DatabaseConnection
{
public:
	//callback function type for asynchronous queries
	typedef boost::function<void(bool successful, RegionPtr region, std::string error)> Callback;
	struct Job
	{
		QueryPtr query;
		Callback callback;
		Job()
		{

		}
		Job(const Job& job) :
			query(job.query), callback(job.callback)
		{
		}
		Job(QueryPtr _query, const Callback& _callback) :
			query(_query), callback(_callback)
		{
		}
	};
	typedef concurrent_queue<Job> JobQueue;
private:
	typedef boost::shared_ptr<boost::thread> ThreadPtr;

	Connection& connection;

	//The Postgresql connection
	PGconn *psql;

	//mutex for database access
	boost::recursive_mutex dbmutex;

	//used to stop the execution of the thread
	bool mustdie;

	ThreadPtr thread;
	concurrent_queue<Job>& jobs;
	bool connected;

	std::string escape(const std::string& s);

	void readPrimaryKey(const std::string& tablename, std::set<std::string>& key);

	//serializes
	void serialize(const Field& field, const char* value, char *dest);

	DatabaseConnection(DatabaseConnection&);
	DatabaseConnection& operator=(DatabaseConnection&);

	void readPostgreSQLMetadata();
	void readCoopSCMetadata();
public:
	//thread methods
	void operator()();
	RegionPtr query(const Query& q);

	DatabaseConnection(Connection& _connection, JobQueue& _jobs);
	DatabaseConnection(const DatabaseConnection&)= delete;
	DatabaseConnection& operator =(const DatabaseConnection&)= delete;

	void connect();
	void disconnect();

	void readMetadata();

	ResultSetPtr query(const std::string& q);


	void execute(const std::string& s);
	void getNQuadsTimestamps(const NQuads& nquads, NQuadTimestamps& result);

	~DatabaseConnection()
	{
		if (connected)
			disconnect();
	}
};

typedef boost::shared_ptr<DatabaseConnection> DatabaseConnectionPtr;

class ServerResultSet: public ResultSet
{
private:
	Connection& connection;
	PGresult* res;
public:
	ServerResultSet(Connection& _connection, PGresult* _res);
	~ServerResultSet();
	virtual unsigned int getNoTuples() const;
	virtual unsigned int getNoFields() const;
	virtual std::string getFieldName(int fieldno) const;
	virtual std::string getValue(unsigned int tupleno, unsigned int fieldno) const;
	virtual std::string getValue(unsigned int tupleno, const std::string& field) const;


	virtual std::string getFieldType(int fieldno) const;
	virtual std::string getFieldType(const std::string& field) const;
	virtual int getFieldSize(int fieldno) const;
	virtual int getFieldSize(const std::string& field) const;

};



}

#endif /* DATABASECONNECTION_H_ */
