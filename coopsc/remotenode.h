#ifndef REMOTENODE_H_
#define REMOTENODE_H_
#include <tuple>
#include "node.h"
#include "queryplan.h"
#include "concurrent_queue.h"
#include "peer.h"
namespace coopsc
{
/*
 Executes the child query plan on a remote CoopSC node
 This node is executed asynchronously. The preExecuteNode method initiate the query execution
 by calling Peer::asyncExecute. After the result is received from the peer, the Peer object notifies the
 RemoteNode by calling the method setResult.
 RemoteNode::executeNode blocks the current thread until the it receives the notification from the corresponding
 Peer object.
 */
class RemoteNode: public Node
{
private:
	friend class boost::serialization::access;
	template<class Archive>
	void serialize(Archive & ar, const unsigned int version)
	{
		ar & boost::serialization::base_object<Node>(*this);
		ar & child;
		ar & peerId;
	}

	//successful	result		  error
	typedef std::tuple<bool, RegionPtr, std::string> Result;
	concurrent_queue<Result>* result;

	QueryPlan child;
	PeerId peerId;
protected:
	virtual void preExecuteNode();
	virtual RegionPtr executeNode(const std::vector<RegionPtr>& childrenresults, bool keepstatistics);
public:
	RemoteNode() :
		result(NULL)
	{
	}

	RemoteNode(Connection& _connection, const QueryPlan& _child, const PeerId& _peerId) :
		Node(_connection), result(NULL), child(_child), peerId(_peerId)
	{
	}

	~RemoteNode()
	{
		if (result)
			delete result;
	}

	virtual void setConnection(Connection& connection)
	{
		Node::setConnection(connection);
		child.setConnection(connection);
	}

	QueryPlan& getQueryPlan()
	{
		return child;
	}

	const PeerId& getPeerId() const
	{
		return peerId;
	}

	void setResult(bool successful, RegionPtr region, std::string error);

	virtual void writeTo(std::ostream& s) const;
};

}

#endif /* REMOTENODE_H_ */
