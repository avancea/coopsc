#ifndef CACHEMANAGER_H_
#define CACHEMANAGER_H_

#include <boost/any.hpp>
#include <boost/thread.hpp>

#include <vector>
#include <map>
#include <list>
#include "region.h"
#include "queryplan.h"
#include "regiondescriptor.h"
namespace coopsc
{

//CoopSC connection forward declaration
class Connection;

//Stores the semantic regions
class StorageManager
{
public:
	typedef std::map<int, RegionPtr> RegionsType;
	typedef std::list<int> AccessListType;

private:

	Connection& connection;

	//regions
	RegionsType regions;

	AccessListType accesslist;

	//current size
	unsigned int size;

	int lastid;

	//for each box, the list of entries that intersects it
	std::map<NQuad, std::set<int>> nquadtimestampindex;

	NQuadTimestamps timestamps;

	boost::recursive_mutex mutex;

	//generates a region name
	int nextId();

	//no copy constructor
	StorageManager(StorageManager&);

	//no assignment operator
	StorageManager& operator =(StorageManager&);

	void add(int regid, RegionPtr region);

	void updateTimestamp(NQuad& nquad, int timestamp);

public:

	//constructor
	StorageManager(Connection& _connection) :
	connection(_connection), size(0), lastid(0)
	{
	}

	~StorageManager();

	//adds a region
	void addRegion(RegionPtr region);

	void removeRegion(int regid, bool removefromdistributedindex = true);

	//returns the list of regions
	RegionsType& getRegions()
	{
		return regions;
	}

	RegionPtr getRegion(int id);

	//used to notify that specified region was used in a query
	void useRegion(int regid);

	//clears the cache
	void clear();

	//rewrites the query using the content of the local cache
	QueryPlan rewrite(const Query& query, const NQuadTimestamps& ts);

	unsigned int getSize()
	{
		return size;
	}

};

}

#endif /*CACHEMANAGER_H_*/
