#include <boost/foreach.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/thread/locks.hpp>
#include "nquadnode.h"
#include "distributedindex.h"
#include "queryplanfactory.h"
#include "servernode.h"
#include "queryrewriter.h"
using namespace std;
using namespace boost;

namespace coopsc
{

class NQuadChildrenRewriter: public QueryOptimizer
{
private:
	NQuadNode& quadnode;
	PeerId source;
	int reqId;
	std::string prefix;
	NQuadTimestamps ts;
	int lastchild;
	int noreqs;
protected:
	NodePtr optimizeNode(NodePtr node);
public:
	NQuadChildrenRewriter(NQuadNode& _quadnode, const PeerId& _source, int _reqId, const std::string& _prefix, const NQuadTimestamps& _ts) :
		quadnode(_quadnode), source(_source), reqId(_reqId), prefix(_prefix), ts(_ts), lastchild(0), noreqs(0)
	{

	}

	int getNoRequests() const
	{
		return noreqs;
	}
};

NQuadNode::NQuadNode(DistributedIndex &_dindex, const NQuad& _nquad, boost::shared_mutex& _mutex) :
	dindex(_dindex), nquad(_nquad), mutex(_mutex)
{
	unsigned int minlevel = dindex.getConnection().getMetadataRegistry().getTable(nquad.getTableName()).getNQuadMinLevel(nquad.getFields());
	if (nquad.getLevel() > minlevel)
	{
		nquad.getChildren(children);
		BOOST_FOREACH(NQuad& nq, children)
		{
			childrensize[nq] = 0;
		}
	}
}

void NQuadNode::clean(const NQuad& nq)
{
	//TODO: optimize this if needed
	Predicate p = nq.getPredicate();
	for (auto it = regions.begin(); it != regions.end();)
	{
		auto jt = it;
		jt++;
		if (!(it->getQuery().getPredicate() && p).isFalse())
		{
			dindex.removeRegionReverse(it->getPeerId(), it->getId());
			regions.erase(it);
		}
		it = jt;
	}
}

void NQuadNode::addRegion(const RegionDescriptor& reg, const NQuadTimestamps& ts)
{
	boost::unique_lock<boost::shared_mutex> lock(mutex);
	NQuad nquadquery;
	NQuad::getPredicateNQuad(reg.getQuery().getPredicate(), nquadquery);

	for (auto it = ts.begin(); it != ts.end(); it++)
	{
		NQuad nq = it->first;
		nq.setConnection(dindex.getConnection());
		if (timestamps[nq] > it->second)
			return; // not inserting
		if (timestamps[nq] < it->second)
			clean(nq); // removing old entries
		timestamps[nq] = it->second;
	}
	if (nquadquery == nquad || children.size() == 0)
	{
		regions.insert(reg);
		return;
	}
	BOOST_FOREACH(NQuad& nq, children)
	if (nq.includes(nquadquery))
	{
		childrensize[nq]++;
		dindex.addRegion(nq, reg, ts);
		return;
	}
}

void NQuadNode::removeRegion(const RegionDescriptor& reg)
{
	boost::unique_lock<boost::shared_mutex> lock(mutex);

	NQuad nquadquery;
	NQuad::getPredicateNQuad(reg.getQuery().getPredicate(), nquadquery);

	if (nquadquery == nquad || children.size() == 0)
	{
		regions.erase(reg);
		return;
	}
	BOOST_FOREACH(NQuad& nq, children)
	if (nq.includes(nquadquery))
	{
		childrensize[nq]--;
		dindex.removeRegion(nq, reg);
		return;
	}
}

void NQuadNode::rewrite(const PeerId& source, int reqId, //request ID
		const std::string& prefix, const Query& query, const NQuadTimestamps& ts)
{
	boost::unique_lock<boost::shared_mutex> lock(mutex);
	bool err = false;
	for (auto it = ts.begin(); it != ts.end(); it++)
	{
		NQuad nq = it->first;
		nq.setConnection(dindex.getConnection());
		if (timestamps[nq] > it->second)
		{
			err = true;
			break;
		}
		if (timestamps[nq] < it->second)
			clean(nq); // removing old entries
		timestamps[nq] = it->second;
	}
	QueryPlanFactory factory(dindex.getConnection());
	QueryPlan plan = factory.queryPlan(factory.server(query));
	if (!err)
	{
		QueryRewriter rewriter(dindex.getConnection());
		plan = rewriter.rewrite(regions.begin(), regions.end(), query);

		NQuadChildrenRewriter crew(*this, source, reqId, prefix, ts);
		crew.optimize(plan);
		dindex.rewriteResponse(source, nquad.getLevel(), reqId, prefix, plan, crew.getNoRequests());
	}
	else
		dindex.rewriteResponse(source, nquad.getLevel(), reqId, prefix, plan, 0);
}

NodePtr NQuadChildrenRewriter::optimizeNode(NodePtr node)
{
	ServerNode* sn = dynamic_cast<ServerNode*> (node.get());
	if (sn == NULL) //not a SeverNode
		return node;
	Query& query = sn->getQuery();

	BOOST_FOREACH(NQuad& nq, quadnode.children)
	{
		Predicate p = query.getPredicate() && nq.getPredicate();
		if (!p.isFalse())
		{
			if (quadnode.childrensize[nq] != 0)
			{
				noreqs++;
				NQuadTimestamps t;
				for (auto it = ts.begin(); it != ts.end(); it++)
					if (nq.includes(it->first) || it->first.includes(nq))
					{
						t[it->first] = it->second;
					}
				quadnode.dindex.rewrite(nq, source, reqId, prefix + ":" + boost::lexical_cast<std::string>(lastchild), Query(
						quadnode.dindex.getConnection(), query.getTableName(), p, query.getFields(), query.getKey()), t);
			}
			lastchild++;
		}
	}
	return node;
}

}
