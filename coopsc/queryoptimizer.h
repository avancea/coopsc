#ifndef QUERYOPTIMIZER_H_
#define QUERYOPTIMIZER_H_
#include <string>
#include "queryplan.h"
#include "peer.h"
namespace coopsc
{

//generic query optimizer
class QueryOptimizer
{
private:
	//post order node traversal
	//returns the optimized node
	NodePtr postorder(NodePtr node);
protected:
	//returns the optimized node
	virtual NodePtr optimizeNode(NodePtr node) = 0;

public:
	void optimize(QueryPlan& plan)
	{
		plan.setRoot(postorder(plan.getRoot()));
	}
	virtual ~QueryOptimizer()
	{
	}
};

//Reduces the size of tree by doing the following optimizations:
// union(... union(x, y), ...) -> union(... .x, y, ...)
// union(... remote(x), remote(y), ...)  -> union(.... remote(union(x, y)), ...)
// union(....server(pred1), server(pred2)...) -> union(...server(pred1 or pred2) ...)
// join(remote(x), remote(y)) -> remote(join(x, y))
// selectproject(remote(x)) -> remote(selectproject(x))
// union(..., selectproject(region(X), p1), selectproject(region(X), p2), ....) ->
//                  union(..., selectproject(region(X), p1 or p2), ...)
class ReducerOptimizer: public QueryOptimizer
{
private:
	Connection& connection;
protected:
	NodePtr optimizeNode(NodePtr node);
public:
	ReducerOptimizer(Connection& _connection) :
		connection(_connection)
	{

	}

};

//Removes unnecessary select projects
class SelectProjectOptimizer: public QueryOptimizer
{
private:
	Connection& connection;
protected:
	NodePtr optimizeNode(NodePtr node);
public:
	SelectProjectOptimizer(Connection& _connection) :
		connection(_connection)
	{

	}
};

}
#endif /*QUERYOPTIMIZER_H_*/
