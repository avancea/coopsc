#include <stdexcept>
#include "metadataregistry.h"

namespace coopsc
{

void Table::addFields(const Field& field)
{
	fieldsmask |= (1 << fields.size());
	fields.push_back(field);
}

void Table::addKeyField(const Field& field)
{
	fieldsmask |= (1 << fields.size());
	keymask |= (1 << fields.size());
	fields.push_back(field);
}

int Table::getFieldNo(const std::string& name) const
{
	for (unsigned int i = 0; i < fields.size(); i++)
		if (fields[i].getName() == name)
			return i;
	throw std::runtime_error("Field not found");
}

FieldsMask Table::getNonKeyFieldsMask() const
{
	return fieldsmask & (~keymask);
}

void Table::getFields(FieldsMask mask, std::vector<Field>& result) const
{
	result.clear();
	for (unsigned int i = 0; i < fields.size(); i++)
		if (mask & (1 << i))
			result.push_back(fields[i]);
}

void Table::getFieldNames(FieldsMask mask, std::vector<std::string>& result) const
{
	result.clear();
	for (unsigned int i = 0; i < fields.size(); i++)
		if (mask & (1 << i))
			result.push_back(fields[i].getName());
}

const Field& Table::getField(unsigned int fieldNo) const
{
	if (fieldNo < fields.size())
		return fields[fieldNo];;
	throw std::runtime_error("Field not found");
}

void Table::addIndexLevels(FieldsMask fields, int minlevel, int maxlevel, int tslevel)
{
	minlevels[fields] = minlevel;
	maxlevels[fields] = maxlevel;
	tslevels[fields] = tslevel;
}

int Table::getNQuadMinLevel(FieldsMask fields) const
{
	auto it = minlevels.find(fields);
	if (it == minlevels.end())
		return 10;
	return it->second;
}

int Table::getNQuadMaxLevel(FieldsMask fields) const
{
	auto it = maxlevels.find(fields);
	if (it == maxlevels.end())
		return 14;
	return it->second;
}

int Table::getNQuadTimestampLevel(FieldsMask fields) const
{
	auto it = tslevels.find(fields);
	if (it == tslevels.end())
		return 13;
	return it->second;
}

Table& MetadataRegistry::getTable(const std::string& name)
{
	for (auto it = tables.begin(); it != tables.end(); it++)
		if (it->getName() == name)
			return *it;
	throw std::runtime_error("Table not found");
}

std::string MetadataRegistry::getType(int oid)
{
	auto it = types.find(oid);
	if (it == types.end()) throw std::runtime_error("Type not found");
	return it->second;
}


}

