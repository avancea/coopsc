#ifndef _REGIONDESCRIPTOR_H
#define _REGIONDESCRIPTOR_H

#include <boost/archive/binary_oarchive.hpp>
#include <boost/archive/binary_iarchive.hpp>
#include <boost/serialization/tracking.hpp>
#include <boost/serialization/map.hpp>
#include <boost/serialization/binary_object.hpp>
#include "logger.h"
#include "predicate.h"
#include "query.h"
#include "peer.h"

namespace coopsc
{
class RegionDescriptor
{
	friend class boost::serialization::access;
private:
	PeerId peerId;
	int id;
	Query query;

	template<class Archive>
	void serialize(Archive & ar, const unsigned int version)
	{
		ar & peerId & id & query;
	}

public:
	RegionDescriptor()
	{
	}

	void setConnection(Connection& conn)
	{
		query.setConnection(conn);
	}

	RegionDescriptor(const PeerId& _peerId, int _id, const Query& _query) :
		peerId(_peerId), id(_id), query(_query)
	{

	}

	RegionDescriptor(const RegionDescriptor& that) :
		peerId(that.peerId), id(that.id), query(that.query)
	{
	}

	const PeerId& getPeerId() const
	{
		return peerId;
	}

	int getId() const
	{
		return id;
	}

	const Query& getQuery() const
	{
		return query;
	}

	bool remote() const
	{
		return true;
	}

	bool operator <(const RegionDescriptor& r) const
	{
		if (id < r.id)
			return false;
		if (id > r.id)
			return true;

		if (peerId < r.peerId)
			return true;
		if (peerId > r.peerId)
			return false;

		return false;
	}
};

}

#endif
