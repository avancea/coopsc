#ifndef PEERSERVER_H_
#define PEERSERVER_H_
#include <boost/smart_ptr.hpp>
#include <boost/thread.hpp>
#include <boost/asio.hpp>
#include "queryplan.h"

namespace coopsc
{

class Connection;

typedef boost::shared_ptr<boost::asio::ip::tcp::iostream> TcpIostreamPtr;

class PeerServer
{
private:
	Connection& connection;
	TcpIostreamPtr stream;

public:
	PeerServer(const PeerServer& that) :
		connection(that.connection), stream(that.stream)
	{

	}

	PeerServer(Connection& _connection, TcpIostreamPtr _stream) :
		connection(_connection), stream(_stream)
	{
	}

	void operator()();
};
}

#endif /* PEERSERVER_H_ */
