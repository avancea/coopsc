#include <stdexcept>
#include <sstream>
#include "logger.h"
#include "connection.h"
#include "resultset.h"

namespace coopsc
{

unsigned long long ResultSet::checksum() const
{
	unsigned long long result = 0;
	for (unsigned int i = 0; i < getNoTuples(); i++)
		for (unsigned int j = 0; j < getNoFields(); j++)
		{
			std::string value = getValue(i, j);
			for (unsigned int k = 0; k < value.size(); k++)
				result += value[k];
		}
	return result;
}

}
