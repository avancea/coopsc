#ifndef COOPSCCLIENT_H_
#define COOPSCCLIENT_H_
#include <string>
#include <ostream>
#include <boost/asio.hpp>
#include <boost/smart_ptr.hpp>
#include <boost/function.hpp>
#include <boost/archive/binary_oarchive.hpp>
#include <boost/archive/binary_iarchive.hpp>
#include "concurrent_queue.h"
#include "region.h"
#include "queryplan.h"
#include "query.h"
namespace coopsc
{

class Connection;

typedef std::pair<std::string, int> PeerId;

std::ostream& operator <<(std::ostream& os, const PeerId& peerId);

class Peer
{
public:
	//callback function type for asynchronous queries
	typedef boost::function<void(bool successful, RegionPtr region, std::string error)> Callback;
private:
	typedef boost::shared_ptr<boost::thread> ThreadPtr;
	typedef boost::shared_ptr<boost::asio::ip::tcp::iostream> TcpIostreamPtr;
	typedef boost::shared_ptr<boost::archive::binary_oarchive> OArchivePtr;
	typedef boost::shared_ptr<boost::archive::binary_iarchive> IArchivePtr;
	struct Job
	{
		QueryPlan queryplan;
		Callback callback;
		Job()
		{

		}
		Job(const Job& job) :
			queryplan(job.queryplan), callback(job.callback)
		{
		}
		Job(QueryPlan _queryplan, const Callback& _callback) :
			queryplan(_queryplan), callback(_callback)
		{
		}
	};

	Connection& connection;
	PeerId peerId;
	TcpIostreamPtr stream;

	boost::mutex mutex;

	//true if the thread is dead (of dying)
	//set to true by the destructor or when there's a communication error
	bool zombie;

	ThreadPtr thread;
	concurrent_queue<Job> jobs;
	bool connected;

	void quit();

	void killthread();

	void sendQueryPlan(const QueryPlan& plan);
	RegionPtr receiveRegion();
	std::string receiveError();

public:
	//construct and connects to the server
	Peer(Connection& _connection, const PeerId& _peerId);

	Peer(const Peer&)= delete;
	Peer& operator =(const Peer&)= delete;

	void connect();
	void disconnect();

	const PeerId& getId() const
	{
		return peerId;
	}

	bool isConnected() const
	{
		return connected;
	}

	bool isZombie() const
	{
		return zombie;
	}

	//asynchronous execution
	void asyncExecute(const QueryPlan& plan, Callback callback);

	//executes a query plan
	RegionPtr execute(const QueryPlan& plan);

	//thread method
	void operator()();

	~Peer();
};

typedef boost::shared_ptr<Peer> PeerPtr;

}

#endif /*COOPSCCLIENT_H_*/
