#include "databasepool.h"
#include "connection.h"

namespace coopsc
{

DatabasePool::DatabasePool(Connection& _connection): connection(_connection), connected(false)
{
	for (int i = 0; i < POOL_SIZE; i++)
	{
		dbconnections.push_back(DatabaseConnectionPtr(new DatabaseConnection(connection, jobs)));
	}
}

void DatabasePool::connect()
{
	for (int i = 0; i < POOL_SIZE; i++)
	{
		dbconnections[i]->connect();
	}
	connected = true;
}

void DatabasePool::disconnect()
{
	connected = false;
	for (int i = 0; i < POOL_SIZE; i++)
	{
		dbconnections[i]->disconnect();
	}
}

void DatabasePool::asyncQuery(const Query& q, DatabaseConnection::Callback callback)
{
	if (!connected)
		throw std::runtime_error("Can not accept new requests! Disconnecting...");
	jobs.push(DatabaseConnection::Job(QueryPtr(new Query(q)), callback));
}


void DatabasePool::readMetadata()
{
	dbconnections[rand() % POOL_SIZE]->readMetadata();
}


ResultSetPtr DatabasePool::query(const std::string& q)
{
	return dbconnections[rand() % POOL_SIZE]->query(q);
}



void DatabasePool::execute(const std::string& s)
{
	dbconnections[rand() % POOL_SIZE]->execute(s);
}

void DatabasePool::getNQuadsTimestamps(const NQuads& nquads, NQuadTimestamps& result)
{
	dbconnections[rand() % POOL_SIZE]->getNQuadsTimestamps(nquads, result);
}

}
