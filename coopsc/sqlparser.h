#ifndef SQLPARSER_H_
#define SQLPARSER_H_

#include <functional>
#include <vector>
#include <string>
#include <utility>
#include <boost/spirit/include/classic_core.hpp>
#include <boost/spirit/include/classic_parse_tree.hpp>
#include <boost/any.hpp>
#include "predicate.h"
#include "query.h"
#include "metadataregistry.h"

namespace coopsc
{

class Connection;

// A simple SQL parser
// select a1, a2, a3 from table where (a
class SQLParser
{
private:

	// Here's some typedefs to simplify things
	typedef char const* iterator_t;
	typedef boost::spirit::classic::tree_match<iterator_t> parse_tree_match_t;
	typedef parse_tree_match_t::const_tree_iterator iter_t;

	typedef boost::spirit::classic::pt_match_policy<iterator_t> match_policy_t;
	typedef boost::spirit::classic::scanner_policies<boost::spirit::classic::iteration_policy, match_policy_t,
			boost::spirit::classic::action_policy> scanner_policy_t;
	typedef boost::spirit::classic::scanner<iterator_t, scanner_policy_t> scanner_t;
	typedef boost::spirit::classic::rule<scanner_t> rule_t;
	typedef enum
	{
		EQUAL, NOT_EQUAL, LESS, GREATER, LESS_EQUAL, GREATER_EQUAL
	} Operator;

	rule_t identifier;
	rule_t allfields;
	rule_t fields;
	rule_t value;
	rule_t comp_op;
	rule_t comp;
	rule_t conj;
	rule_t disj;
	rule_t sql;
	rule_t term;

	Connection& connection;

	Query ev_sql(iter_t const& i, std::vector<int>&);
	std::string ev_identifier(iter_t const& i);
	Operator ev_operator(iter_t const& i);
	int ev_value(iter_t const& i);

	Predicate ev_comp(iter_t const& i, std::string tablename);
	Predicate ev_term(iter_t const& i, std::string tablename);
	Predicate ev_conj(iter_t const& i, std::string tablename);
	Predicate ev_disj(iter_t const& i, std::string tablename);
	void ev_fields(iter_t const& i, std::string tablename, std::vector<int>& result);

public:
	SQLParser(Connection& _connection);

	Query parse(const std::string& q, std::vector<int>& fields); //contains the list of fields in the right order (FieldsMask has a set semantic)
};

}
#endif /*SQLPARSER_H_*/
