#ifndef DATABASEPOOL_H_
#define DATABASEPOOL_H_
#include <boost/smart_ptr.hpp>
#include <vector>
#include "databaseconnection.h"
namespace coopsc
{
class Connection;
//database connection pool
class DatabasePool
{
private:
	static const int POOL_SIZE = 1;
	DatabaseConnection::JobQueue jobs;
	std::vector<DatabaseConnectionPtr> dbconnections;
	Connection& connection;
	bool connected;

public:
	DatabasePool(Connection& _connection);

	void connect();
	void disconnect();

	//asynchronous query execution
	//q - the query to be executed
	//callback - the functor that is called when the query execution is finished
	void asyncQuery(const Query& q, DatabaseConnection::Callback callback);



	//synchronous query execution
	ResultSetPtr query(const std::string& q);
	void execute(const std::string& q);

	void readMetadata();
	void getNQuadsTimestamps(const NQuads& nquads, NQuadTimestamps& result);
};

}

#endif
