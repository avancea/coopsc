#ifndef QUERYPLAN_H_
#define QUERYPLAN_H_

#include <boost/smart_ptr.hpp>
#include <boost/archive/binary_oarchive.hpp>
#include <boost/archive/binary_iarchive.hpp>
#include <boost/serialization/shared_ptr.hpp>
#include <boost/serialization/vector.hpp>
#include <boost/serialization/tracking.hpp>
#include <sstream>
#include <set>
#include "region.h"
#include "query.h"
#include "metadataregistry.h"
#include "node.h"
#include "nquad.h"

namespace coopsc
{

//forward declarations
class Connection;
class SelectProjectNode;
class UnionNode;
class JoinNode;
class RegionNode;
;
class ServerNode;
class RemoteNode;

//The query plan
class QueryPlan
{
private:
	NodePtr root;
	Connection* connection;
	friend class boost::serialization::access;
	template<class Archive>
	void serialize(Archive & ar, const unsigned int version)
	{
		ar.register_type(static_cast<SelectProjectNode *> (NULL));
		ar.register_type(static_cast<UnionNode *> (NULL));
		ar.register_type(static_cast<JoinNode *> (NULL));
		ar.register_type(static_cast<RegionNode *> (NULL));
		ar.register_type(static_cast<ServerNode *> (NULL));
		ar.register_type(static_cast<RemoteNode *> (NULL));
		ar & root;
	}

public:
	QueryPlan() :
		connection(NULL)
	{
	}

	QueryPlan(const QueryPlan& that) :
		root(that.root), connection(that.connection)
	{
	}

	QueryPlan& operator =(const QueryPlan& that)
	{
		root = that.root;
		return *this;
	}

	QueryPlan(Connection& _connection, NodePtr _root) :
		root(_root), connection(&_connection)
	{

	}

	~QueryPlan()
	{
	}

	void setConnection(Connection& _connection);

	RegionPtr execute(const NQuadTimestamps& ts, bool keepstatistics = true)
	{
		root->preExecute();
		return root->execute(ts, keepstatistics);
	}

	NodePtr getRoot()
	{
		return root;
	}

	const NodePtr getRoot() const
	{
		return root;
	}

	void setRoot(NodePtr _root)
	{
		root = _root;
	}

};

typedef boost::shared_ptr<QueryPlan> QueryPlanPtr;
std::ostream& operator <<(std::ostream& s, const QueryPlan& plan);
}
;

BOOST_CLASS_TRACKING(coopsc::QueryPlan, boost::serialization::track_never)
;

#endif /*QUERYPLAN_H_*/
