#include <boost/bind.hpp>
#include "logger.h"
#include "remotenode.h"
#include "queryplan.h"
#include "connection.h"
namespace coopsc
{

void RemoteNode::preExecuteNode()
{
	if (!result)
		result = new concurrent_queue<Result> ();
	getConnection().getPeerPool(). get(peerId)->asyncExecute(child, boost::bind(&RemoteNode::setResult, this, _1, _2, _3));
}

RegionPtr RemoteNode::executeNode(const std::vector<RegionPtr>& childrenresults, bool keepstatistics)
{
	Result r;
	result->wait_and_pop(r);
	if (std::get<0>(r)) //successful
	{
		if (getConnection().getCachePeerData())
			getConnection().getStorageManager().addRegion(std::get<1>(r));
		if (keepstatistics)
		{
			getConnection().getStatisticsRegistry().tuplesPeer(peerId, std::get<1>(r)->getNoTuples());
		}
		return std::get<1>(r);
	}
	throw std::runtime_error(std::get<2>(r));

}
void RemoteNode::writeTo(std::ostream& s) const
{
	s << "remote(" << peerId.first << "," << peerId.second << ",";
	s << child;
	s << ")";
}

void RemoteNode::setResult(bool successful, RegionPtr region, std::string error)
{
	if (result)
		result->push(Result(successful, region, error));
}

}
