#ifndef RESULTSET_H_
#define RESULTSET_H_
#include <boost/smart_ptr.hpp>
#include <string>

namespace coopsc
{

class Connection;

//The generic result set class
class ResultSet
{
public:
	virtual ~ResultSet()
	{
	}
	virtual unsigned int getNoTuples() const = 0;
	virtual unsigned int getNoFields() const = 0;
	virtual std::string getFieldName(int fieldno) const = 0;
	virtual std::string getValue(unsigned int tupleno, unsigned int fieldno) const = 0;
	virtual std::string
	getValue(unsigned int tupleno, const std::string& field) const = 0;


	virtual std::string getFieldType(int fieldno) const = 0;
	virtual std::string getFieldType(const std::string& field) const = 0;
	virtual int getFieldSize(int fieldno) const = 0;
	virtual int getFieldSize(const std::string& field) const = 0;


	//calculates the sum of all character of the result set
	unsigned long long checksum() const;

};

typedef boost::shared_ptr<ResultSet> ResultSetPtr;

}

#endif /* RESULTSET_H_ */
