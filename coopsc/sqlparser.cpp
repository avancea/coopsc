#include <iostream>
#include <stdexcept>
#include <stdlib.h>
#include <utility>
#include <sstream>
#include "logger.h"
#include "sqlparser.h"
#include "connection.h"

using namespace boost::spirit::classic;
using namespace std;

namespace coopsc
{

//macro for discarted elements
#define space 	   discard_node_d[(*blank_p)]

//mandatory space
#define m_space   discard_node_d[(+blank_p)]
#define keyword(s) discard_node_d[as_lower_d[str_p(s)]]

SQLParser::SQLParser(Connection& _connection) :
	connection(_connection)
{
	identifier = token_node_d[(alpha_p | "_") >> (*(alnum_p | '_'))];
	allfields = token_node_d[str_p("*")];
	fields = (space >> allfields) | (space >> identifier >> (*(space >> keyword(",") >> space >> identifier)));
	value = token_node_d[int_p];

	comp_op = token_node_d[str_p("=") | ">=" | "<=" | "<>" | "!=" | "<" | ">"];

	comp = (space >> identifier >> space >> comp_op >> space >> value) | (space >> value >> space >> comp_op >> space >> identifier);

	term = comp | (space >> keyword("(") >> disj >> space >> keyword(")"));

	conj = (term >> (*(space >> keyword("and") >> term)));

	disj = (conj >> (*(space >> keyword("or") >> conj)));

	sql = keyword("select") >> m_space >> fields >> m_space >> keyword("from") >> m_space >> identifier >> m_space >> keyword("where")
			>> m_space >> disj >> space;
}

std::string SQLParser::ev_identifier(iter_t const& i)
{
	if (i->value.id() != parser_id(&identifier))
		throw std::runtime_error("Not an identifier");
	return std::string(i->children.begin()->value.begin(), i->children.begin()->value.end());
}

int SQLParser::ev_value(iter_t const& i)
{
	if (i->value.id() != parser_id(&value))
		throw std::runtime_error("Not a value");
	iter_t chi = i->children.begin();
	std::string tmp(chi->value.begin(), chi->value.end());

	std::stringstream ssint(tmp.c_str());
	int v_int;
	ssint >> v_int;
	if (ssint)
		return v_int;

	throw std::runtime_error("Value type not recognized");
}

SQLParser::Operator SQLParser::ev_operator(iter_t const& i)
{
	if (i->value.id() != parser_id(&comp_op))
		throw std::runtime_error("Not an operator");
	std::string tmp(i->children.begin()->value.begin(), i->children.begin()->value.end());
	if (tmp == "=")
		return SQLParser::EQUAL;
	else if (tmp == "<")
		return SQLParser::LESS;
	else if (tmp == ">")
		return SQLParser::GREATER;
	else if (tmp == "<=")
		return SQLParser::LESS_EQUAL;
	else if (tmp == ">=")
		return SQLParser::GREATER_EQUAL;
	else if (tmp == "<>")
		return SQLParser::NOT_EQUAL;
	else if (tmp == "!=")
		return SQLParser::NOT_EQUAL;
	else
		throw std::runtime_error("Operator not recognized");
}

Predicate SQLParser::ev_comp(iter_t const& i, std::string tablename)
{
	if (i->value.id() != parser_id(&comp))
		throw std::runtime_error("Not a comparation");

	int fieldno = 0;
	SQLParser::Operator op = SQLParser::EQUAL;
	int val = 0;

	//the "normal" order is "field operator value"
	//invers is set to true if the order is reversed
	bool invers = false;
	bool fieldsSet = false;
	for (iter_t it = i->children.begin(); it != i->children.end(); it++)
	{
		if (it->value.id() == parser_id(&comp))
			return ev_comp(it, tablename);

		if (it->value.id() == parser_id(&identifier))
		{
			std::string field = ev_identifier(it);
			fieldno = connection.getMetadataRegistry().getTable(tablename).getFieldNo(field);
			fieldsSet = true;
		}
		if (it->value.id() == parser_id(&comp_op))
			op = ev_operator(it);
		if (it->value.id() == parser_id(&value))
		{
			if (!fieldsSet)
				invers = true;
			val = ev_value(it);
		}
	}

	if (invers)
	{
		switch (op)
		{
		case SQLParser::LESS:
			op = SQLParser::GREATER;
			break;
		case SQLParser::GREATER:
			op = SQLParser::LESS;
			break;
		case SQLParser::LESS_EQUAL:
			op = SQLParser::GREATER_EQUAL;
			break;
		case SQLParser::GREATER_EQUAL:
			op = SQLParser::LESS_EQUAL;
			break;
		default:
			break;
		}
	}

	switch (op)
	{
	case SQLParser::LESS:
		return Predicate(connection, tablename, fieldno, Predicate::_oo, val - 1);
	case SQLParser::LESS_EQUAL:
		return Predicate(connection, tablename, fieldno, Predicate::_oo, val);
	case SQLParser::GREATER:
		return Predicate(connection, tablename, fieldno, val + 1, Predicate::oo);
	case SQLParser::GREATER_EQUAL:
		return Predicate(connection, tablename, fieldno, val, Predicate::oo);
	case SQLParser::EQUAL:
		return Predicate(connection, tablename, fieldno, val, val);
	case SQLParser::NOT_EQUAL:
		return Predicate(connection, tablename, fieldno, Predicate::_oo, val - 1) || Predicate(connection, tablename, fieldno, val + 1,
				Predicate::oo);
	default:
		throw std::runtime_error("Operator not recognized");
	}
}

Predicate SQLParser::ev_term(iter_t const& i, std::string tablename)
{
	if (i->value.id() != parser_id(&term))
		throw std::runtime_error("Not a term");
	iter_t it = i->children.begin();
	if (it->value.id() == parser_id(&comp))
		return ev_comp(it, tablename);
	else
		return ev_disj(it, tablename);
}

Predicate SQLParser::ev_conj(iter_t const& i, std::string tablename)
{
	if (i->value.id() != parser_id(&conj))
		throw std::runtime_error("Not a conjunction");
	Predicate result;
	bool first = true;
	for (iter_t it = i->children.begin(); it != i->children.end(); it++)
	{
		Predicate w = ev_term(it, tablename);
		if (first)
		{
			result = w;
			first = false;
		}
		else
			result = result && w;
	}
	return result;
}

Predicate SQLParser::ev_disj(iter_t const& i, std::string tablename)
{
	if (i->value.id() != parser_id(&disj))
		throw std::runtime_error("Not a disjunction");
	Predicate result;
	bool first = true;
	for (iter_t it = i->children.begin(); it != i->children.end(); it++)
	{
		Predicate w = ev_conj(it, tablename);
		if (first)
		{
			result = w;
			first = false;
		}
		else
			result = result || w;
	}
	return result;
}

void SQLParser::ev_fields(iter_t const& i, std::string tablename, std::vector<int>& result)
{
	if (i->value.id() != parser_id(&fields))
		throw std::runtime_error("Not a field list");
	const Table& table = connection.getMetadataRegistry().getTable(tablename);
	if (i->children.begin()->value.id() == parser_id(&allfields))
	{
		for (unsigned int i = 0; i < table.getNoFields(); i++)
			result.push_back(i);
		return;
	}
	for (iter_t it = i->children.begin(); it != i->children.end(); it++)
	{
		std::string field;
		if (it->value.id() == parser_id(&identifier))
		{
			field = ev_identifier(it);
			result.push_back(table.getFieldNo(field));
		}
	}
}

Query SQLParser::ev_sql(iter_t const& i, std::vector<int>& fields)
{
	if (i->value.id() != parser_id(&sql))
		throw std::runtime_error("Not a sql query");

	Predicate pred;
	std::string tablename;

	for (iter_t it = i->children.begin(); it != i->children.end(); it++)
	{
		if (it->value.id() == parser_id(&identifier))
		{
			tablename = ev_identifier(it);
		}
	}

	for (iter_t it = i->children.begin(); it != i->children.end(); it++)
	{

		if (it->value.id() == parser_id(&(this->fields)))
			ev_fields(it, tablename, fields);

		if (it->value.id() == parser_id(&disj))
			pred = ev_disj(it, tablename);
	}
	FieldsMask fieldsmask = 0;
	for (unsigned int i = 0; i < fields.size(); i++)
		set(fieldsmask, fields[i]);
	return Query(connection, tablename, pred, fieldsmask);
}

Query SQLParser::parse(const std::string& q, std::vector<int>& fields)
{
	tree_parse_info<> info = pt_parse(q.c_str(), sql);
	if (!info.full)
		throw std::runtime_error(std::string("Invalid sql query : ") + q);

	return ev_sql(info.trees.begin(), fields);
}

}
