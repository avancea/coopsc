#ifndef COOPSCSERVER_H_
#define COOPSCSERVER_H_
#include <string>
#include <boost/smart_ptr.hpp>
#include <boost/thread.hpp>
#include <boost/asio.hpp>
#include "peerserver.h"

namespace coopsc
{
class Connection;

//listen for connections from remote peers
class Listener
{
private:
	typedef boost::shared_ptr<boost::thread> ThreadPtr;

	typedef boost::shared_ptr<boost::asio::ip::tcp::acceptor> AcceptorPtr;

	Connection& connection;

	AcceptorPtr acceptor;

	boost::asio::io_service io_service;

	//acceptor thread
	ThreadPtr thread;

	void handle_accept(PeerServer server, const boost::system::error_code& error);

public:
	Listener(Connection& _connection) :
		connection(_connection)
	{
	}

	//no copy constructor
	Listener(const Listener&)= delete;

	//no assignment operator
	Listener& operator =(const Listener&)= delete;

	~Listener()
	{
		stop();
	}

	void operator()();
	void start();
	void stop();

};

}

#endif /*COOPSCSERVER_H_*/
